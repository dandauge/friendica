<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:32
  from '/var/www/friendica/view/templates/group_selection.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1d8735095_62975991',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d6f0541be008e0fcb51ecf3fb2c60ef6ac7d73c' => 
    array (
      0 => '/var/www/friendica/view/templates/group_selection.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1d8735095_62975991 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="field custom">
<label for="group-selection" id="group-selection-lbl"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['label']->value, ENT_QUOTES, 'UTF-8');?>
</label>
<select name="group-selection" id="group-selection" >
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'group');
$_smarty_tpl->tpl_vars['group']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->do_else = false;
?>
<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['group']->value['selected']) {?>selected="selected"<?php }?> ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['name'], ENT_QUOTES, 'UTF-8');?>
</option>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</select>
</div>
<?php }
}
