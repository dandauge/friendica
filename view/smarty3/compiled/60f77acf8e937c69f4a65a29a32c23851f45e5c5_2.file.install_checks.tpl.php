<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:32:24
  from '/var/www/friendica/view/templates/install_checks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449da83da164_54557449',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '60f77acf8e937c69f4a65a29a32c23851f45e5c5' => 
    array (
      0 => '/var/www/friendica/view/templates/install_checks.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449da83da164_54557449 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h1><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/images/friendica-32.png"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pass']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
<form  action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/index.php?pagename=install" method="post">
<table>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['checks']->value, 'check');
$_smarty_tpl->tpl_vars['check']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['check']->value) {
$_smarty_tpl->tpl_vars['check']->do_else = false;
?>
	<tr><td><?php echo $_smarty_tpl->tpl_vars['check']->value['title'];?>
 </td><td>
	<?php if ($_smarty_tpl->tpl_vars['check']->value['status']) {?>
		<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/install/green.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ok']->value, ENT_QUOTES, 'UTF-8');?>
">
	<?php } else { ?>
		<?php if ($_smarty_tpl->tpl_vars['check']->value['required']) {?>
			<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/install/red.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['requirement_not_satisfied']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php } else { ?>
			<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/install/yellow.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['optional_requirement_not_satisfied']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php }?>
	<?php }?>
	</td><td><?php if ($_smarty_tpl->tpl_vars['check']->value['required']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['required']->value, ENT_QUOTES, 'UTF-8');
}?></td></tr>
	<?php if ($_smarty_tpl->tpl_vars['check']->value['help']) {?>
	<tr><td class="help" colspan="3">
		<blockquote><?php echo $_smarty_tpl->tpl_vars['check']->value['help'];?>
</blockquote>
		<?php if ($_smarty_tpl->tpl_vars['check']->value['error_msg']) {?>
		<div class="error_header"><b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value['error_msg']['head'], ENT_QUOTES, 'UTF-8');?>
<br><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value['error_msg']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value['error_msg']['url'], ENT_QUOTES, 'UTF-8');?>
</a></b></div>
		<blockquote><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value['error_msg']['msg'], ENT_QUOTES, 'UTF-8');?>
</blockquote>
		<?php }?>
	</td></tr>
	<?php }
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</table>

<?php if ($_smarty_tpl->tpl_vars['phpath']->value) {?>
	<input type="hidden" name="config-php_path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['php_path']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['passed']->value) {?>
	<input type="hidden" name="pass" value="2">
	<input type="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php } else { ?>
	<input type="hidden" name="pass" value="1">
	<input type="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reload']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php }?>
</form>
<?php }
}
