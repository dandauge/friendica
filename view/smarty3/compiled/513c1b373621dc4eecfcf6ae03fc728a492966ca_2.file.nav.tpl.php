<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:36:08
  from '/var/www/friendica/view/theme/vier/templates/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e88790535_52686136',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '513c1b373621dc4eecfcf6ae03fc728a492966ca' => 
    array (
      0 => '/var/www/friendica/view/theme/vier/templates/nav.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449e88790535_52686136 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header>
	
	<div id="site-location"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
</div>
	<div id="banner"><?php echo $_smarty_tpl->tpl_vars['banner']->value;?>
</div>
</header>
<nav role="menubar">
	<ul>
		<li class="mobile-aside-toggle" style="display:none;">
			<a href="#">
				<i class="icons icon-reorder"></i>
			</a>
		</li>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?>
			<li role="menuitem" id="nav-home-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['home'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="p" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][3], ENT_QUOTES, 'UTF-8');?>
" >
					<span class="desktop-view"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
</span>
					<i class="icon s22 icon-home mobile-view"><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
</span></i>
					<span id="home-update" class="nav-notification"></span>
				</a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?>
			<li role="menuitem" id="nav-network-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['network'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="n" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][3], ENT_QUOTES, 'UTF-8');?>
" >
					<span class="desktop-view"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
</span>
					<i class="icon s22 icon-th mobile-view"><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
</span></i>
					<span id="net-update" class="nav-notification"></span>
				</a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['events']) {?>
			<li role="menuitem" id="nav-events-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['events'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="e" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][2], ENT_QUOTES, 'UTF-8');?>
 desktop-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][2], ENT_QUOTES, 'UTF-8');?>
 mobile-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="icon s22 icon-calendar"></i></a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['community']) {?>
			<li role="menuitem" id="nav-community-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['community'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="c" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][2], ENT_QUOTES, 'UTF-8');?>
 desktop-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][2], ENT_QUOTES, 'UTF-8');?>
 mobile-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="icon s22 icon-bullseye"></i></a>
			</li>
		<?php }?>

		<li role="menu" aria-haspopup="true" id="nav-site-linkmenu" class="nav-menu-icon"><a><span class="icon s22 icon-question"><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][3], ENT_QUOTES, 'UTF-8');?>
</span></span></a>
			<ul id="nav-site-menu" class="menu-popup">
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['help']) {?> <li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
				<li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['tos']) {?><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
				<li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
			</ul>
		</li>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
			<li role="menu" aria-haspopup="true" id="nav-messages-linkmenu" class="nav-menu-icon">
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
">
					<span class="icon s22 icon-envelope"><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</span></span>
					<span id="mail-update" class="nav-notification"></span>
				</a>
			</li>
		<?php }?>


		<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
			<li role="menu" aria-haspopup="true" id="nav-notifications-linkmenu" class="nav-menu-icon">
				<a title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
">
					<span class="icon s22 icon-bell tilted-icon"><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</span></span>
					<span id="notification-update" class="nav-notification"></span>
				</a>
				<ul id="nav-notifications-menu" class="menu-popup">
					<li role="menuitem" id="nav-notifications-mark-all"><a onclick="notificationMarkAll(); return false;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<li role="menuitem" id="nav-notifications-see-all"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<li role="menuitem" class="empty"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emptynotifications']->value, ENT_QUOTES, 'UTF-8');?>
</li>
				</ul>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['userinfo']->value) {?>
			<li role="menu" aria-haspopup="true" id="nav-user-linkmenu" class="nav-menu">
				<a accesskey="u" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['userinfo']->value['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['userinfo']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><span id="nav-user-linklabel"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['userinfo']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span><span id="intro-update" class="nav-notification"></span></a>
				<ul id="nav-user-menu" class="menu-popup">
					<li role="menuitem"> <a  class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</a> </li>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['introductions']) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][1], ENT_QUOTES, 'UTF-8');?>
</a><span id="intro-update-li" class="nav-notification"></span></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
 <span id="mail-update-li" class="nav-notification"></span></a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['usermenu'][1]) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['usermenu'][1][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['usermenu'][1][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['usermenu'][1][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['usermenu'][1][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?><li role="menuitem"><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?>
					<li role="menuitem">
						<a accesskey="a" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a>
					</li>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?><li role="menuitem"><a class="menu-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
				</ul>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['login']) {?>
			<li role="menuitem" id="nav-login-link" class="nav-menu">
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?>
			<li role="menuitem" id="nav-logout-link" class="nav-menu">
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['search']) {?>
			<li role="search" id="nav-search-box">
				<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
">
					<input accesskey="s" id="nav-search-text" class="nav-menu-search" type="text" value="" name="q" placeholder=" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_hint']->value, ENT_QUOTES, 'UTF-8');?>
">
					<select name="search-option">
						<option value="fulltext"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['searchoption'][0], ENT_QUOTES, 'UTF-8');?>
</option>
						<option value="tags"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['searchoption'][1], ENT_QUOTES, 'UTF-8');?>
</option>
						<option value="contacts"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['searchoption'][2], ENT_QUOTES, 'UTF-8');?>
</option>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['searchoption'][3]) {?><option value="forums"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['searchoption'][3], ENT_QUOTES, 'UTF-8');?>
</option><?php }?>
					</select>
				</form>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?>
			<li role="menuitem" id="nav-admin-link" class="nav-menu">
				<a accesskey="a" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
 icon-sliders" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
</span></a>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['apps']) {?>
			<li role="menu" aria-haspopup="true" id="nav-apps-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['apps'], ENT_QUOTES, 'UTF-8');?>
">
				<a class=" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][2], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<ul id="nav-apps-menu" class="menu-popup">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['apps']->value, 'ap');
$_smarty_tpl->tpl_vars['ap']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ap']->value) {
$_smarty_tpl->tpl_vars['ap']->do_else = false;
?>
					<li role="menuitem"><?php echo $_smarty_tpl->tpl_vars['ap']->value;?>
</li>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</ul>
			</li>
		<?php }?>
	</ul>

</nav>
<ul id="nav-notifications-template" style="display:none;" rel="template">
	<li class="{4}" onclick="location.href='{0}';">
		<div class="notif-entry-wrapper">
			<div class="notif-photo-wrapper"><a href="{6}"><img data-src="{1}"></a></div>
			<div class="notif-desc-wrapper">
				{8}{7}
				<div><time class="notif-when" title="{5}">{3}</time></div>
			</div>
		</div>
	</li>
</ul>
<!--
<div class="icon-flag" style="position: fixed; bottom: 10px; left: 20px; z-index:9;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['langselector']->value, ENT_QUOTES, 'UTF-8');?>
</div>
-->
<?php }
}
