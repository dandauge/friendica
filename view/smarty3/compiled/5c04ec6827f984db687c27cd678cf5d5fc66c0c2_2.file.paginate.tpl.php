<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/theme/frio/templates/paginate.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291bbf1a4_19823135',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c04ec6827f984db687c27cd678cf5d5fc66c0c2' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/paginate.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291bbf1a4_19823135 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if ($_smarty_tpl->tpl_vars['pager']->value) {?>
<ul class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['class'], ENT_QUOTES, 'UTF-8');?>
 pagination-sm">
	<?php if ($_smarty_tpl->tpl_vars['pager']->value['first']) {?><li class="pager_first <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['text'], ENT_QUOTES, 'UTF-8');?>
">&#8739;&lt;</a></li><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['prev']) {?><li class="pager_prev <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['text'], ENT_QUOTES, 'UTF-8');?>
">&lt;</a></li><?php }?>

	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pager']->value['pages'], 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?><li class="pager_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['class'], ENT_QUOTES, 'UTF-8');?>
 hidden-xs hidden-sm"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['text'], ENT_QUOTES, 'UTF-8');?>
</a></li><?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['next']) {?><li class="pager_next <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['text'], ENT_QUOTES, 'UTF-8');?>
">&gt;</a></li><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['last']) {?><li class="pager_last <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['text'], ENT_QUOTES, 'UTF-8');?>
">&gt;&#8739;</a></li><?php }?>
</ul>
<?php }
}
}
