<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/jot-header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f3778026_77750148',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24f66d744a657934c79e7a6b3203c4e613ddd7c8' => 
    array (
      0 => '/var/www/friendica/view/templates/jot-header.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f3778026_77750148 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
 language="javascript" type="text/javascript">

var editor = false;
var textlen = 0;

function initEditor(callback) {
	if (editor == false) {
		var  colorbox_options = {
			<?php if ($_smarty_tpl->tpl_vars['is_mobile']->value) {?>
			'width' : '100%',
			'height' : '100%',
			<?php }?>
			'inline' : true,
			'transition' : 'elastic'
		}

		$("#profile-jot-text-loading").show();
		$("#profile-jot-text-loading").hide();
		$("#profile-jot-text").css({ 'height': 200, 'color': '#000' });
		$("#profile-jot-text").editor_autocomplete(baseurl + '/search/acl');
		$("#profile-jot-text").bbco_autocomplete('bbcode');
		$("a#jot-perms-icon").colorbox(colorbox_options);
		$(".jothidden").show();

		editor = true;
	}
	if (typeof callback != "undefined") {
		callback();
	}
}

function enableOnUser(){
	if (editor) {
		return;
	}
	$(this).val('');
	initEditor();
}

<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/js/ajaxupload.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
	var ispublic = '<?php echo $_smarty_tpl->tpl_vars['ispublic']->value;?>
';


	$(document).ready(function() {

		/* enable editor on focus and click */
		$("#profile-jot-text").focus(enableOnUser);
		$("#profile-jot-text").click(enableOnUser);

		/* show images / file browser window
		 *
		 **/

		/* callback */
		$('body').on('fbrowser.image.main', function(e, filename, embedcode, id) {
			$.colorbox.close();
			addeditortext(embedcode);
		});
		$('body').on('fbrowser.file.main', function(e, filename, embedcode, id) {
			$.colorbox.close();
			addeditortext(embedcode);
		});

		$('#wall-image-upload').on('click', function(){
			Dialog.doImageBrowser("main");
		});

		$('#wall-file-upload').on('click', function(){
			Dialog.doFileBrowser("main");
		});
	});


	function deleteCheckedItems() {
		if(confirm('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delitems']->value, ENT_QUOTES, 'UTF-8');?>
')) {
			var checkedstr = '';

			$("#item-delete-selected").hide();
			$('#item-delete-selected-rotator').show();

			$('.item-select').each( function() {
				if($(this).is(':checked')) {
					if(checkedstr.length != 0) {
						checkedstr = checkedstr + ',' + $(this).val();
					}
					else {
						checkedstr = $(this).val();
					}
				}
			});
			$.post('item', { dropitems: checkedstr }, function(data) {
				window.location.reload();
			});
		}
	}

	function jotGetLink() {
		reply = prompt("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['linkurl']->value, ENT_QUOTES, 'UTF-8');?>
");
		if(reply && reply.length) {
			reply = bin2hex(reply);
			$('#profile-rotator').show();
			$.get('parse_url?binurl=' + reply, function(data) {
				addeditortext(data);
				$('#profile-rotator').hide();
			});
		}
	}

	function jotVideoURL() {
		reply = prompt("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vidurl']->value, ENT_QUOTES, 'UTF-8');?>
");
		if(reply && reply.length) {
			addeditortext('[video]' + reply + '[/video]');
		}
	}

	function jotAudioURL() {
		reply = prompt("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['audurl']->value, ENT_QUOTES, 'UTF-8');?>
");
		if(reply && reply.length) {
			addeditortext('[audio]' + reply + '[/audio]');
		}
	}


	function jotGetLocation() {
		reply = prompt("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['whereareu']->value, ENT_QUOTES, 'UTF-8');?>
", $('#jot-location').val());
		if(reply && reply.length) {
			$('#jot-location').val(reply);
		}
	}

	function jotShare(id) {
		if ($('#jot-popup').length != 0) $('#jot-popup').show();

		$('#like-rotator-' + id).show();
		$.get('share/' + id, function(data) {
			if (!editor) $("#profile-jot-text").val("");
			initEditor(function(){
				addeditortext(data);
				$('#like-rotator-' + id).hide();
				$(window).scrollTop(0);
			});

		});
	}

	function linkdropper(event) {
		var linkFound = event.dataTransfer.types.contains("text/uri-list");
		if(linkFound)
			event.preventDefault();
	}

	function linkdrop(event) {
		var reply = event.dataTransfer.getData("text/uri-list");
		event.target.textContent = reply;
		event.preventDefault();
		if(reply && reply.length) {
			reply = bin2hex(reply);
			$('#profile-rotator').show();
			$.get('parse_url?binurl=' + reply, function(data) {
				if (!editor) $("#profile-jot-text").val("");
				initEditor(function(){
					addeditortext(data);
					$('#profile-rotator').hide();
				});
			});
		}
	}

	function itemTag(id) {
		reply = prompt("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['term']->value, ENT_QUOTES, 'UTF-8');?>
");
		if(reply && reply.length) {
			reply = reply.replace('#','');
			if(reply.length) {

				commentBusy = true;
				$('body').css('cursor', 'wait');

				$.get('tagger/' + id + '?term=' + reply);
				if(timer) clearTimeout(timer);
				timer = setTimeout(NavUpdate,3000);
				liking = 1;
			}
		}
	}

	function itemFiler(id) {

		var bordercolor = $("input").css("border-color");

		$.get('filer/', function(data){
			$.colorbox({html:data});
			$("#id_term").keypress(function(){
				$(this).css("border-color",bordercolor);
			})
			$("#select_term").change(function(){
				$("#id_term").css("border-color",bordercolor);
			})

			$("#filer_save").click(function(e){
				e.preventDefault();
				reply = $("#id_term").val();
				if(reply && reply.length) {
					commentBusy = true;
					$('body').css('cursor', 'wait');
					$.get('filer/' + id + '?term=' + reply, NavUpdate);
//					if(timer) clearTimeout(timer);
//					timer = setTimeout(NavUpdate,3000);
					liking = 1;
					$.colorbox.close();
				} else {
					$("#id_term").css("border-color","#FF0000");
				}
				return false;
			});
		});

	}

	function jotClearLocation() {
		$('#jot-coord').val('');
		$('#profile-nolocation-wrapper').hide();
	}

	function addeditortext(data) {
		var currentText = $("#profile-jot-text").val();
		$("#profile-jot-text").val(currentText + data);
	}

	<?php echo $_smarty_tpl->tpl_vars['geotag']->value;?>


<?php echo '</script'; ?>
>

<?php }
}
