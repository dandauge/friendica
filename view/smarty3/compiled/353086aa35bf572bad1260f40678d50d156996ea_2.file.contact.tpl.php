<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:56:49
  from '/var/www/friendica/view/templates/admin/blocklist/contact.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1714befd1_19123959',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '353086aa35bf572bad1260f40678d50d156996ea' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/blocklist/contact.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
    'file:field_textarea.tpl' => 1,
  ),
),false)) {
function content_6044b1714befd1_19123959 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	function selectall(cls) {
		$('.' + cls).prop('checked', true);
		return false;
	}
	function selectnone(cls) {
		$('.' + cls).prop('checked', false);
		return false;
	}
<?php echo '</script'; ?>
>
<div id="adminpage">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<p><?php echo $_smarty_tpl->tpl_vars['description']->value;?>
</p>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/blocklist/contact" method="post">
        <input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_contacts']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
	<?php if ($_smarty_tpl->tpl_vars['contacts']->value) {?>
		<table id="contactblock">
			<thead>
				<tr>
					<th></th>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['th_contacts']->value, 'th');
$_smarty_tpl->tpl_vars['th']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['th']->value) {
$_smarty_tpl->tpl_vars['th']->do_else = false;
?>
					<th>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['th']->value, ENT_QUOTES, 'UTF-8');?>

					</th>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contacts']->value, 'contact');
$_smarty_tpl->tpl_vars['contact']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->do_else = false;
?>
				<tr>
					<td class="checkbox"><input type="checkbox" class="contacts_ckbx" id="id_contact_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['id'], ENT_QUOTES, 'UTF-8');?>
" name="contacts[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['id'], ENT_QUOTES, 'UTF-8');?>
"/></td>
					<td><img class="icon" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['micro'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
"></td>
					<td class="name">
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['name'], ENT_QUOTES, 'UTF-8');?>
<br>
						<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['addr'], ENT_QUOTES, 'UTF-8');?>
</a>
					</td>
					<td class="reason"><?php if ($_smarty_tpl->tpl_vars['contact']->value['block_reason']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['block_reason'], ENT_QUOTES, 'UTF-8');
} else { ?>N/A<?php }?></td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
		<p><a href="#" onclick="return selectall('contacts_ckbx');"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['select_all']->value, ENT_QUOTES, 'UTF-8');?>
</a> | <a href="#" onclick="return selectnone('contacts_ckbx');"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['select_none']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>
		<?php echo $_smarty_tpl->tpl_vars['paginate']->value;?>

		<div class="submit"><input type="submit" name="page_contactblock_unblock" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unblock']->value, ENT_QUOTES, 'UTF-8');?>
" /></div>
	<?php } else { ?>
		<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_data']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<?php }?>
	</form>

	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_newblock']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/blocklist/contact" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
		<table id="contactblock">
			<tbody>
				<tr>
					<td><?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['contacturl']->value), 0, false);
?></td>
					<td><?php $_smarty_tpl->_subTemplateRender("file:field_textarea.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['contact_block_reason']->value), 0, false);
?></td>
				</tr>
			</tbody>
		</table>
		<div class="submit"><input type="submit" name="page_contactblock_block" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" /></div>
	</form>
</div>
<?php }
}
