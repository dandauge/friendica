<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:10:24
  from '/var/www/friendica/view/theme/frio/templates/settings/profile/field/edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4a05a3456_97980286',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '484332beed3d64e2e64e6b536565464fa5db7bd7' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/profile/field/edit.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
    'file:field_textarea.tpl' => 1,
  ),
),false)) {
function content_6044b4a05a3456_97980286 (Smarty_Internal_Template $_smarty_tpl) {
?><fieldset data-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['id'], ENT_QUOTES, 'UTF-8');?>
">
	<legend>&#8801; <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['legend'], ENT_QUOTES, 'UTF-8');?>
</legend>

	<input type="hidden" name="profile_field_order[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['id'], ENT_QUOTES, 'UTF-8');?>
">

	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['profile_field']->value['fields']['label']), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender("file:field_textarea.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['profile_field']->value['fields']['value']), 0, false);
?>

		<p>
		<a id="settings-default-perms-menu" class="settings-default-perms" data-toggle="modal" data-target="#profile-field-acl-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['permissions'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['permdesc'], ENT_QUOTES, 'UTF-8');?>
</a>
	</p>

		<div class="modal" id="profile-field-acl-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['id'], ENT_QUOTES, 'UTF-8');?>
">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_field']->value['permissions'], ENT_QUOTES, 'UTF-8');?>
</h4>
				</div>
				<div class="modal-body">
					<?php echo $_smarty_tpl->tpl_vars['profile_field']->value['fields']['acl'];?>

				</div>
			</div>
		</div>
	</div>
</fieldset><?php }
}
