<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/theme/frio/templates/common_tabs.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291b236c4_64168261',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b42e3aa92dec7192a2c76180b1d9e200d16467c' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/common_tabs.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291b236c4_64168261 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="tabbar-wrapper">
		<ul role="menubar" class="tabbar list-inline visible-lg visible-md visible-sm hidden-xs">
				<li>
			<ul class="tabs flex-nav" role="menu" >
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tabs']->value, 'tab');
$_smarty_tpl->tpl_vars['tab']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->do_else = false;
?>
				<li id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" role="presentation" <?php if ($_smarty_tpl->tpl_vars['tab']->value['sel']) {?> class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['sel'], ENT_QUOTES, 'UTF-8');?>
" <?php }?>><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['tab']->value['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['tab']->value['title']) {?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		</li>

				<li class="pull-right">
			<ul class="tabs tabs-extended" role="menu">
				<li role="presentation" class="dropdown flex-target">
					<button type="button" class="btn-link dropdown-toggle" id="dropdownMenuTools" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</button>
				</li>
			 </ul>
		</li>
	</ul>

		<ul role="menubar" class="tabbar list-inline visible-xs">
				<li>
			<ul class="tabs" role="menu">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tabs']->value, 'tab');
$_smarty_tpl->tpl_vars['tab']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->do_else = false;
?>
					<?php if ($_smarty_tpl->tpl_vars['tab']->value['sel']) {?>
					<li id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-xs" role="presentation" <?php if ($_smarty_tpl->tpl_vars['tab']->value['sel']) {?> class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['sel'], ENT_QUOTES, 'UTF-8');?>
" <?php }?>><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['tab']->value['title']) {?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<?php } else { ?>
					<?php $_tmp_array = isset($_smarty_tpl->tpl_vars['exttabs']) ? $_smarty_tpl->tpl_vars['exttabs']->value : array();
if (!(is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess)) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['tab']->value;
$_smarty_tpl->_assignInScope('exttabs', $_tmp_array);?>
					<?php }?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		</li>

				<li class="pull-right">
			<ul class="tabs tabs-extended">
				<li class="dropdown">
					<button type="button" class="btn-link dropdown-toggle" id="dropdownMenuTools-xs" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</button>
					<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenuTools">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['exttabs']->value, 'tab');
$_smarty_tpl->tpl_vars['tab']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->do_else = false;
?>
						<li id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-xs" role="presentation" <?php if ($_smarty_tpl->tpl_vars['tab']->value['sel']) {?> class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['sel'], ENT_QUOTES, 'UTF-8');?>
" <?php }?>><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['tab']->value['title']) {?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div>
<?php }
}
