<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:33:12
  from '/var/www/friendica/view/templates/install_db.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449dd8242521_87173816',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d43d1b826a958d7641e63fd2bb65937bff35965' => 
    array (
      0 => '/var/www/friendica/view/templates/install_db.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 3,
    'file:field_password.tpl' => 1,
  ),
),false)) {
function content_60449dd8242521_87173816 (Smarty_Internal_Template $_smarty_tpl) {
?><h1><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/images/friendica-32.png"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pass']->value, ENT_QUOTES, 'UTF-8');?>
</h2>

<p>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_01']->value, ENT_QUOTES, 'UTF-8');?>
<br>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_02']->value, ENT_QUOTES, 'UTF-8');?>
<br>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_03']->value, ENT_QUOTES, 'UTF-8');?>

</p>

<table>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['checks']->value, 'check');
$_smarty_tpl->tpl_vars['check']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['check']->value) {
$_smarty_tpl->tpl_vars['check']->do_else = false;
?>
	<tr>
		<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value['title'], ENT_QUOTES, 'UTF-8');?>
 </td>
		<td>
			<?php if (!$_smarty_tpl->tpl_vars['check']->value['status']) {?>
			<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/install/red.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['requirement_not_satisfied']->value, ENT_QUOTES, 'UTF-8');?>
">
			<?php }?>
		</td>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</table>

<form id="install-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/install" method="post">

	<input type="hidden" name="config-php_path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['php_path']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="config-hostname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hostname']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="system-ssl_policy" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ssl_policy']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="system-basepath" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['basepath']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="system-urlpath" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urlpath']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="pass" value="4" />

	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['dbhost']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['dbuser']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['dbpass']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['dbdata']->value), 0, true);
?>

	<input id="install-submit" type="submit" name="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" />

</form>
<?php }
}
