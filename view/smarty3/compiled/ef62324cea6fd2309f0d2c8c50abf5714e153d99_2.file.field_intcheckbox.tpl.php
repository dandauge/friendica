<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:40
  from '/var/www/friendica/view/theme/frio/templates/field_intcheckbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2d0378ec9_36420649',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef62324cea6fd2309f0d2c8c50abf5714e153d99' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_intcheckbox.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b2d0378ec9_36420649 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<div class="form-group field checkbox">
		<input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[3], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[2]) {?>checked="checked"<?php }?> aria-checked="<?php if ($_smarty_tpl->tpl_vars['field']->value[2]) {?>true<?php } else { ?>false<?php }?>" aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?>
		<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[4];?>
</span>
		<?php }?>
	</div>
	<div class="clear"></div>
<?php }
}
