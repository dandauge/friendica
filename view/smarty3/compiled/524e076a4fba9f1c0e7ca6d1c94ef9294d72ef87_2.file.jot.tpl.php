<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/theme/frio/templates/jot.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291b77e04_02999066',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '524e076a4fba9f1c0e7ca6d1c94ef9294d72ef87' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/jot.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291b77e04_02999066 (Smarty_Internal_Template $_smarty_tpl) {
?><a class="btn btn-sm btn-primary pull-right" id="jotOpen" href="compose/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['posttype']->value, ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['content']->value) {?>?body=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content']->value, ENT_QUOTES, 'UTF-8');
}?>" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_post']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_post']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-pencil-square-o fa-2x"></i></a>

<div id="jot-content">
	<div id="jot-sections">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float: right;">&times;</button>

			<a href="/compose" class="btn compose-link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['compose_link_title']->value, ENT_QUOTES, 'UTF-8');?>
" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['compose_link_title']->value, ENT_QUOTES, 'UTF-8');?>
">
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			</a>

						<ul class="nav nav-tabs hidden-xs jot-nav" role="tablist" data-tabs="tabs">
								<li class="active" role="presentation">
					<a href="#profile-jot-wrapper" class="jot-text-lnk jot-nav-lnk" id="jot-text-lnk" role="tab" aria-controls="profile-jot-wrapper">
						<i class="fa fa-file-text-o" aria-hidden="true"></i>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');?>

					</a>
				</li>
				<?php if ($_smarty_tpl->tpl_vars['acl']->value) {?>
				<li role="presentation">
					<a href="#profile-jot-acl-wrapper" class="jot-perms-lnk jot-nav-lnk" id="jot-perms-lnk" role="tab" aria-controls="profile-jot-acl-wrapper">
						<i class="fa fa-shield" aria-hidden="true"></i>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shortpermset']->value, ENT_QUOTES, 'UTF-8');?>

					</a>
				</li>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['preview']->value) {?>
				<li role="presentation">
					<a href="#jot-preview-content" class="jot-preview-lnk jot-nav-lnk" id="jot-preview-lnk" role="tab" aria-controls="jot-preview-content">
						<i class="fa fa-eye" aria-hidden="true"></i>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview']->value, ENT_QUOTES, 'UTF-8');?>

					</a>
				</li>
				<?php }?>
				<li role="presentation">
					<a href="#jot-fbrowser-wrapper" class="jot-browser-lnk jot-nav-lnk" id="jot-browser-link" role="tab" aria-controls="jot-fbrowser-wrapper">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['browser']->value, ENT_QUOTES, 'UTF-8');?>

					</a>
				</li>
			</ul>

						<div class="dropdown dropdown-head dropdown-mobile-jot jot-nav hidden-lg hidden-md hidden-sm" role="menubar" data-tabs="tabs" style="float: left;">
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');?>
&nbsp;<span class="caret"></span></button>
				<ul class="dropdown-menu nav nav-pills" aria-label="submenu">
										<li role="presentation" style="display: none;">
						<button class="jot-text-lnk btn-link jot-nav-lnk jot-nav-lnk-mobile" id="jot-text-lnk-mobile" aria-controls="profile-jot-wrapper" role="menuitem"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</li>
					<?php if ($_smarty_tpl->tpl_vars['acl']->value) {?>
					<li role="presentation">
						<button class="jot-perms-lnk btn-link jot-nav-lnk jot-nav-lnk-mobile" id="jot-perms-lnk-mobile" aria-controls="profile-jot-acl-wrapper" role="menuitem"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shortpermset']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</li>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['preview']->value) {?>
					<li role="presentation">
						<button class="jot-preview-lnk btn-link jot-nav-lnk jot-nav-lnk-mobile" id="jot-preview-lnk-mobile" aria-controls="jot-preview-content" role="menuitem"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</li>
					<?php }?>
					<li role="presentation">
						<button class="jot-browser-lnk-mobile btn-link jot-nav-lnk jot-nav-lnk-mobile" id="jot-browser-lnk-mobile" aria-controls="jot-fbrowser-wrapper" role="menuitem"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['browser']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</li>
				</ul>
			</div>
		</div>

		<div id="jot-modal-body" class="modal-body">
			<form id="profile-jot-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');?>
" method="post">
				<div id="profile-jot-wrapper" aria-labelledby="jot-text-lnk" role="tabpanel" aria-hidden="false">
					<div>
						<!--<div id="profile-jot-desc" class="jothidden pull-right">&nbsp;</div>-->
					</div>

					<div id="profile-jot-banner-end"></div>

										<input type="hidden" name="jot" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['jot']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="wall" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wall']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="post_type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['posttype']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="profile_uid" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_uid']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="return" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['return_path']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="location" id="jot-location" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['defloc']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="coord" id="jot-coord" value="" />
					<input type="hidden" name="post_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<input type="hidden" name="preview" id="jot-preview" value="0" />
					<input type="hidden" name="post_id_random" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rand_num']->value, ENT_QUOTES, 'UTF-8');?>
" />
					<?php if ($_smarty_tpl->tpl_vars['notes_cid']->value) {?>
					<input type="hidden" name="contact_allow[]" value="<<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notes_cid']->value, ENT_QUOTES, 'UTF-8');?>
>" />
					<?php }?>
					<div id="jot-title-wrap"><input name="title" id="jot-title" class="jothidden jotforms form-control" type="text" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdertitle']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdertitle']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
" style="display:block;" /></div>
					<?php if ($_smarty_tpl->tpl_vars['placeholdercategory']->value) {?>
					<div id="jot-category-wrap"><input name="category" id="jot-category" class="jothidden jotforms form-control" type="text" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdercategory']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdercategory']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value, ENT_QUOTES, 'UTF-8');?>
" /></div>
					<?php }?>

										<div id="jot-text-wrap">
						<textarea rows="2" cols="64" class="profile-jot-text form-control text-autosize" id="profile-jot-text" name="body" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['share']->value, ENT_QUOTES, 'UTF-8');?>
" onFocus="jotTextOpenUI(this);" onBlur="jotTextCloseUI(this);" style="min-width:100%; max-width:100%;"><?php if ($_smarty_tpl->tpl_vars['content']->value) {
echo $_smarty_tpl->tpl_vars['content']->value;
}?></textarea>
					</div>

					<ul id="profile-jot-submit-wrapper" class="jothidden nav nav-pills">
						<li role="presentation"><button type="button" class="hidden-xs btn-link icon underline" style="cursor: pointer;" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['eduline']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['eduline']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="insertFormattingToPost('u');"><i class="fa fa-underline"></i></button></li>
						<li role="presentation"><button type="button" class="hidden-xs btn-link icon italic" style="cursor: pointer;" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editalic']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editalic']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="insertFormattingToPost('i');"><i class="fa fa-italic"></i></button></li>
						<li role="presentation"><button type="button" class="hidden-xs btn-link icon bold" style="cursor: pointer;" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edbold']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edbold']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="insertFormattingToPost('b');"><i class="fa fa-bold"></i></button></li>
						<li role="presentation"><button type="button" class="hidden-xs btn-link icon quote" style="cursor: pointer;" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edquote']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edquote']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="insertFormattingToPost('quote');"><i class="fa fa-quote-left"></i></button></li>
						<li role="presentation"><button type="button" class="btn-link icon" style="cursor: pointer;" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edurl']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edurl']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="insertFormattingToPost('url');"><i class="fa fa-link"></i></button></li>
						<li role="presentation"><button type="button" class="btn-link" id="profile-attach"  ondragenter="return linkDropper(event);" ondragover="return linkDropper(event);" ondrop="linkDrop(event);" onclick="jotGetLink();" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edattach']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-paperclip"></i></button></li>
						<li role="presentation"><button type="button" class="btn-link" id="profile-location" onclick="jotGetLocation();" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['setloc']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-map-marker" aria-hidden="true"></i></button></li>
						<!-- TODO: waiting for a better placement
						<li><button type="button" class="btn-link" id="profile-nolocation" onclick="jotClearLocation();" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['noloc']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shortnoloc']->value, ENT_QUOTES, 'UTF-8');?>
</button></li>
						-->

						<li role="presentation" class="pull-right">
							<button class="btn btn-primary" type="submit" id="profile-jot-submit" name="submit" data-loading-text="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loading']->value, ENT_QUOTES, 'UTF-8');?>
">
								<i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['share']->value, ENT_QUOTES, 'UTF-8');?>

							</button>
						</li>
						<li role="presentation" id="character-counter" class="grey jothidden text-info pull-right"></li>
						<li role="presentation" id="profile-rotator-wrapper" class="pull-right" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
							<img role="presentation" id="profile-rotator" src="images/rotator.gif" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" />
						</li>
						<li role="presentation" id="profile-jot-plugin-wrapper">
							<?php echo $_smarty_tpl->tpl_vars['jotplugins']->value;?>

						</li>
					</ul>

				</div>

				<div id="profile-jot-acl-wrapper" class="minimize" aria-labelledby="jot-perms-lnk" role="tabpanel" aria-hidden="true">
					<?php echo $_smarty_tpl->tpl_vars['acl']->value;?>

				</div>

				<div id="jot-preview-content" class="minimize" aria-labelledby="jot-preview-lnk" role="tabpanel" aria-hidden="true"></div>

				<div id="jot-preview-share" class="minimize" aria-labelledby="jot-preview-lnk" role="tabpanel" aria-hidden="true">
					<ul id="profile-jot-preview-submit-wrapper" class="jothidden nav nav-pills">
						<li role="presentation" class="pull-right">
							<button class="btn btn-primary" type="submit" id="profile-jot-preview-submit" name="submit" data-loading-text="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loading']->value, ENT_QUOTES, 'UTF-8');?>
">
								<i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['share']->value, ENT_QUOTES, 'UTF-8');?>

							</button>
						</li>
					</ul>
				</div>

				<div id="jot-fbrowser-wrapper" class="minimize" aria-labelledby="jot-browser-link" role="tabpanel" aria-hidden="true"></div>

			</form>

			<?php if ($_smarty_tpl->tpl_vars['content']->value) {
echo '<script'; ?>
 type="text/javascript">initEditor();<?php echo '</script'; ?>
><?php }?>
		</div>
	</div>
</div>


<div id="jot-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div id="jot-modal-content" class="modal-content"></div>
	</div>
</div>


<?php echo '<script'; ?>
 type="text/javascript">
	$('iframe').load(function() {
		this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
	});
<?php echo '</script'; ?>
>
<?php }
}
