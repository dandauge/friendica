<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:32:33
  from '/var/www/friendica/view/templates/install_base.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449db18c0ac9_86864745',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef5ccb6afaf93c091db767908be472921273a703' => 
    array (
      0 => '/var/www/friendica/view/templates/install_base.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_select.tpl' => 1,
    'file:field_input.tpl' => 3,
  ),
),false)) {
function content_60449db18c0ac9_86864745 (Smarty_Internal_Template $_smarty_tpl) {
?><h1><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/images/friendica-32.png"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pass']->value, ENT_QUOTES, 'UTF-8');?>
</h2>

<p>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_01']->value, ENT_QUOTES, 'UTF-8');?>
<br>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_02']->value, ENT_QUOTES, 'UTF-8');?>
<br>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_03']->value, ENT_QUOTES, 'UTF-8');?>

</p>

<form id="install-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/install" method="post">

	<input type="hidden" name="config-php_path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['php_path']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="pass" value="3" />

	<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['ssl_policy']->value), 0, false);
?>
	<br />
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hostname']->value), 0, false);
?>
	<br />
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['basepath']->value), 0, true);
?>
	<br />
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['urlpath']->value), 0, true);
?>

	<input id="install-submit" type="submit" name="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" />

</form>
<?php }
}
