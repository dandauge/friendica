<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:36:08
  from '/var/www/friendica/view/templates/profile/publish.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e886be693_71840141',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b99da75a694a0da2ac702749267217ab469c8774' => 
    array (
      0 => '/var/www/friendica/view/templates/profile/publish.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449e886be693_71840141 (Smarty_Internal_Template $_smarty_tpl) {
?>
<p id="profile-publish-desc-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pubdesc']->value, ENT_QUOTES, 'UTF-8');?>

</p>

		<div id="profile-publish-yes-wrapper-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
">
		<label id="profile-publish-yes-label-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" for="profile-publish-yes-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['str_yes']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="radio" name="profile_publish_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" id="profile-publish-yes-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['yes_selected']->value, ENT_QUOTES, 'UTF-8');?>
 value="1" />

		<div id="profile-publish-break-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" ></div>	
		</div>
		<div id="profile-publish-no-wrapper-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
">
		<label id="profile-publish-no-label-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" for="profile-publish-no-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['str_no']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="radio" name="profile_publish_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" id="profile-publish-no-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_selected']->value, ENT_QUOTES, 'UTF-8');?>
 value="0"  />

		<div id="profile-publish-end-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instance']->value, ENT_QUOTES, 'UTF-8');?>
"></div>
		</div>
<?php }
}
