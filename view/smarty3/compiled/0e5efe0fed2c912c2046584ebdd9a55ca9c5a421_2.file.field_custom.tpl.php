<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:32
  from '/var/www/friendica/view/theme/frio/templates/field_custom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1d8751916_61761154',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0e5efe0fed2c912c2046584ebdd9a55ca9c5a421' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_custom.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1d8751916_61761154 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group field custom">
	<label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
	<?php echo $_smarty_tpl->tpl_vars['field']->value[2];?>

	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
	<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
</div>
<?php }
}
