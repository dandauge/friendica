<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:40
  from '/var/www/friendica/view/templates/settings/pagetypes.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2d0314791_47500530',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '57cd739ace2ca990b0e9c6e33e63c81ac8545c1d' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/pagetypes.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_radio.tpl' => 9,
  ),
),false)) {
function content_6044b2d0314791_47500530 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['account_types']->value, ENT_QUOTES, 'UTF-8');?>
</h4>
<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['account_person']->value), 0, false);
?>
<div id="account-type-sub-0" class="pageflags">
	<h5><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value, ENT_QUOTES, 'UTF-8');?>
</h5>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['page_normal']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['page_soapbox']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['page_freelove']->value), 0, true);
?>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['account_organisation']->value), 0, true);
$_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['account_news']->value), 0, true);
?>

<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['account_community']->value), 0, true);
?>
<div id="account-type-sub-3" class="pageflags">
	<h5><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['community']->value, ENT_QUOTES, 'UTF-8');?>
</h5>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['page_community']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['page_prvgroup']->value), 0, true);
?>
</div>

<?php echo '<script'; ?>
 language="javascript" type="text/javascript">
	// This js part changes the state of page-flags radio buttons according
	// to the selected account type. For a translation of the different
	// account-types and page-flags have a look in the define section in boot.php
	var accountType = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['account_type']->value, ENT_QUOTES, 'UTF-8');?>
;

	$(document).ready(function(){
		// Hide all DIV for page-flags expet the one which belongs to the present
		// account-type
		showPageFlags(accountType);

		// Save the ID of the active page-flage
		var activeFlag = $('[id^=id_page-flags_]:checked');

		$("[id^=id_account-type_]").change(function(){
			// Since the ID of the radio buttons containing the type of
			// the account-type we catch the last character of the ID to
			// know for what account-type the radio button stands for.
			var type = this.id.substr(this.id.length - 1);

			// Hide all DIV with page-flags and show only the one which belongs
			// to the selected radio button
			showPageFlags(type);

			// Uncheck all page-flags radio buttons
			$('input:radio[name="page-flags"]').prop("checked", false);

			// If the selected account type is the active one mark the page-flag
			// radio button as checked which is already by database state
			if (accountType == type) {
				$(activeFlag).prop("checked", true);
			} else if (type == 1 || type == 2) {
				// For account-type 1 or 2 the page-flags are always set to 1
				$('#id_page-flags_1').prop("checked", true);
			} else {
				// Mark the first available page-flags radio button of the selected
				// account-type as checked
				$('#account-type-sub-' + type + ' input:radio[name="page-flags"]').first().prop("checked", true);
			}
		});
	});

	// Show/Hide the page-flags according to the selected account-type
	function showPageFlags(type) {
		$(".pageflags").hide();

		if (type == 0 || type == 3) {
			$("#account-type-sub-" + type).show();
		}
	}
<?php echo '</script'; ?>
>
<?php }
}
