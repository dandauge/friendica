<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:34:46
  from '/var/www/friendica/view/templates/install_settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e36cedf28_87172063',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '323f72716b04b07390cc51cf0c0d44450b6b9ef5' => 
    array (
      0 => '/var/www/friendica/view/templates/install_settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
    'file:field_select.tpl' => 1,
  ),
),false)) {
function content_60449e36cedf28_87172063 (Smarty_Internal_Template $_smarty_tpl) {
?>

<h1><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/images/friendica-32.png"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pass']->value, ENT_QUOTES, 'UTF-8');?>
</h2>


<form id="install-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/install" method="post">

<input type="hidden" name="config-php_path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['php_path']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="config-hostname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hostname']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="system-ssl_policy" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ssl_policy']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="system-basepath" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['basepath']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="system-urlpath" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urlpath']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="database-hostname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dbhost']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="database-username" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dbuser']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="database-password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dbpass']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="database-database" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dbdata']->value, ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="pass" value="5" />

<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['adminmail']->value), 0, false);
?> <br />
<?php echo $_smarty_tpl->tpl_vars['timezone']->value;?>
 <br />
<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['language']->value), 0, false);
?>

<input id="install-submit" type="submit" name="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" />

</form>

<?php }
}
