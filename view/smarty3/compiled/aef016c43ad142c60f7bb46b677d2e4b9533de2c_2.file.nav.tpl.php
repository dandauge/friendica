<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b879d270_44370252',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aef016c43ad142c60f7bb46b677d2e4b9533de2c' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/nav.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b879d270_44370252 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['nav']->value['userinfo']) {?>
<header>
	
	<div id="site-location"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
</div>
	<div id="banner" class="hidden-sm hidden-xs">
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['remote']) {?>
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
"><div id="remote-logo-img" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['home']->value, ENT_QUOTES, 'UTF-8');?>
"></div></a>
		<?php } else { ?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
"><div id="logo-img" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['home']->value, ENT_QUOTES, 'UTF-8');?>
"></div></a>
		<?php }?>
	</div>
</header>
<nav id="topbar-first" class="topbar">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"><!-- div for navbar width-->
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="topbar-nav" role="navigation">

								<button type="button" class="navbar-toggle collapsed pull-right" data-toggle="offcanvas" data-target="#myNavmenu" aria-controls="myNavmenu" aria-haspopup="true">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
				</button>
				<button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#search-mobile" aria-expanded="false" aria-controls="search-mobile">
					<span class="sr-only">Toggle Search</span>
					<i class="fa fa-search" aria-hidden="true" style="color:#FFF;"></i>
				</button>
				<button type="button" class="navbar-toggle collapsed pull-left visible-sm visible-xs" data-toggle="offcanvas" data-target="aside" aria-haspopup="true">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
				</button>

								<ul class="nav navbar-left" role="menubar">
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?>
					<li class="nav-segment">
						<a accesskey="n" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['network'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-lg fa-th" aria-hidden="true"></i><span id="net-update" class="nav-network-badge badge nav-notification"></span></a>
					</li>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?>
					<li class="nav-segment">
						<a accesskey="p" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['home'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-lg fa-home" aria-hidden="true"></i><span id="home-update" class="nav-home-badge badge nav-notification"></span></a>
					</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['nav']->value['community']) {?>
					<li class="nav-segment">
						<a accesskey="c" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['community'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-lg fa-bullseye" aria-hidden="true"></i></a>
					</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
					<li class="nav-segment hidden-xs">
						<a accesskey="m" id="nav-messages-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['messages'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i><span id="mail-update" class="nav-mail-badge badge nav-notification"></span></a>
					</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['nav']->value['events']) {?>
					<li class="nav-segment hidden-xs">
						<a accesskey="e" id="nav-events-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][1], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['events'][1], ENT_QUOTES, 'UTF-8');?>
" class="nav-menu"><i class="fa fa-lg fa-calendar"></i></a>
					</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?>
					<li class="nav-segment hidden-xs">
						<a accesskey="k" id="nav-contacts-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
"class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['contacts'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-users fa-lg"></i></a>
					</li>
					<?php }?>

										<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
						<li id="nav-notification" class="nav-segment dropdown" role="presentation">
							<button id="nav-notifications-menu-btn" class="btn-link dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false" aria-controls="nav-notifications-menu">
								<span id="notification-update" class="nav-notification-badge badge nav-notification"></span>
								<i class="fa fa-bell fa-lg" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"></i>
							</button>
														<ul id="nav-notifications-menu" class="dropdown-menu menu-popup" role="menu" aria-labelledby="nav-notifications-menu-btn">
																<li role="presentation" id="nav-notifications-mark-all" class="dropdown-header">
									<div class="arrow"></div>
									<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>

									<div class="dropdown-header-link">
										<button role="menuitem" type="button" class="btn-link" onclick="notificationMarkAll();" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][1], ENT_QUOTES, 'UTF-8');?>
</button>
									</div>

								</li>

								<li role="presentation">
									<p role="menuitem" class="text-muted"><i><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emptynotifications']->value, ENT_QUOTES, 'UTF-8');?>
</i></p>
								</li>
							</ul>
						</li>
					<?php }?>

				</ul>
			</div>

						<div class="topbar-actions pull-right">
				<ul class="nav">

										<?php if ($_smarty_tpl->tpl_vars['nav']->value['search']) {?>
					<li id="search-box" class="hidden-xs">
							<form class="navbar-form" role="search" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
">
								<!-- <img class="hidden-xs" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');?>
" style="max-width:33px; max-height:33px; min-width:33px; min-height:33px; width:33px; height:33px;"> -->
								<div class="form-group form-group-search">
									<input accesskey="s" id="nav-search-input-field" class="form-control form-search" type="text" name="q" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_hint']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
">
									<button class="btn btn-default btn-sm form-button-search" type="submit"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</button>
								</div>
							</form>
					</li>
					<?php }?>

										<?php if ($_smarty_tpl->tpl_vars['nav']->value['userinfo']) {?>
					<li id="nav-user-linkmenu" class="dropdown account nav-menu hidden-xs">
						<button accesskey="u" id="main-menu" class="btn-link dropdown-toggle nav-avatar" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false" aria-controls="nav-user-menu">
							<div aria-hidden="true" class="user-title pull-left hidden-xs hidden-sm hidden-md">
								<strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');?>
</strong><br>
								<?php if ($_smarty_tpl->tpl_vars['nav']->value['remote']) {?><span class="trunctate"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['remote'], ENT_QUOTES, 'UTF-8');?>
</span><?php }?>
							</div>

							<img id="avatar" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');?>
">
							<span class="caret"></span>
						</button>

												<ul id="nav-user-menu" class="dropdown-menu pull-right menu-popup" role="menu" aria-labelledby="main-menu">
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['remote']) {
if ($_smarty_tpl->tpl_vars['nav']->value['sitename']) {?>
							<li id="nav-sitename" role="menuitem"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['sitename'], ENT_QUOTES, 'UTF-8');?>
</li>
							<li role="presentation" class="divider"></li>
							<?php }
}?>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nav']->value['usermenu'], 'usermenu');
$_smarty_tpl->tpl_vars['usermenu']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['usermenu']->value) {
$_smarty_tpl->tpl_vars['usermenu']->do_else = false;
?>
							<li role="presentation"><a role="menuitem" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
							<li role="presentation" class="divider"></li>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
							<li role="presentation"><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-bell fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
							<li role="presentation"><a role="menuitem" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['messages'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-envelope fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
 <span id="mail-update-li" class="nav-mail-badge badge nav-notification"></span></a></li>
							<?php }?>
							<li role="presentation" class="divider"></li>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?>
							<li role="presentation"><a role="menuitem" id="nav-menu-contacts-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-users fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?>
							<li role="presentation"><a role="menuitem" id="nav-delegation-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['delegation'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-flag fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<li role="presentation"><a role="menuitem" id="nav-directory-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-sitemap fa-fw" aria-hidden="true"></i><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<li role="presentation" class="divider"></li>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['apps']) {?>
							<li role="presentation"><a role="menuitem" id="nav-apps-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-puzzle-piece fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][1], ENT_QUOTES, 'UTF-8');?>
</a>
							<li role="presentation" class="divider"></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['help']) {?>
							<li role="presentation"><a role="menuitem" id="nav-help-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-question-circle fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?>
							<li role="presentation"><a role="menuitem" id="nav-settings-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?>
							<li role="presentation"><a accesskey="a" role="menuitem" id="nav-admin-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-user-secret fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['tos']) {?>
							<li role="presentation" class="divider"></li>
							<li role="presentation"><a role="menuitem" id="nav-tos-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
							<li role="presentation" class="divider"></li>
							<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?>
							<li role="presentation"><a role="menuitem" id="nav-logout-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa fa-sign-out fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php } else { ?>
							<li role="presentation"><a role="menuitem" id="nav-login-link" class="nav-login-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-power-off fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
							<?php }?>
						</ul>
					</li>					<?php }?>

				<!-- Language selector, I do not find it relevant, activate if necessary.
					<li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['langselector']->value, ENT_QUOTES, 'UTF-8');?>
</li>
				-->
				</ul>
			</div>
						<div id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-right offcanvas">
				<div class="nav-container">
					<ul role="menu" class="list-group">
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['remote']) {
if ($_smarty_tpl->tpl_vars['nav']->value['sitename']) {?>
						<li role="menuitem" class="nav-sitename list-group-item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['sitename'], ENT_QUOTES, 'UTF-8');?>
</li>
						<?php }
}?>
						<li role="presentation" class="list-group-item"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');?>
" style="max-width:15px; max-height:15px; min-width:15px; min-height:15px; width:15px; height:15px;"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['nav']->value['remote']) {?> (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['remote'], ENT_QUOTES, 'UTF-8');?>
)<?php }?></li>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nav']->value['usermenu'], 'usermenu');
$_smarty_tpl->tpl_vars['usermenu']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['usermenu']->value) {
$_smarty_tpl->tpl_vars['usermenu']->do_else = false;
?>
						<li role="menuitem" class="list-group-item"><a role="menuitem" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-bell fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-users fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['messages'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-envelope fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['delegation'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-flag fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][3], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-user-secret fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa fa-sign-out fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php } else { ?>
						<li role="presentation" class="list-group-item"><a role="menuitem" class="nav-login-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" ><i class="fa fa-power-off fa-fw" aria-hidden="true"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
						<?php }?>
					</ul>
				</div>
			</div><!--/.sidebar-offcanvas-->
		</div><!-- end of div for navbar width-->
	</div><!-- /.container -->
</nav><!-- /.navbar -->
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['nav']->value['userinfo'] == '') {?>
<nav class="navbar navbar-fixed-top">
	<div class="container">
		<div class="navbar-header pull-left">
			<a class="navbar-brand" href="#"><div id="navbrand-container">
				<div id="logo-img"></div>
				<div id="navbar-brand-text"> Friendica</div></div>
			</a>
		</div>
		<div class="pull-right">
			<ul class="nav navbar-nav navbar-right">
				<li role="presentation">
					<a href="login?mode=none" id="nav-login"
						data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
">
							<i class="fa fa-sign-in fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				<li role="presentation">
					<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][0], ENT_QUOTES, 'UTF-8');?>
" id="nav-about" data-toggle="tooltip" aria-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][3], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['about'][3], ENT_QUOTES, 'UTF-8');?>
">
						<i class="fa fa-info fa-fw" aria-hidden="true"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<?php }?>

<div id="search-mobile" class="hidden-lg hidden-md collapse">
	<form class="navbar-form" role="search" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
">
		<!-- <img class="hidden-xs" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['userinfo']['name'], ENT_QUOTES, 'UTF-8');?>
" style="max-width:33px; max-height:33px; min-width:33px; min-height:33px; width:33px; height:33px;"> -->
		<div class="form-group form-group-search">
			<input id="nav-search-input-field-mobile" class="form-control form-search" type="text" name="q" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_hint']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
">
			<button class="btn btn-default btn-sm form-button-search" type="submit"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</button>
		</div>
	</form>
</div>

<div id="topbar-second" class="topbar">
	<div class="container">
		<div class="col-lg-3 col-md-3 hidden-sm hidden-xs" id="nav-short-info"></div>
		<div class="col-lg-7 col-md-7 col-sm-11 col-xs-10" id="tabmenu"></div>
		<div class="col-lg-2 col-md-2 col-sm-1 col-xs-2" id="navbar-button"></div>
	</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
<ul id="nav-notifications-template" class="media-list" style="display:none;" rel="template">
	<li class="{4} notif-entry">
		<div class="notif-entry-wrapper media">
			<div class="notif-photo-wrapper media-object pull-left" aria-hidden="true"><a href="{6}" class="userinfo click-card" tabIndex="-1"><img data-src="{1}"></a></div>
			<a href="{0}" class="notif-desc-wrapper media-body">
				{2}
				<div><time class="notif-when time" data-toggle="tooltip" title="{5}">{3}</time></div>
			</a>
		</div>
	</li>
</ul>
<?php }?>

<svg id="friendica-logo-mask" x="0px" y="0px" width="0px" height="0px" viewBox="0 0 250 250">
	<defs>
		<mask id="logo-mask" maskUnits="objectBoundingBox" maskContentUnits="objectBoundingBox">
			<path style="fill-rule:evenodd;clip-rule:evenodd;fill:#ffffff;" d="M0.796,0L0.172,0.004C0.068,0.008,0.008,0.068,0,0.172V0.824c0,0.076,0.06,0.16,0.168,0.172h0.652c0.072,0,0.148-0.06,0.172-0.144V0.14C1,0.06,0.908,0,0.796,0zM0.812,0.968H0.36v-0.224h0.312v-0.24H0.36V0.3h0.316l0-0.264l0.116-0c0.088,0,0.164,0.044,0.164,0.096l0,0.696C0.96,0.912,0.876,0.968,0.812,0.968z"></path>
		</mask>
	</defs>
</svg>
<?php }
}
