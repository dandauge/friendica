<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:09
  from '/var/www/friendica/view/templates/field_radio.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b185698aa9_77230103',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cec66858ebe80642664bb7745c0bafefb6145dcb' => 
    array (
      0 => '/var/www/friendica/view/templates/field_radio.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b185698aa9_77230103 (Smarty_Internal_Template $_smarty_tpl) {
?>	<div class='field radio'>
		<label for='id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="radio" name='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
' id='id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
' value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?>checked<?php }?> aria-describedby=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
_tip'>
		<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class='field_help' role='tooltip' id='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
_tip'><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
		<?php }?>
	</div>
<?php }
}
