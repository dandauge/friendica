<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:03:59
  from '/var/www/friendica/view/templates/admin/dbsync/structure_check.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b31f191900_23234418',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f83eee18da6c9d2be2b92938d5e78e5fbecd81c' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/dbsync/structure_check.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b31f191900_23234418 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="adminpage">
	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value, ENT_QUOTES, 'UTF-8');?>
</h2>

	<p><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/dbsync/check"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['check']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>

	<hr />
</div>
<?php }
}
