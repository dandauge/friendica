<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:35:53
  from '/var/www/friendica/view/templates/install_finished.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e798e0f69_18664584',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e7145301a722f6b60f878c6a17ebb3afb0294ab' => 
    array (
      0 => '/var/www/friendica/view/templates/install_finished.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449e798e0f69_18664584 (Smarty_Internal_Template $_smarty_tpl) {
?><h1><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/images/friendica-32.png"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pass']->value, ENT_QUOTES, 'UTF-8');?>
</h2>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['checks']->value, 'check');
$_smarty_tpl->tpl_vars['check']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['check']->value) {
$_smarty_tpl->tpl_vars['check']->do_else = false;
?>
<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/install/red.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['requirement_not_satisfied']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php echo $_smarty_tpl->tpl_vars['check']->value['title'];?>

<textarea rows="24" cols="80"><?php echo $_smarty_tpl->tpl_vars['check']->value['help'];?>
</textarea>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php echo $_smarty_tpl->tpl_vars['text']->value;?>

<?php }
}
