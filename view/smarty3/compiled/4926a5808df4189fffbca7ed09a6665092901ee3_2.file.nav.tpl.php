<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:11:36
  from '/var/www/friendica/view/theme/quattro/templates/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4e8f0d5c1_91463383',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4926a5808df4189fffbca7ed09a6665092901ee3' => 
    array (
      0 => '/var/www/friendica/view/theme/quattro/templates/nav.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b4e8f0d5c1_91463383 (Smarty_Internal_Template $_smarty_tpl) {
?><header>
	
	<div id="site-location"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
</div>
	<div id="banner"><?php echo $_smarty_tpl->tpl_vars['banner']->value;?>
</div>
</header>
<nav>
	<ul>
		<?php if ($_smarty_tpl->tpl_vars['userinfo']->value) {?>
			<li id="nav-user-linkmenu" class="nav-menu-icon"><a accesskey="u" href="#" rel="#nav-user-menu" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['userinfo']->value['icon'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['userinfo']->value['name'], ENT_QUOTES, 'UTF-8');?>
"></a>
				<ul id="nav-user-menu" class="menu-popup">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nav']->value['usermenu'], 'usermenu');
$_smarty_tpl->tpl_vars['usermenu']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['usermenu']->value) {
$_smarty_tpl->tpl_vars['usermenu']->do_else = false;
?>
						<li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usermenu']->value[1], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>	
				</ul>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['community']) {?>
			<li id="nav-community-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['community'], ENT_QUOTES, 'UTF-8');?>
">
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][1], ENT_QUOTES, 'UTF-8');?>
</a>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?>
			<li id="nav-network-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['network'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="n" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<span id="net-update" class="nav-notification"></span>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?>
			<li id="nav-home-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['home'], ENT_QUOTES, 'UTF-8');?>
">
				<a accesskey="p" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<span id="home-update" class="nav-notification"></span>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['introductions']) {?>
			<li id="nav-introductions-link" class="nav-menu-icon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['introductions'], ENT_QUOTES, 'UTF-8');?>
">
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][3], ENT_QUOTES, 'UTF-8');?>
" >
				    <span class="icon s22 intro"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				</a>
				<span id="intro-update" class="nav-notification"></span>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
			<li id="nav-messages-link" class="nav-menu-icon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['messages'], ENT_QUOTES, 'UTF-8');?>
">
				<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" >
				    <span class="icon s22 mail"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				</a>
				<span id="mail-update" class="nav-notification"></span>
			</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
			<li  id="nav-notifications-linkmenu" class="nav-menu-icon"><a accesskey="f" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
" rel="#nav-notifications-menu" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"><span class="icon s22 notify"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</span></a>
				<span id="notification-update" class="nav-notification"></span>
				<ul id="nav-notifications-menu" class="menu-popup">
					<!-- TODO: better icons! -->
					<li id="nav-notifications-mark-all" class="toolbar"><a href="#" onclick="notificationMarkAll(); return false;" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][3], ENT_QUOTES, 'UTF-8');?>
"><span class="icon s10 edit"></span></a></a><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][1], ENT_QUOTES, 'UTF-8');?>
"><span class="icon s10 plugin"></span></a></li>
					<li class="empty"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emptynotifications']->value, ENT_QUOTES, 'UTF-8');?>
</li>
				</ul>
			</li>
		<?php }?>

		<li id="nav-site-linkmenu" class="nav-menu-icon"><a href="#" rel="#nav-site-menu"><span class="icon s22 gear">Site</span></a>
			<ul id="nav-site-menu" class="menu-popup">
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>

				<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?><li><a accesskey="a" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>

				<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?><li><a class="menu-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['login']) {?><li><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a><li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['nav']->value['tos']) {?><li><a class="menu-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			</ul>
		</li>

		<?php if ($_smarty_tpl->tpl_vars['nav']->value['help']) {?> 
		<li id="nav-help-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['help'], ENT_QUOTES, 'UTF-8');?>
">
			<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][2], ENT_QUOTES, 'UTF-8');?>
" target="friendica-help" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][1], ENT_QUOTES, 'UTF-8');?>
</a>
		</li>
		<?php }?>

		<li id="nav-search-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['search'], ENT_QUOTES, 'UTF-8');?>
">
			<a accesskey="s" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</a>
		</li>
		<li id="nav-directory-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['directory'], ENT_QUOTES, 'UTF-8');?>
">
			<a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][1], ENT_QUOTES, 'UTF-8');?>
</a>
		</li>
		
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['apps']) {?>
			<li id="nav-apps-link" class="nav-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['apps'], ENT_QUOTES, 'UTF-8');?>
">
				<a class=" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][2], ENT_QUOTES, 'UTF-8');?>
" href="#" rel="#nav-apps-menu" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<ul id="nav-apps-menu" class="menu-popup">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['apps']->value, 'ap');
$_smarty_tpl->tpl_vars['ap']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ap']->value) {
$_smarty_tpl->tpl_vars['ap']->do_else = false;
?>
					<li><?php echo $_smarty_tpl->tpl_vars['ap']->value;?>
</li>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</ul>
			</li>
		<?php }?>
	</ul>

</nav>
<ul id="nav-notifications-template" style="display:none;" rel="template">
	<li class="{4}"><a href="{0}" title="{5}"><img data-src="{1}">{2} <span class="notif-when">{3}</span></a></li>
</ul>

<div style="position: fixed; top: 3px; left: 5px; z-index:9999"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['langselector']->value, ENT_QUOTES, 'UTF-8');?>
</div>
<?php }
}
