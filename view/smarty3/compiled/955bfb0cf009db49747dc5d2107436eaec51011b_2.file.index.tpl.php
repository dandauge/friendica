<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:10:24
  from '/var/www/friendica/view/theme/frio/templates/settings/profile/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4a0591a63_20574791',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '955bfb0cf009db49747dc5d2107436eaec51011b' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/profile/index.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 8,
    'file:field_textarea.tpl' => 1,
    'file:settings/profile/field/edit.tpl' => 1,
  ),
),false)) {
function content_6044b4a0591a63_20574791 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

		<div id="profile-edit-links">
		<ul class="nav nav-pills preferences">
			<li class="dropdown pull-right">
				<button type="button" class="btn btn-link btn-sm dropdown-toggle" id="profile-edit-links-dropdown" data-toggle="dropdown" aria-expanded="false">
					<i class="fa fa-angle-down" aria-hidden="true"></i>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_action']->value, ENT_QUOTES, 'UTF-8');?>

				</button>
				<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="profile-edit-links-dropdown">
					<li role="presentation"><a role="menuitem" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profpiclink']->value, ENT_QUOTES, 'UTF-8');?>
" id="profile-photo_upload-link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profpic']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profpic']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
					<li role="presentation"><button role="menuitem" type="button" class="btn-link" id="profile-photo_upload-link-new" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_profile_photo']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="openClose('profile-photo-upload-section');"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_profile_photo']->value, ENT_QUOTES, 'UTF-8');?>
</button></li>
					<li role="presentation" class="divider"></li>
					<li role="presentation"><a role="menuitem" href="profile/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nickname']->value, ENT_QUOTES, 'UTF-8');?>
/profile" id="profile-edit-view-link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['viewprof']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['viewprof']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
				</ul>
			</li>
		</ul>
	</div>

	<div id="profile-edit-links-end"></div>

	<form enctype="multipart/form-data" action="settings/profile/photo" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token_photo']->value, ENT_QUOTES, 'UTF-8');?>
">

		<div id="profile-photo-upload-section" class="panel">
			<a id="profile-photo-upload-close" class="close pull-right" onclick="openClose('profile-photo-upload-section');"><i class="fa fa-times" aria-hidden="true"></i></a>
			<div id="profile-photo-upload-wrapper">
				<label id="profile-photo-upload-label" for="profile-photo-upload"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_profile_photo']->value, ENT_QUOTES, 'UTF-8');?>
:</label>
				<input name="userfile" type="file" id="profile-photo-upload" size="48" />
			</div>

			<div class="profile-edit-submit-wrapper pull-right">
				<button type="submit" name="submit" class="profile-edit-submit-button btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
			</div>
			<div class="clear"></div>
		</div>
	</form>

	
	<form id="profile-edit-form" name="form1" action="" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<div class="panel-group panel-group-settings" id="profile-edit-wrapper" role="tablist" aria-multiselectable="true">
						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="personal">
					<h2>
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#profile-edit-wrapper" href="#personal-collapse" aria-expanded="true" aria-controls="personal-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_personal_section']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
								<div id="personal-collapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="personal">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['name']->value), 0, false);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_textarea.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['about']->value), 0, false);
?>

						<?php echo $_smarty_tpl->tpl_vars['dob']->value;?>


						<?php echo $_smarty_tpl->tpl_vars['hide_friends']->value;?>

					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="location">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#profile-edit-wrapper" href="#location-collapse" aria-expanded="false" aria-controls="location-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_location_section']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="location-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="location">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['address']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['locality']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['postal_code']->value), 0, true);
?>

						<div id="profile-edit-country-name-wrapper" class="form-group field select">
							<label id="profile-edit-country-name-label" for="profile-edit-country-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country_name']->value[1], ENT_QUOTES, 'UTF-8');?>
 </label>
							<select name="country_name" id="profile-edit-country-name" class="form-control" onChange="Fill_States('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value[2], ENT_QUOTES, 'UTF-8');?>
');">
								<option selected="selected"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country_name']->value[2], ENT_QUOTES, 'UTF-8');?>
</option>
								<option>temp</option>
							</select>
						</div>
						<div class="clear"></div>

						<div id="profile-edit-region-wrapper" class="form-group field select">
							<label id="profile-edit-region-label" for="profile-edit-region"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value[1], ENT_QUOTES, 'UTF-8');?>
 </label>
							<select name="region" id="profile-edit-region" class="form-control" onChange="Update_Globals();">
								<option selected="selected"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value[2], ENT_QUOTES, 'UTF-8');?>
</option>
								<option>temp</option>
							</select>
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="miscellaneous">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#profile-edit-wrapper" href="#miscellaneous-collapse" aria-expanded="false" aria-controls="miscellaneous-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_miscellaneous_section']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="miscellaneous-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="miscellaneous">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['homepage']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['xmpp']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['pub_keywords']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['prv_keywords']->value), 0, true);
?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="custom-fields">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#profile-edit-wrapper" href="#custom-fields-collapse" aria-expanded="false" aria-controls="custom-fields-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_custom_fields_section']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="custom-fields-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="custom-fields">
					<div class="panel-body">
						<?php echo $_smarty_tpl->tpl_vars['custom_fields_description']->value;?>

						<div id="profile-custom-fields">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['custom_fields']->value, 'custom_field');
$_smarty_tpl->tpl_vars['custom_field']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['custom_field']->value) {
$_smarty_tpl->tpl_vars['custom_field']->do_else = false;
?>
							<?php $_smarty_tpl->_subTemplateRender("file:settings/profile/field/edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('profile_field'=>$_smarty_tpl->tpl_vars['custom_field']->value), 0, true);
?>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
	Fill_Country('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country_name']->value[2], ENT_QUOTES, 'UTF-8');?>
');
	Fill_States('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value[2], ENT_QUOTES, 'UTF-8');?>
');

	// initiale autosize for the textareas
	autosize($("textarea.text-autosize"));
<?php echo '</script'; ?>
>
<?php }
}
