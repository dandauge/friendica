<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/common_tabs.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f3714df6_87560094',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '93aab2b0ea04f0a74a516b08fd7ddb345297d69b' => 
    array (
      0 => '/var/www/friendica/view/templates/common_tabs.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f3714df6_87560094 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul role="menubar" class="tabs">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tabs']->value, 'tab');
$_smarty_tpl->tpl_vars['tab']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->do_else = false;
?>
		<li role="menuitem" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['tab']->value['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?> class="tab button <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['sel'], ENT_QUOTES, 'UTF-8');?>
"<?php if ($_smarty_tpl->tpl_vars['tab']->value['title']) {?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></li>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>
<?php }
}
