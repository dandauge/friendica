<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:45
  from '/var/www/friendica/view/theme/vier/templates/communityhome.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f5c46306_02906209',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1888023330ba033c8a47d1ffe540ce196ad86afe' => 
    array (
      0 => '/var/www/friendica/view/theme/vier/templates/communityhome.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:widget/peoplefind.tpl' => 1,
  ),
),false)) {
function content_6044b0f5c46306_02906209 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['page']->value) {?>
<div id="right_pages" class="widget">
<div><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</div>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['comunity_profiles_title']->value) {?>
<div id="right_profiles" class="widget">
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comunity_profiles_title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<div id='lastusers-wrapper' class='items-wrapper'>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comunity_profiles_items']->value, 'i');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
	<?php echo $_smarty_tpl->tpl_vars['i']->value;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<div class="clear"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['helpers']->value) {?>
<div id="right_helpers" class="widget">
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['helpers']->value['title'][1], ENT_QUOTES, 'UTF-8');?>
</h3>
<ul role="menu">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['helpers_items']->value, 'i');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
	<?php echo $_smarty_tpl->tpl_vars['i']->value;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['con_services']->value) {?>
<div id="right_services" class="widget">
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['con_services']->value['title'][1], ENT_QUOTES, 'UTF-8');?>
</h3>
<div id="right_services_icons">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['connector_items']->value, 'i');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
	<?php echo $_smarty_tpl->tpl_vars['i']->value;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['nv']->value) {
$_smarty_tpl->_subTemplateRender('file:widget/peoplefind.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('nv'=>$_smarty_tpl->tpl_vars['nv']->value), 0, false);
}?>

<?php if ($_smarty_tpl->tpl_vars['lastusers_title']->value) {?>
<div id="right_lastusers" class="widget">
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lastusers_title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<div id='lastusers-wrapper' class='items-wrapper'>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lastusers_items']->value, 'i');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
	<?php echo $_smarty_tpl->tpl_vars['i']->value;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<div class="clear"></div>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['activeusers_title']->value) {?>
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['activeusers_title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<div class='items-wrapper'>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['activeusers_items']->value, 'i');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
	<?php echo $_smarty_tpl->tpl_vars['i']->value;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php }
}
}
