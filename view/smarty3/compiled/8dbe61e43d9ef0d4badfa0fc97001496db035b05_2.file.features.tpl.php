<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:10:41
  from '/var/www/friendica/view/theme/frio/templates/settings/features.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4b1330189_39988678',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8dbe61e43d9ef0d4badfa0fc97001496db035b05' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/features.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_checkbox.tpl' => 1,
  ),
),false)) {
function content_6044b4b1330189_39988678 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<form action="settings/features" method="post" autocomplete="off">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
				<div class="panel-group panel-group-settings" id="settings" role="tablist" aria-multiselectable="true">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['features']->value, 'f', false, 'g');
$_smarty_tpl->tpl_vars['f']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['g']->value => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->do_else = false;
?>
			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['g']->value, ENT_QUOTES, 'UTF-8');?>
-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['g']->value, ENT_QUOTES, 'UTF-8');?>
-settings-content" aria-expanded="true" aria-controls="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['g']->value, ENT_QUOTES, 'UTF-8');?>
-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value[0], ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['g']->value, ENT_QUOTES, 'UTF-8');?>
-settings-content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['g']->value, ENT_QUOTES, 'UTF-8');?>
-settings-title">
					<div class="panel-body">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['f']->value[1], 'fcat');
$_smarty_tpl->tpl_vars['fcat']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['fcat']->value) {
$_smarty_tpl->tpl_vars['fcat']->do_else = false;
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['fcat']->value), 0, true);
?>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>

	</form>
</div>
<?php }
}
