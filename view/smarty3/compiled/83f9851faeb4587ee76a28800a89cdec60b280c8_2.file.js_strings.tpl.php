<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/js_strings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b87277c1_92026894',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '83f9851faeb4587ee76a28800a89cdec60b280c8' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/js_strings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b87277c1_92026894 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
	var updateInterval = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['update_interval']->value, ENT_QUOTES, 'UTF-8');?>
;

	var localUser = <?php if ($_smarty_tpl->tpl_vars['local_user']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['local_user']->value, ENT_QUOTES, 'UTF-8');
} else { ?>false<?php }?>;
	var aStr = {
		'delitem'     : "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delitem']->value, ENT_QUOTES, 'UTF-8');?>
",
	};
<?php echo '</script'; ?>
>
<?php }
}
