<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:06
  from '/var/www/friendica/view/templates/admin/item/source.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b18258e527_22124980',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '385ae0dad9111a49d7a0b1f8af4ad04696e91cbc' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/item/source.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
  ),
),false)) {
function content_6044b18258e527_22124980 (Smarty_Internal_Template $_smarty_tpl) {
?><h2>Item Source</h2>
<form action="admin/item/source" method="get" class="panel panel-default">
	<div class="panel-body">
		<div class="form-group">
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['guid']->value), 0, false);
?>
		</div>
		<p><button type="submit" class="btn btn-primary">Submit</button></p>
	</div>
</form>

<?php if ($_smarty_tpl->tpl_vars['source']->value) {?>
<div class="itemsource-results">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Item Id</h3>
		</div>
		<div class="panel-body">
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_id']->value, ENT_QUOTES, 'UTF-8');?>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Item URI</h3>
		</div>
		<div class="panel-body">
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_uri']->value, ENT_QUOTES, 'UTF-8');?>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Terms</h3>
		</div>
		<div class="panel-body">
			<table class="table table-condensed table-striped">
				<tr>
					<th>Type</th>
					<th>Term</th>
					<th>URL</th>
				</tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['terms']->value, 'term');
$_smarty_tpl->tpl_vars['term']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['term']->value) {
$_smarty_tpl->tpl_vars['term']->do_else = false;
?>
				<tr>
					<td>
			<?php if ($_smarty_tpl->tpl_vars['term']->value['type'] == 1) {?>Tag<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['term']->value['type'] == 2) {?>Mention<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['term']->value['type'] == 8) {?>Implicit Mention<?php }?>
					</td>
					<td>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['term']->value['name'], ENT_QUOTES, 'UTF-8');?>

					</td>
					<td>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['term']->value['url'], ENT_QUOTES, 'UTF-8');?>

					</td>
				</tr>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Source</h3>
		</div>
		<pre><code class="language-php"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');?>
</code></pre>
	</div>
</div>
<?php }
}
}
