<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/field_input.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b86e9856_71479856',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c2b916171c15076eceab17f9059fdf112d34b13' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_input.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b86e9856_71479856 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<div id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_wrapper" class="form-group field input">
	<?php if (!(isset($_smarty_tpl->tpl_vars['label']->value)) || $_smarty_tpl->tpl_vars['label']->value != false) {?>
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="label_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->tpl_vars['field']->value[1];
if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> <span class="required" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');?>
">*</span><?php }?></label>
	<?php }?>
		<input class="form-control" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" type="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['field']->value[6])===null||$tmp==='' ? 'text' : $tmp), ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
"<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> required<?php }
if ($_smarty_tpl->tpl_vars['field']->value[5] == "autofocus") {?> autofocus<?php } elseif ($_smarty_tpl->tpl_vars['field']->value[5]) {?> <?php echo $_smarty_tpl->tpl_vars['field']->value[5];
}?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
		<div class="clear"></div>
	</div>
<?php }
}
