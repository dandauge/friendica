<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/jot.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f3783d62_92464711',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '367b8f662b5b865e9019d810e8a99887c3b98fd5' => 
    array (
      0 => '/var/www/friendica/view/templates/jot.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f3783d62_92464711 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="profile-jot-wrapper" >
	<div id="profile-jot-banner-wrapper">
		<div id="profile-jot-desc" >&nbsp;</div>
		<div id="character-counter" class="grey"></div>
	</div>
	<div id="profile-jot-banner-end"></div>

	<form id="profile-jot-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');?>
" method="post" >
		<input type="hidden" name="wall" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wall']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="post_type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['posttype']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="profile_uid" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_uid']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="return" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['return_path']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="location" id="jot-location" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['defloc']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="coord" id="jot-coord" value="" />
		<input type="hidden" name="post_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="preview" id="jot-preview" value="0" />
		<input type="hidden" name="post_id_random" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rand_num']->value, ENT_QUOTES, 'UTF-8');?>
" />
		<?php if ($_smarty_tpl->tpl_vars['notes_cid']->value) {?>
		<input type="hidden" name="contact_allow[]" value="<<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notes_cid']->value, ENT_QUOTES, 'UTF-8');?>
>" />
		<?php }?>
		<div id="jot-title-wrap"><input name="title" id="jot-title" type="text" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdertitle']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
" class="jothidden" style="display:none"></div>
		<?php if ($_smarty_tpl->tpl_vars['placeholdercategory']->value) {?>
		<div id="jot-category-wrap"><input name="category" id="jot-category" type="text" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['placeholdercategory']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value, ENT_QUOTES, 'UTF-8');?>
" class="jothidden" style="display:none" /></div>
		<?php }?>
		<div id="jot-text-wrap">
		<img id="profile-jot-text-loading" src="images/rotator.gif" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" />
		<textarea rows="5" cols="64" class="profile-jot-text" id="profile-jot-text" name="body" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['share']->value, ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['content']->value) {
echo $_smarty_tpl->tpl_vars['content']->value;
}?></textarea>
		</div>

<div id="profile-jot-submit-wrapper" class="jothidden">
	<input type="submit" id="profile-jot-submit" name="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['share']->value, ENT_QUOTES, 'UTF-8');?>
" />

	<div id="profile-upload-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<div id="wall-image-upload-div" ><a href="#" onclick="return false;" id="wall-image-upload" class="icon camera" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['upload']->value, ENT_QUOTES, 'UTF-8');?>
"></a></div>
	</div>
	<div id="profile-attach-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<div id="wall-file-upload-div" ><a href="#" onclick="return false;" id="wall-file-upload" class="icon attach" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attach']->value, ENT_QUOTES, 'UTF-8');?>
"></a></div>
	</div>

	<div id="profile-link-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" ondragenter="linkdropper(event);" ondragover="linkdropper(event);" ondrop="linkdrop(event);" >
		<a id="profile-link" class="icon link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['weblink']->value, ENT_QUOTES, 'UTF-8');?>
" ondragenter="return linkdropper(event);" ondragover="return linkdropper(event);" ondrop="linkdrop(event);" onclick="jotGetLink(); return false;"></a>
	</div>
	<div id="profile-video-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<a id="profile-video" class="icon video" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="jotVideoURL();return false;"></a>
	</div>
	<div id="profile-audio-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<a id="profile-audio" class="icon audio" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['audio']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="jotAudioURL();return false;"></a>
	</div>
	<div id="profile-location-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<a id="profile-location" class="icon globe" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['setloc']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="jotGetLocation();return false;"></a>
	</div>
	<div id="profile-nolocation-wrapper" style="display: none;" >
		<a id="profile-nolocation" class="icon noglobe" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['noloc']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="jotClearLocation();return false;"></a>
	</div>

	<div id="profile-jot-perms" class="profile-jot-perms" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pvisit']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<a href="#profile-jot-acl-wrapper" id="jot-perms-icon" class="icon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lockstate']->value, ENT_QUOTES, 'UTF-8');?>
"  title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['permset']->value, ENT_QUOTES, 'UTF-8');?>
" ></a><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bang']->value, ENT_QUOTES, 'UTF-8');?>

	</div>

	<!-- <?php if ($_smarty_tpl->tpl_vars['preview']->value) {?><span onclick="preview_post();" id="jot-preview-link" class="fakelink"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview']->value, ENT_QUOTES, 'UTF-8');?>
</span><?php }?> -->
	<?php if ($_smarty_tpl->tpl_vars['preview']->value) {?><input type="submit" onclick="preview_post(); return false;" id="jot-preview-link" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview']->value, ENT_QUOTES, 'UTF-8');?>
" /><?php }?>

	<div id="profile-jot-perms-end"></div>


	<div id="profile-jot-plugin-wrapper">
	<?php echo $_smarty_tpl->tpl_vars['jotplugins']->value;?>

	</div>

	<div id="profile-rotator-wrapper" style="display: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visitor']->value, ENT_QUOTES, 'UTF-8');?>
;" >
		<img id="profile-rotator" src="images/rotator.gif" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wait']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" />
	</div>

	<div id="jot-preview-content" style="display:none;"></div>

	<div style="display: none;">
		<div id="profile-jot-acl-wrapper" style="width:auto;height:auto;overflow:auto;">
			<?php echo $_smarty_tpl->tpl_vars['acl']->value;?>

		</div>
	</div>


</div>

<div id="profile-jot-end"></div>
</form>
</div>
		<?php if ($_smarty_tpl->tpl_vars['content']->value) {
echo '<script'; ?>
>initEditor();<?php echo '</script'; ?>
><?php }
}
}
