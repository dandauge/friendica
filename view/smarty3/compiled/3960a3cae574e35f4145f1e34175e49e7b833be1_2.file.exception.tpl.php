<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:42:19
  from '/var/www/friendica/view/templates/exception.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449ffbcfb6d3_47321169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3960a3cae574e35f4145f1e34175e49e7b833be1' => 
    array (
      0 => '/var/www/friendica/view/templates/exception.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449ffbcfb6d3_47321169 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="exception" class="generic-page-wrapper">
    <img class="hare" src="images/friendica-404_svg_flexy-o-hare.png"/>
    <h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
    <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');?>
</p>
<?php if ($_smarty_tpl->tpl_vars['thrown']->value) {?>
	<pre><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thrown']->value, ENT_QUOTES, 'UTF-8');?>

<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack_trace']->value, ENT_QUOTES, 'UTF-8');?>

<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['trace']->value, ENT_QUOTES, 'UTF-8');?>
</pre>
<?php }?>
	<p><button type="button" onclick="window.history.back()" class="btn btn-primary"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8');?>
</button></p>
</div>
<?php }
}
