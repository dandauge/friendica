<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:11:36
  from '/var/www/friendica/view/templates/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4e8eb8d68_64862051',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3d15915d924f603fc7298897cb75faebf7a5191' => 
    array (
      0 => '/var/www/friendica/view/templates/footer.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b4e8eb8d68_64862051 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['footerScripts']->value, 'scriptUrl');
$_smarty_tpl->tpl_vars['scriptUrl']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['scriptUrl']->value) {
$_smarty_tpl->tpl_vars['scriptUrl']->do_else = false;
echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scriptUrl']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
