<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:28
  from '/var/www/friendica/view/templates/settings/settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b19839cc28_21853514',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9b9186a0cf55f95309d5bf2e5958f5d0b6587fec' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_password.tpl' => 4,
    'file:field_input.tpl' => 7,
    'file:field_custom.tpl' => 1,
    'file:field_select.tpl' => 1,
    'file:field_checkbox.tpl' => 16,
    'file:field_intcheckbox.tpl' => 8,
  ),
),false)) {
function content_6044b19839cc28_21853514 (Smarty_Internal_Template $_smarty_tpl) {
?><h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ptitle']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

<?php echo $_smarty_tpl->tpl_vars['nickname_block']->value;?>


<form action="settings" id="settings-form" method="post" autocomplete="off" enctype="multipart/form-data">
	<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_pass']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password1']->value), 0, false);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password2']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password3']->value), 0, true);
?>

		<?php if ($_smarty_tpl->tpl_vars['oid_enable']->value) {?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['openid']->value), 0, false);
?>
		<?php }?>

		<div class="settings-submit-wrapper">
			<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_basic']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">

		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['username']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['email']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password4']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_custom.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['timezone']->value), 0, false);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['language']->value), 0, false);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['defloc']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['allowloc']->value), 0, false);
?>


		<div class="settings-submit-wrapper">
			<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_prv']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">

		<input type="hidden" name="visibility" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visibility']->value, ENT_QUOTES, 'UTF-8');?>
"/>

		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['maxreq']->value), 0, true);
?>

		<?php echo $_smarty_tpl->tpl_vars['profile_in_dir']->value;?>


		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['profile_in_net_dir']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_friends']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_wall']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['unlisted']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['accessiblephotos']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['blockwall']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['blocktags']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['unkmail']->value), 0, true);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['cntunkmail']->value), 0, true);
?>

		<?php echo $_smarty_tpl->tpl_vars['group_select']->value;?>


		<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['permissions']->value, ENT_QUOTES, 'UTF-8');?>
</h3>

		<?php echo $_smarty_tpl->tpl_vars['aclselect']->value;?>

		<div class="settings-submit-wrapper">
			<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['expire']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<div id="settings-expiry">
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['days']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['items']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['notes']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['starred']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['network_only']), 0, true);
?>

			<div class="settings-submit-wrapper">
				<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
			</div>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_not']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<div id="settings-notifications">

			<div id="settings-notification-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_not']->value, ENT_QUOTES, 'UTF-8');?>
</div>

			<div class="group">
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify1']->value), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify2']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify3']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify4']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify5']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify6']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify7']->value), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify8']->value), 0, true);
?>
			</div>

			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['email_textonly']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['detailed_notif']->value), 0, true);
?>

			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['desktop_notifications']->value), 0, true);
?>
			<?php echo '<script'; ?>
>
				(function () {
					let $notificationField = $("#div_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['desktop_notifications']->value[0], ENT_QUOTES, 'UTF-8');?>
");
					let $notificationCheckbox = $("#id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['desktop_notifications']->value[0], ENT_QUOTES, 'UTF-8');?>
");

					if (getNotificationPermission() === 'granted') {
						$notificationCheckbox.prop('checked', true);
					}
					if (getNotificationPermission() === null) {
						$notificationField.hide();
					}

					$notificationCheckbox.on('change', function (e) {
						if (Notification.permission === 'granted') {
							localStorage.setItem('notification-permissions', $notificationCheckbox.prop('checked') ? 'granted' : 'denied');
						} else if (Notification.permission === 'denied') {
							localStorage.setItem('notification-permissions', 'denied');

							$notificationCheckbox.prop('checked', false);
						} else if (Notification.permission === 'default') {
							Notification.requestPermission(function (choice) {
								if (choice === 'granted') {
									localStorage.setItem('notification-permissions', $notificationCheckbox.prop('checked') ? 'granted' : 'denied');
								} else {
									localStorage.setItem('notification-permissions', 'denied');
									$notificationCheckbox.prop('checked', false);
								}
							});
						}
					})
				})();
			<?php echo '</script'; ?>
>

		</div>

		<div class="settings-submit-wrapper">
			<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_advn']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<div id="settings-pagetype-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_descadvn']->value, ENT_QUOTES, 'UTF-8');?>
</div>

		<?php echo $_smarty_tpl->tpl_vars['pagetype']->value;?>


		<div class="settings-submit-wrapper">
			<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact_maxsize']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		<div id="settings-pagetype-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact_text']->value, ENT_QUOTES, 'UTF-8');?>
</div>
		<input type="file" name="importcontact-filename"/>

		<div class="settings-submit-wrapper">
			<input type="submit" name="importcontact-submit" class="importcontact-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact_button']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>

	<h2 class="settings-heading"><a href="javascript:;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relocate']->value, ENT_QUOTES, 'UTF-8');?>
</a></h2>
	<div class="settings-content-block">
		<div id="settings-pagetype-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relocate_text']->value, ENT_QUOTES, 'UTF-8');?>
</div>

		<div class="settings-submit-wrapper">
			<input type="submit" name="resend_relocate" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relocate_button']->value, ENT_QUOTES, 'UTF-8');?>
"/>
		</div>
	</div>
<?php }
}
