<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:28
  from '/var/www/friendica/view/templates/field_custom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1983a6450_17188975',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd9cd207b2c168d1e8011928eae2e0a5889dc2f7' => 
    array (
      0 => '/var/www/friendica/view/templates/field_custom.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1983a6450_17188975 (Smarty_Internal_Template $_smarty_tpl) {
?>
	
	<div class='field custom'>
		<label for='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<?php echo $_smarty_tpl->tpl_vars['field']->value[2];?>

		<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
		<?php }?>
	</div>
<?php }
}
