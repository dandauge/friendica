<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:32
  from '/var/www/friendica/view/theme/frio/templates/settings/connectors.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1d8747d00_16818454',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cea48d09d00a207a620a3e3fe4bb66d11618a5c2' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/connectors.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_checkbox.tpl' => 6,
    'file:field_input.tpl' => 6,
    'file:field_custom.tpl' => 1,
    'file:field_select.tpl' => 2,
    'file:field_password.tpl' => 1,
  ),
),false)) {
function content_6044b1d8747d00_16818454 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

	<p class="connector_statusmsg"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diasp_enabled']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<p class="connector_statusmsg"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ostat_enabled']->value, ENT_QUOTES, 'UTF-8');?>
</p>

	<form action="settings/connectors" method="post" autocomplete="off">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<div class="panel-group panel-group-settings" id="settings" role="tablist" aria-multiselectable="true">
			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="content-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#content-settings-content" aria-expanded="false" aria-controls="content-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['general_settings']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="content-settings-content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="content-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['accept_only_sharer']->value), 0, false);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['disable_cw']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['no_intelligent_shortening']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['attach_link_title']->value), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['ostatus_autofriend']->value), 0, true);
?>

						<?php echo $_smarty_tpl->tpl_vars['default_group']->value;?>


						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['legacy_contact']->value), 0, false);
?>

						<p><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['repair_ostatus_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['repair_ostatus_text']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>
					</div>
					<div class="panel-footer">
						<button type="submit" id="general-submit" name="general-submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>
		</div>

		<?php echo $_smarty_tpl->tpl_vars['settings_connectors']->value;?>


<?php if (!$_smarty_tpl->tpl_vars['mail_disabled']->value) {?>
		<span id="settings_mail_inflated" class="settings-block fakelink" style="display: block;" onclick="openClose('settings_mail_expanded'); openClose('settings_mail_inflated');">
			<img class="connector" src="images/mail.png" /><h3 class="settings-heading connector"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_imap']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
		</span>
		<div id="settings_mail_expanded" class="settings-block" style="display: none;">
			<span class="fakelink" onclick="openClose('settings_mail_expanded'); openClose('settings_mail_inflated');">
				<img class="connector" src="images/mail.png" /><h3 class="settings-heading connector"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_imap']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
			</span>
			<p><?php echo $_smarty_tpl->tpl_vars['imap_desc']->value;?>
</p>

			<?php $_smarty_tpl->_subTemplateRender("file:field_custom.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['imap_lastcheck']->value), 0, false);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_server']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_port']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_ssl']->value), 0, false);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_user']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_pass']->value), 0, false);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_replyto']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_pubmail']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_action']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mail_movetofolder']->value), 0, true);
?>

			<div class="settings-submit-wrapper" >
				<input type="submit" id="imap-submit" name="imap-submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" />
			</div>
		</div>
<?php }?>

	</form>
</div>
<?php }
}
