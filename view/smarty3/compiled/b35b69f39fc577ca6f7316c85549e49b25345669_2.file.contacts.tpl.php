<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/widget/contacts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f36d3091_42094142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b35b69f39fc577ca6f7316c85549e49b25345669' => 
    array (
      0 => '/var/www/friendica/view/templates/widget/contacts.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f36d3091_42094142 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="contact-block">
<h3 class="contact-block-h4"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contacts']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<?php if ($_smarty_tpl->tpl_vars['micropro']->value) {?>
		<a class="allcontact-link" href="profile/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nickname']->value, ENT_QUOTES, 'UTF-8');?>
/contacts"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['viewcontacts']->value, ENT_QUOTES, 'UTF-8');?>
</a>
		<div class='contact-block-content'>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['micropro']->value, 'm');
$_smarty_tpl->tpl_vars['m']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['m']->value) {
$_smarty_tpl->tpl_vars['m']->do_else = false;
?>
			<?php echo $_smarty_tpl->tpl_vars['m']->value;?>

		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>
<?php }?>
</div>
<div class="clear"></div>
<?php }
}
