<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/theme/vier/templates/profile/vcard.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f3706333_43423501',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98ae21652d733ddf02d963a56b58775fd0bb4019' => 
    array (
      0 => '/var/www/friendica/view/theme/vier/templates/profile/vcard.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:diaspora_vcard.tpl' => 1,
  ),
),false)) {
function content_6044b0f3706333_43423501 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="vcard h-card">

	<div class="tool">
		<div class="fn p-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['name'], ENT_QUOTES, 'UTF-8');?>
</div>
		<?php if ($_smarty_tpl->tpl_vars['profile']->value['edit']) {?>
			<div class="action">
				<a class="icon s16 edit ttright" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['edit'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['edit'][3], ENT_QUOTES, 'UTF-8');?>
"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['edit'][1], ENT_QUOTES, 'UTF-8');?>
</span></a>
			</div>
		<?php }?>
	</div>

	<?php if ($_smarty_tpl->tpl_vars['profile']->value['addr']) {?><div class="p-addr"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['addr'], ENT_QUOTES, 'UTF-8');?>
</div><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['profile']->value['picdate']) {?>
		<div id="profile-photo-wrapper"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><img class="photo u-photo" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['photo'], ENT_QUOTES, 'UTF-8');?>
?rev=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['picdate'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['name'], ENT_QUOTES, 'UTF-8');?>
"></a></div>
	<?php } else { ?>
		<div id="profile-photo-wrapper"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><img class="photo u-photo" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['photo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['name'], ENT_QUOTES, 'UTF-8');?>
"></a></div>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['account_type']->value) {?><div class="account-type"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['account_type']->value, ENT_QUOTES, 'UTF-8');?>
</div><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['profile']->value['network_link']) {?><dl class="network"><dt class="network-label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['network']->value, ENT_QUOTES, 'UTF-8');?>
</dt><dd class="x-network"><?php echo $_smarty_tpl->tpl_vars['profile']->value['network_link'];?>
</dd></dl><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['location']->value) {?>
		<dl class="location">
			<dt class="location-label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value, ENT_QUOTES, 'UTF-8');?>
</dt>
			<dd class="adr h-adr">
				<?php if ($_smarty_tpl->tpl_vars['profile']->value['address']) {?><p class="street-address p-street-address"><?php echo $_smarty_tpl->tpl_vars['profile']->value['address'];?>
</p><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value['location']) {?><p class="p-location"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['location'], ENT_QUOTES, 'UTF-8');?>
</p><?php }?>
			</dd>
		</dl>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['profile']->value['xmpp']) {?>
		<dl class="xmpp">
			<dt class="xmpp-label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['xmpp']->value, ENT_QUOTES, 'UTF-8');?>
</dt>
			<dd class="xmpp-data"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['xmpp'], ENT_QUOTES, 'UTF-8');?>
</dd>
		</dl>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['profile']->value['upubkey']) {?><div class="key u-key" style="display:none;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['upubkey'], ENT_QUOTES, 'UTF-8');?>
</div><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['contacts']->value) {?><div class="contacts" style="display:none;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contacts']->value, ENT_QUOTES, 'UTF-8');?>
</div><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['updated']->value) {?><div class="updated" style="display:none;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['updated']->value, ENT_QUOTES, 'UTF-8');?>
</div><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['homepage']->value) {?><dl class="homepage"><dt class="homepage-label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['homepage']->value, ENT_QUOTES, 'UTF-8');?>
</dt><dd class="homepage-url"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['homepage'], ENT_QUOTES, 'UTF-8');?>
" class="u-url" rel="me" target="_blank" rel="noopener noreferrer"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['homepage'], ENT_QUOTES, 'UTF-8');?>
</a></dd></dl><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['about']->value) {?><dl class="about"><dt class="about-label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['about']->value, ENT_QUOTES, 'UTF-8');?>
</dt><dd class="x-network"><?php echo $_smarty_tpl->tpl_vars['profile']->value['about'];?>
</dd></dl><?php }?>

	<?php $_smarty_tpl->_subTemplateRender("file:diaspora_vcard.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div id="profile-extra-links">
		<ul>
			<?php if ($_smarty_tpl->tpl_vars['unfollow_link']->value) {?>
				<li><a id="dfrn-request-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unfollow_link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unfollow']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['follow_link']->value) {?>
				<li><a id="dfrn-request-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['follow_link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['follow']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['wallmessage_link']->value) {?>
				<li><a id="wallmessage-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wallmessage_link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wallmessage']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['subscribe_feed_link']->value) {?>
				<li><a id="subscribe-feed-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subscribe_feed_link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subscribe_feed']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php }?>
		</ul>
	</div>
</div>

<?php echo $_smarty_tpl->tpl_vars['contact_block']->value;?>

<?php }
}
