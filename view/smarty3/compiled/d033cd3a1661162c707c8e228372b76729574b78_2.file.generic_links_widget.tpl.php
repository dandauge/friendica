<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:12:28
  from '/var/www/friendica/view/templates/generic_links_widget.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b51ca941a3_01483536',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd033cd3a1661162c707c8e228372b76729574b78' => 
    array (
      0 => '/var/www/friendica/view/templates/generic_links_widget.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b51ca941a3_01483536 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="widget<?php if ($_smarty_tpl->tpl_vars['class']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8');
}?>">
	<?php if ($_smarty_tpl->tpl_vars['title']->value) {?><h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['desc']->value) {?><div class="desc"><?php echo $_smarty_tpl->tpl_vars['desc']->value;?>
</div><?php }?>
	
	<ul role="menu">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
			<li role="menuitem" class="tool"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?> class="<?php if ($_smarty_tpl->tpl_vars['item']->value['selected']) {?>selected<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['label'], ENT_QUOTES, 'UTF-8');?>
</a></li>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>
	
</div>
<?php }
}
