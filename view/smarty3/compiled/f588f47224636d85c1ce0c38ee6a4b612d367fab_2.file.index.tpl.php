<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:16
  from '/var/www/friendica/view/templates/settings/profile/photo/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0d8d9f7f1_64703485',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f588f47224636d85c1ce0c38ee6a4b612d367fab' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/profile/photo/index.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0d8d9f7f1_64703485 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['current_picture']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<p><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['avatar']->value, ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['current_picture']->value, ENT_QUOTES, 'UTF-8');?>
"/></p>
	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['upload_picture']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<form enctype="multipart/form-data" action="settings/profile/photo" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<div id="profile-photo-upload-wrapper" class="form-group field input">
			<label id="profile-photo-upload-label" for="profile-photo-upload"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_upfile']->value, ENT_QUOTES, 'UTF-8');?>
 </label>
			<input class="form-control" name="userfile" type="file" id="profile-photo-upload" size="48">
			<div class="clear"></div>
		</div>

		<div id="profile-photo-submit-wrapper" class="pull-right settings-submit-wrapper">
			<button type="submit" name="submit" id="profile-photo-submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
		</div>
		<div class="clear"></div>
	</form>

	<p id="profile-photo-link-select-wrapper">
	<?php echo $_smarty_tpl->tpl_vars['select']->value;?>

	</p>
</div><?php }
}
