<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b8721e07_14874704',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bdcdfb5de10d28d7e3e842527dbfc82abcd43d55' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:js_strings.tpl' => 1,
  ),
),false)) {
function content_6044b1b8721e07_14874704 (Smarty_Internal_Template $_smarty_tpl) {
?>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<base href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/" />
<meta name="generator" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['generator']->value, ENT_QUOTES, 'UTF-8');?>
" />
<meta name="viewport" content="initial-scale=1.0">

<link rel="stylesheet" href="view/global.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="all" />
<link rel="stylesheet" href="view/asset/jquery-colorbox/example5/colorbox.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/jgrowl/jquery.jgrowl.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/jquery-datetimepicker/build/jquery.datetimepicker.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/perfect-scrollbar/dist/css/perfect-scrollbar.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />

<link rel="stylesheet" href="view/theme/frio/frameworks/bootstrap/css/bootstrap.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/bootstrap/css/bootstrap-theme.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/asset/fork-awesome/css/fork-awesome.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/jasny/css/jasny-bootstrap.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/bootstrap-select/css/bootstrap-select.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/ekko-lightbox/ekko-lightbox.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/justifiedGallery/justifiedGallery.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/frameworks/bootstrap-toggle/css/bootstrap-toggle.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/font/open_sans/open-sans.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>

<link rel="stylesheet" href="view/theme/frio/css/hovercard.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>
<link rel="stylesheet" href="view/theme/frio/css/font-awesome.custom.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen"/>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value, 'media', false, 'stylesheetUrl');
$_smarty_tpl->tpl_vars['media']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['stylesheetUrl']->value => $_smarty_tpl->tpl_vars['media']->value) {
$_smarty_tpl->tpl_vars['media']->do_else = false;
?>
<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheetUrl']->value, ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['media']->value, ENT_QUOTES, 'UTF-8');?>
" />
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<link rel="shortcut icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shortcut_icon']->value, ENT_QUOTES, 'UTF-8');?>
" />
<link rel="apple-touch-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['touch_icon']->value, ENT_QUOTES, 'UTF-8');?>
"/>

<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="manifest" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/manifest" />
<?php echo '<script'; ?>
 type="text/javascript">
// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
// Prevents links to switch to Safari in a home screen app - see https://gist.github.com/irae/1042167
(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(chref=d.href).replace("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/", "").replace(e.href,"").indexOf("#")&&(!/^[a-z\+\.\-]+:/i.test(chref)||chref.indexOf(e.protocol+"//"+e.host)===0)&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone");
// |license-end
<?php echo '</script'; ?>
>

<link rel="search"
         href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/opensearch"
         type="application/opensearchdescription+xml"
         title="Search in Friendica" />


<!--[if IE]>
<?php echo '<script'; ?>
 type="text/javascript" src="https://html5shiv.googlecode.com/svn/trunk/html5.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<![endif]-->
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/modernizr.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery/dist/jquery.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/jquery.textinputs.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/textcomplete/dist/textcomplete.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/autocomplete.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery-colorbox/jquery.colorbox-min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jgrowl/jquery.jgrowl.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/imagesloaded/imagesloaded.pkgd.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/base64/base64.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/dompurify/dist/purify.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/main.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/bootstrap/js/bootstrap.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/jasny/js/jasny-bootstrap.custom.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/bootstrap-select/js/bootstrap-select.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/ekko-lightbox/ekko-lightbox.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/justifiedGallery/jquery.justifiedGallery.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/flexMenu/flexmenu.custom.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/jquery-scrollspy/jquery-scrollspy.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/autosize/autosize.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/frameworks/sticky-kit/jquery.sticky-kit.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/js/theme.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/js/modal.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php if (!$_smarty_tpl->tpl_vars['block_public']->value) {
echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/js/hovercard.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
 type="text/javascript" src="view/theme/frio/js/textedit.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("file:js_strings.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
