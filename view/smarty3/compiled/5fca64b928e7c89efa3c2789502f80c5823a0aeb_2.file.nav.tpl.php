<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:12:28
  from '/var/www/friendica/view/theme/duepuntozero/templates/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b51cb4dd82_09161508',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5fca64b928e7c89efa3c2789502f80c5823a0aeb' => 
    array (
      0 => '/var/www/friendica/view/theme/duepuntozero/templates/nav.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b51cb4dd82_09161508 (Smarty_Internal_Template $_smarty_tpl) {
?>
<nav>
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['langselector']->value, ENT_QUOTES, 'UTF-8');?>


	<div id="site-location"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
</div>

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?><a id="nav-logout-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a> <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['login']) {?><a id="nav-login-link" class="nav-login-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a> <?php }?>

	<span id="nav-link-wrapper" >

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['register']) {?><a id="nav-register-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['register'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>
		
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['help']) {?> <a id="nav-help-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][2], ENT_QUOTES, 'UTF-8');?>
" target="friendica-help" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['tos']) {?> <a id="nav-tos-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>
		
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['apps']) {?><a id="nav-apps-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>

	<a accesskey="s" id="nav-search-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<a id="nav-directory-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][1], ENT_QUOTES, 'UTF-8');?>
</a>

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?><a accesskey="a" id="nav-admin-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?>
	<a accesskey="n" id="nav-network-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['network'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<span id="net-update" class="nav-ajax-left"></span>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?>
	<a accesskey="p" id="nav-home-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['home'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<span id="home-update" class="nav-ajax-left"></span>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['community']) {?>
	<a accesskey="c" id="nav-community-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['community'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['introductions']) {?>
	<a id="nav-notification-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['introductions'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<span id="intro-update" class="nav-ajax-left"></span>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?>
	<a id="nav-messages-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['messages'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</a>
	<span id="mail-update" class="nav-ajax-left"></span>
	<?php }?>





		<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?>
			<a accesskey="f" id="nav-notifications-linkmenu" class="nav-commlink" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
" rel="#nav-notifications-menu" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</a>
				<span id="notification-update" class="nav-ajax-left"></span>
				<ul id="nav-notifications-menu" class="menu-popup">
					<li id="nav-notifications-see-all"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<li id="nav-notifications-mark-all"><a href="#" onclick="notificationMarkAll(); return false;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][3], ENT_QUOTES, 'UTF-8');?>
</a></li>
					<li class="empty"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emptynotifications']->value, ENT_QUOTES, 'UTF-8');?>
</li>
				</ul>
		<?php }?>		

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?><a id="nav-settings-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['nav']->value['profiles']) {?><a id="nav-profiles-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?><a id="nav-contacts-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][3], ENT_QUOTES, 'UTF-8');?>
" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>


	<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?><a id="nav-delegation-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sel']->value['delegation'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][3], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a><?php }?>
	</span>
	<span id="nav-end"></span>
	<span id="banner"><?php echo $_smarty_tpl->tpl_vars['banner']->value;?>
</span>
</nav>

<ul id="nav-notifications-template" style="display:none;" rel="template">
	<li class="{4}"><a href="{0}" title="{5}"><img data-src="{1}" height="24" width="24" alt="" />{2} <span class="notif-when">{3}</span></a></li>
</ul>
<?php }
}
