<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:03:22
  from '/var/www/friendica/view/templates/settings/delegation.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2faa78351_80908759',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '55a6ea88b878cd302b7da1c915bfbe21023709a4' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/delegation.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_select.tpl' => 1,
    'file:field_password.tpl' => 1,
  ),
),false)) {
function content_6044b2faa78351_80908759 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="delegation" class="generic-page-wrapper">
<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['header']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

<?php if (!$_smarty_tpl->tpl_vars['is_child_user']->value) {?>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['account_header']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
<div id="add-account-desc" class="add-account-desc"><p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['account_desc']->value, ENT_QUOTES, 'UTF-8');?>
</p></div>
<p><a href='register'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['add_account']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['parent_user']->value) {?>
<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['parent_header']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
<div id="delegate-parent-desc" class="delegate-parent-desc"><p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['parent_desc']->value, ENT_QUOTES, 'UTF-8');?>
</p></div>
<div id="delegate-parent" class="delegate-parent">
	<form action="settings/delegation" method="post">
		<input type='hidden' name='form_security_token' value='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
'>
        <?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['parent_user']->value), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['parent_password']->value), 0, false);
?>
		<div class="submit"><input type="submit" name="delegate" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/></div>
	</form>
</div>
<?php }?>

<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delegates_header']->value, ENT_QUOTES, 'UTF-8');?>
</h2>

<div id="delegate-desc" class="delegate-desc"><p><?php echo $_smarty_tpl->tpl_vars['desc']->value;?>
</p></div>

<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['head_delegates']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<?php if ($_smarty_tpl->tpl_vars['delegates']->value) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['delegates']->value, 'x');
$_smarty_tpl->tpl_vars['x']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['x']->value) {
$_smarty_tpl->tpl_vars['x']->do_else = false;
?>
<div class="contact-block-div">
	<a class="contact-block-link" href="settings/delegation/remove/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['uid'], ENT_QUOTES, 'UTF-8');?>
">
		<img class="contact-block-img" src="photo/thumb/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['uid'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['username'], ENT_QUOTES, 'UTF-8');?>
 (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
)">
	</a>
</div>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
<div class="clear"></div>
<?php } else { ?>
    <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['none']->value, ENT_QUOTES, 'UTF-8');?>
</p>
<?php }?>

<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['head_potentials']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
<?php if ($_smarty_tpl->tpl_vars['potentials']->value) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['potentials']->value, 'x');
$_smarty_tpl->tpl_vars['x']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['x']->value) {
$_smarty_tpl->tpl_vars['x']->do_else = false;
?>
<div class="contact-block-div">
	<a class="contact-block-link" href="settings/delegation/add/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['uid'], ENT_QUOTES, 'UTF-8');?>
">
		<img class="contact-block-img" src="photo/thumb/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['uid'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['username'], ENT_QUOTES, 'UTF-8');?>
 (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
)">
	</a>
</div>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
<div class="clear"></div>
<?php } else { ?>
    <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['none']->value, ENT_QUOTES, 'UTF-8');?>
</p>
<?php }?>
</div>
<?php }
}
