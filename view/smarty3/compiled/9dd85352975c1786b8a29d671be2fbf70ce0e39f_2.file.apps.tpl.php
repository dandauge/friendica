<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:58
  from '/var/www/friendica/view/templates/apps.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2a6a4c4d7_17415676',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9dd85352975c1786b8a29d671be2fbf70ce0e39f' => 
    array (
      0 => '/var/www/friendica/view/templates/apps.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b2a6a4c4d7_17415676 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>

<ul>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['apps']->value, 'ap');
$_smarty_tpl->tpl_vars['ap']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ap']->value) {
$_smarty_tpl->tpl_vars['ap']->do_else = false;
?>
	<li><?php echo $_smarty_tpl->tpl_vars['ap']->value;?>
</li>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>
<?php }
}
