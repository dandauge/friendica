<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:55:14
  from '/var/www/friendica/view/templates/admin/aside.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1120ce426_41561490',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8f1d9087d6daebd1b8e390bca8e4c8e8ba7920f2' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/aside.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1120ce426_41561490 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	// update pending count //
	$(function(){

		$("nav").bind('nav-update',  function(e,data){
			var elm = $('#pending-update');
			var register = $(data).find('register').html();
			if (register=="0") { register=""; elm.hide();} else { elm.show(); }
			elm.html(register);
		});
	});
<?php echo '</script'; ?>
>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subpages']->value, 'page');
$_smarty_tpl->tpl_vars['page']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->do_else = false;
?>
<h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value[0], ENT_QUOTES, 'UTF-8');?>
</h4>
<ul class="admin linklist" role="menu">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['page']->value[1], 'item');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
	<li class='admin link button <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[2], ENT_QUOTES, 'UTF-8');?>
' role="menuitem"><a href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[0], ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[1], ENT_QUOTES, 'UTF-8');?>
</a></li>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>



<?php if ($_smarty_tpl->tpl_vars['admin']->value['update']) {?>
<ul class='admin linklist'>
	<li class='admin link button <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][2], ENT_QUOTES, 'UTF-8');?>
'><a href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][0], ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
</ul>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['admin']->value['addons_admin']) {?><h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugadmtxt']->value, ENT_QUOTES, 'UTF-8');?>
</h4>
<ul class='admin linklist'>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['admin']->value['addons_admin'], 'item', false, 'name');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['name']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
	<li role="menuitem" class="admin link button <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['class'], ENT_QUOTES, 'UTF-8');?>
">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
	</li>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>
<?php }?>
	
	
<?php }
}
