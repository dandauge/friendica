<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:45
  from '/var/www/friendica/view/theme/vier/templates/ch_connectors.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f5c34d56_24273877',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2780c765c31f13bfb4da6575f4c4f8d6ff82b390' => 
    array (
      0 => '/var/www/friendica/view/theme/vier/templates/ch_connectors.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f5c34d56_24273877 (Smarty_Internal_Template $_smarty_tpl) {
?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url']->value, ENT_QUOTES, 'UTF-8');?>
/settings/connectors"><img alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['alt_text']->value, ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['photo']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['alt_text']->value, ENT_QUOTES, 'UTF-8');?>
"></a>

<?php }
}
