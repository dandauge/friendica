<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:37:41
  from '/var/www/friendica/view/templates/login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449ee52ebc46_62905266',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2c1963281d7f6857f544c2cb0b55ea0c5ce62a8f' => 
    array (
      0 => '/var/www/friendica/view/templates/login.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
    'file:field_password.tpl' => 1,
    'file:field_openid.tpl' => 1,
    'file:field_checkbox.tpl' => 1,
  ),
),false)) {
function content_60449ee52ebc46_62905266 (Smarty_Internal_Template $_smarty_tpl) {
?>

<form id="login-form" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dest_url']->value, ENT_QUOTES, 'UTF-8');?>
" role="form" method="post" >
<div id="login-group" role="group" aria-labelledby="login-head">
	<input type="hidden" name="auth-params" value="login" />

	<h3 id="login-head" class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['login']->value, ENT_QUOTES, 'UTF-8');?>
</h3>

	<div id="login_standard">
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['lname']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['lpassword']->value), 0, false);
?>
	<div id="login-lost-password-link">
		<a href="lostpass" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lostpass']->value, ENT_QUOTES, 'UTF-8');?>
" id="lost-password-link" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lostlink']->value, ENT_QUOTES, 'UTF-8');?>
</a>
	</div>
	</div>
	
	<?php if ($_smarty_tpl->tpl_vars['openid']->value) {?>
		<div id="login_openid">
		<?php $_smarty_tpl->_subTemplateRender("file:field_openid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['lopenid']->value), 0, false);
?>
		</div>
	<?php }?>

	<div id="login-submit-wrapper" >
		<input type="submit" name="submit" id="login-submit-button" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['login']->value, ENT_QUOTES, 'UTF-8');?>
" />
	</div>

	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['lremember']->value), 0, false);
?>
	
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['hiddens']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	
</div>
</form>

<?php if ($_smarty_tpl->tpl_vars['register']->value) {?>
<div id="login-extra-links">
	<h3 id="login-head" class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['register']->value['title'], ENT_QUOTES, 'UTF-8');?>
</h3>
	<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['register']->value['url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['register']->value['title'], ENT_QUOTES, 'UTF-8');?>
" id="register-link"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['register']->value['desc'], ENT_QUOTES, 'UTF-8');?>
</a>
</div>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript"> $(document).ready(function() { $("#id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lname']->value[0], ENT_QUOTES, 'UTF-8');?>
").focus();} );<?php echo '</script'; ?>
>
<?php }
}
