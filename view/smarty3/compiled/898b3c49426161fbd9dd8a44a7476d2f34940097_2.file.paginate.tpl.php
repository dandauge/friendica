<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/paginate.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f37e85e9_06050884',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '898b3c49426161fbd9dd8a44a7476d2f34940097' => 
    array (
      0 => '/var/www/friendica/view/templates/paginate.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f37e85e9_06050884 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['pager']->value && ($_smarty_tpl->tpl_vars['pager']->value['prev'] || $_smarty_tpl->tpl_vars['pager']->value['next'])) {?>
<div class="pager">
	<?php if ($_smarty_tpl->tpl_vars['pager']->value['prev']) {?><span class="pager_prev <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['prev']['text'], ENT_QUOTES, 'UTF-8');?>
</a></span><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['first']) {?><span class="pager_first <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['first']['text'], ENT_QUOTES, 'UTF-8');?>
</a></span><?php }?>

	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pager']->value['pages'], 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?><span class="pager_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['text'], ENT_QUOTES, 'UTF-8');?>
</a></span><?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['last']) {?>&nbsp;<span class="pager_last <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['last']['text'], ENT_QUOTES, 'UTF-8');?>
</a></span><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['pager']->value['next']) {?><span class="pager_next <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pager']->value['next']['text'], ENT_QUOTES, 'UTF-8');?>
</a></span><?php }?>
</div>
<?php }
}
}
