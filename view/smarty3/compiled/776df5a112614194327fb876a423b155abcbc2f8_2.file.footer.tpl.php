<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b872aed8_02629330',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '776df5a112614194327fb876a423b155abcbc2f8' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/footer.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b872aed8_02629330 (Smarty_Internal_Template $_smarty_tpl) {
?>			<!-- Modal  -->
			<div id="modal" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-full-screen">
					<div class="modal-content">
						<div id="modal-header" class="modal-header">
							<button id="modal-cloase" type="button" class="close" data-dismiss="modal">
								&times;
							</button>
							<h4 id="modal-title" class="modal-title"></h4>
						</div>
						<div id="modal-body" class="modal-body">
							<!-- /# content goes here -->
						</div>
					</div>
				</div>
			</div>

			<!-- Dummy div to append other div's when needed (e.g. used for js function editpost() -->
			<div id="cache-container"></div>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['footerScripts']->value, 'scriptUrl');
$_smarty_tpl->tpl_vars['scriptUrl']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['scriptUrl']->value) {
$_smarty_tpl->tpl_vars['scriptUrl']->do_else = false;
?>
			<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scriptUrl']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
