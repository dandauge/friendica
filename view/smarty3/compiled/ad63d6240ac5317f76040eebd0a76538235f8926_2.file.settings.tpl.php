<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:40
  from '/var/www/friendica/view/theme/frio/templates/settings/settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2d036a439_44688036',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad63d6240ac5317f76040eebd0a76538235f8926' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_password.tpl' => 4,
    'file:field_input.tpl' => 7,
    'file:field_checkbox.tpl' => 17,
    'file:field_custom.tpl' => 1,
    'file:field_select.tpl' => 1,
    'file:field_intcheckbox.tpl' => 8,
  ),
),false)) {
function content_6044b2d036a439_44688036 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ptitle']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

	<?php echo $_smarty_tpl->tpl_vars['nickname_block']->value;?>


	<form action="settings" id="settings-form" method="post" autocomplete="off" enctype="multipart/form-data">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

				<div class="panel-group panel-group-settings" id="settings" role="tablist" aria-multiselectable="true">
						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="password-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#password-settings-collapse" aria-expanded="false" aria-controls="password-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_pass']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="password-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="password-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password1']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password2']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password3']->value), 0, true);
?>

					<?php if ($_smarty_tpl->tpl_vars['oid_enable']->value) {?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['openid']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['delete_openid']->value), 0, false);
?>
					<?php }?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="basic-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#basic-settings-collapse" aria-expanded="false" aria-controls="basic-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_basic']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="basic-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="basic-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['username']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['email']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password4']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_custom.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['timezone']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['language']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['defloc']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['allowloc']->value), 0, true);
?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="privacy-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#privacy-settings-collapse" aria-expanded="false" aria-controls="privacy-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_prv']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="privacy-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="privacy-settings">
					<div class="panel-body">

						<input type="hidden" name="visibility" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['visibility']->value, ENT_QUOTES, 'UTF-8');?>
" />

						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['maxreq']->value), 0, true);
?>

						<?php echo $_smarty_tpl->tpl_vars['profile_in_dir']->value;?>


						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['profile_in_net_dir']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_friends']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_wall']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['unlisted']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['accessiblephotos']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['blockwall']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['blocktags']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['unkmail']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['cntunkmail']->value), 0, true);
?>

						<?php echo $_smarty_tpl->tpl_vars['group_select']->value;?>


						<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['permissions']->value, ENT_QUOTES, 'UTF-8');?>
</h3>

						<?php echo $_smarty_tpl->tpl_vars['aclselect']->value;?>

					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="expire-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#expire-settings-collapse" aria-expanded="false" aria-controls="expire-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['expire']->value['label'], ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="expire-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="expire-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['days']), 0, true);
?>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['items']), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['notes']), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['starred']), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['expire']->value['network_only']), 0, true);
?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="notification-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#notification-settings-collapse" aria-expanded="false" aria-controls="notification-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_not']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="notification-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="notification-settings">
					<div id="settings-notifications" class="panel-body">

						<div id="settings-notification-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lbl_not']->value, ENT_QUOTES, 'UTF-8');?>
</div>

						<div class="group">
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify1']->value), 0, false);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify2']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify3']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify4']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify5']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify6']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify7']->value), 0, true);
?>
							<?php $_smarty_tpl->_subTemplateRender("file:field_intcheckbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['notify8']->value), 0, true);
?>
						</div>

						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['email_textonly']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['detailed_notif']->value), 0, true);
?>

						
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['desktop_notifications']->value), 0, true);
?>
						<?php echo '<script'; ?>
 type="text/javascript">
							(function(){
								let $notificationField = $("#div_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['desktop_notifications']->value[0], ENT_QUOTES, 'UTF-8');?>
");
								let $notificationCheckbox = $("#id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['desktop_notifications']->value[0], ENT_QUOTES, 'UTF-8');?>
");

								if (getNotificationPermission() === 'granted') {
									$notificationCheckbox.prop('checked', true);
								}
								if (getNotificationPermission() === null) {
									$notificationField.hide();
								}

								$notificationCheckbox.on('change', function(e){
									if (Notification.permission === 'granted') {
										localStorage.setItem('notification-permissions', $notificationCheckbox.prop('checked') ? 'granted' : 'denied');
									} else if (Notification.permission === 'denied') {
										localStorage.setItem('notification-permissions', 'denied');

										$notificationCheckbox.prop('checked', false);
									} else if (Notification.permission === 'default') {
										Notification.requestPermission(function(choice) {
											if (choice === 'granted') {
												localStorage.setItem('notification-permissions', $notificationCheckbox.prop('checked') ? 'granted' : 'denied');
											} else {
												localStorage.setItem('notification-permissions', 'denied');
												$notificationCheckbox.prop('checked', false);
											}
										});
									}
								})
							})();
						<?php echo '</script'; ?>
>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="additional-account-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#additional-account-settings-collapse" aria-expanded="false" aria-controls="additional-account-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_advn']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="additional-account-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="additional-account-settings">
					<div class="panel-body">
						<div id="settings-pagetype-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['h_descadvn']->value, ENT_QUOTES, 'UTF-8');?>
</div>

						<?php echo $_smarty_tpl->tpl_vars['pagetype']->value;?>

					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="importcontact-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#importcontact-settings-collapse" aria-expanded="false" aria-controls="importcontact-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="importcontact-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="importcontact-settings">
					<div class="panel-body">
						<div id="importcontact-relocate-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact_text']->value, ENT_QUOTES, 'UTF-8');?>
</div>
						<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importcontact_maxsize']->value, ENT_QUOTES, 'UTF-8');?>
" />
						<input type="file" name="importcontact-filename" />
					</div>
					<div class="panel-footer">
						<button type="submit" name="importcontact-submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

						<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="relocate-settings">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#relocate-settings-collapse" aria-expanded="false" aria-controls="relocate-settings-collapse">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relocate']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="relocate-settings-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="relocate-settings">
					<div class="panel-body">
						<div id="settings-relocate-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relocate_text']->value, ENT_QUOTES, 'UTF-8');?>
</div>
					</div>
					<div class="panel-footer">
						<button type="submit" name="resend_relocate" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<?php }
}
