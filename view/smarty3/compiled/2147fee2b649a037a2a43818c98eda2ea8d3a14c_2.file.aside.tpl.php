<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:42
  from '/var/www/friendica/view/theme/frio/templates/admin/aside.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1e2af7084_87860650',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2147fee2b649a037a2a43818c98eda2ea8d3a14c' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/admin/aside.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1e2af7084_87860650 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
	// update pending count //
	$(function(){
		$("nav").bind('nav-update', function(e,data){
			var elm = $('#pending-update');
			var register = parseInt($(data).find('register').text());
			if (register > 0) {
				elm.html(register);
			}
		});
	});
<?php echo '</script'; ?>
>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subpages']->value, 'page');
$_smarty_tpl->tpl_vars['page']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->do_else = false;
?>
<div class="widget">
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value[0], ENT_QUOTES, 'UTF-8');?>
</h3>
	<ul role="menu">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['page']->value[1], 'item');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
		<li role="menuitem" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[2], ENT_QUOTES, 'UTF-8');?>
">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[0], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value[1], ENT_QUOTES, 'UTF-8');?>

				<?php if ($_smarty_tpl->tpl_vars['name']->value == "users") {?>
				 <span id="pending-update" class="badge pull-right"></span>
				<?php }?>
			</a>
		</li>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>

	<?php if ($_smarty_tpl->tpl_vars['admin']->value['update']) {?>
	<ul role="menu">
		<li role="menuitem" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][2], ENT_QUOTES, 'UTF-8');?>
">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][0], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['admin']->value['update']['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update']['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['admin']->value['update'][1], ENT_QUOTES, 'UTF-8');?>

			</a>
		</li>
	</ul>
	<?php }?>
</div>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php if ($_smarty_tpl->tpl_vars['admin']->value['addons_admin']) {?>
<div class="widget">
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugadmtxt']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
	<ul role="menu">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['admin']->value['addons_admin'], 'item', false, 'name');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['name']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
		<li role="menuitem" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['class'], ENT_QUOTES, 'UTF-8');?>
">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['accesskey']) {?>accesskey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['accesskey'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8');?>

			</a>
		</li>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>
</div>
<?php }?>

<?php }
}
