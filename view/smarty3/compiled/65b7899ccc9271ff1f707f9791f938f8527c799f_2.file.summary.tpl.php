<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:42
  from '/var/www/friendica/view/theme/frio/templates/admin/summary.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1e2bc6e92_99843719',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65b7899ccc9271ff1f707f9791f938f8527c799f' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/admin/summary.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1e2bc6e92_99843719 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id='adminpage-summery' class="adminpage generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

	<?php if (count($_smarty_tpl->tpl_vars['warningtext']->value)) {?>
	<div id="admin-warning-message-wrapper" class="alert alert-warning">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['warningtext']->value, 'wt');
$_smarty_tpl->tpl_vars['wt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wt']->value) {
$_smarty_tpl->tpl_vars['wt']->do_else = false;
?>
		<p><?php echo $_smarty_tpl->tpl_vars['wt']->value;?>
</p>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
	<?php }?>

	<div id="admin-summary-wrapper">
				<div id="admin-summary-queues" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['label'], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/queue/deferred"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['deferred'], ENT_QUOTES, 'UTF-8');?>
</a> - <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/queue"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['workerq'], ENT_QUOTES, 'UTF-8');?>
</a></div>
		</div>

				<div id="admin-summary-pending" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pending']->value[0], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pending']->value[1], ENT_QUOTES, 'UTF-8');?>
</div>
		</div>

				<div id="admin-summary-users" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['users']->value[0], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['users']->value[1], ENT_QUOTES, 'UTF-8');?>
</div>
		</div>

				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accounts']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry"><?php if ($_smarty_tpl->tpl_vars['p']->value[1]) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[1], ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?></div>
		</div>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

				<div id="admin-summary-addons" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value[0], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addons']->value[1], 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
				<a href="/admin/addons/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a><br>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
		</div>

				<div id="admin-summary-version" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['version']->value[0], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['platform']->value, ENT_QUOTES, 'UTF-8');?>
 '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['codename']->value, ENT_QUOTES, 'UTF-8');?>
' <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['version']->value[1], ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['build']->value, ENT_QUOTES, 'UTF-8');?>
</div>
		</div>

				<div id="admin-summary-php" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin-summary">
			<hr class="admin-summary-separator">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 admin-summary-label-name text-muted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['serversettings']->value['label'], ENT_QUOTES, 'UTF-8');?>
</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 admin-summary-entry">
				<table class="table">
				<tbody>
					<tr class="info"><td colspan="2">PHP</td></tr>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['serversettings']->value['php'], 'p', false, 'k');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
						<tr><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
</td><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</td></tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<tr class="info"><td colspan="2">MySQL / MariaDB</td></tr>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['serversettings']->value['mysql'], 'p', false, 'k');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
						<tr><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
</td><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</td></tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="clear"></div>

</div>
<?php }
}
