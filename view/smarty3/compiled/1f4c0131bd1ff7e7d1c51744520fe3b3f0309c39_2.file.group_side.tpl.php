<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/theme/frio/templates/group_side.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291aebfb7_63335788',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1f4c0131bd1ff7e7d1c51744520fe3b3f0309c39' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/group_side.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291aebfb7_63335788 (Smarty_Internal_Template $_smarty_tpl) {
?><span id="group-sidebar-inflated" class="widget fakelink" onclick="openCloseWidget('group-sidebar', 'group-sidebar-inflated');">
        <h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
</span>
<div class="widget" id="group-sidebar">
	<div id="sidebar-group-header">
		<span class="fakelink" onclick="openCloseWidget('group-sidebar', 'group-sidebar-inflated');">
			<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
		</span>
		<?php if (!$_smarty_tpl->tpl_vars['newgroup']->value) {?>
		<a class="group-edit-tool pull-right widget-action faded-icon" id="sidebar-edit-group" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['grouppage']->value, ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editgroupstext']->value, ENT_QUOTES, 'UTF-8');?>
">
			<i class="fa fa-pencil" aria-hidden="true"></i>
		</a>
		<?php } else { ?>
		<a class="group-edit-tool pull-right widget-action faded-icon" id="sidebar-new-group" onclick="javascript:$('#group-new-form').fadeIn('fast');" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['createtext']->value, ENT_QUOTES, 'UTF-8');?>
">
			<i class="fa fa-plus" aria-hidden="true"></i>
		</a>
		<form id="group-new-form" action="group/new" method="post" style="display:none;">
			<div class="form-group">
				<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
				<input name="groupname" id="id_groupname" class="form-control input-sm" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['creategroup']->value, ENT_QUOTES, 'UTF-8');?>
">
			</div>
		</form>
		<?php }?>
	</div>
	<div id="sidebar-group-list">
				<ul role="menu" id="sidebar-group-ul">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'group');
$_smarty_tpl->tpl_vars['group']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->do_else = false;
?>
				<li role="menuitem" class="sidebar-group-li group-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['group']->value['selected']) {?>selected<?php }?>">
					<?php if (!$_smarty_tpl->tpl_vars['newgroup']->value) {?><span class="notify badge pull-right"></span><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['group']->value['cid']) {?>
						<div class="checkbox pull-right group-checkbox ">
							<input type="checkbox"
								id="sidebar-group-checkbox-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
"
								class="<?php if ($_smarty_tpl->tpl_vars['group']->value['selected']) {?>ticked<?php } else { ?>unticked <?php }?> action"
								onclick="return contactgroupChangeMember(this, '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
','<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['cid'], ENT_QUOTES, 'UTF-8');?>
');"
								<?php if ($_smarty_tpl->tpl_vars['group']->value['ismember']) {?>checked="checked"<?php }?>
								aria-checked="<?php if ($_smarty_tpl->tpl_vars['group']->value['ismember']) {?>true<?php } else { ?>false<?php }?>"
							/>
							<label for="sidebar-group-checkbox-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
"></label>
							<div class="clearfix"></div>
						</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['group']->value['edit']) {?>
												<a id="edit-sidebar-group-element-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="group-edit-tool pull-right faded-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['edit']['href'], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edittext']->value, ENT_QUOTES, 'UTF-8');?>
">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</a>
					<?php }?>
					<a id="sidebar-group-element-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="sidebar-group-element" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['href'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['text'], ENT_QUOTES, 'UTF-8');?>
</a>
				</li>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

			<?php if ($_smarty_tpl->tpl_vars['ungrouped']->value) {?><li class="<?php if ($_smarty_tpl->tpl_vars['ungrouped_selected']->value) {?>selected<?php }?> sidebar-group-li" id="sidebar-ungrouped"><a href="nogroup"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ungrouped']->value, ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
		</ul>
	</div>
</div>
<?php echo '<script'; ?>
>
initWidget('group-sidebar', 'group-sidebar-inflated');
<?php echo '</script'; ?>
>
<?php }
}
