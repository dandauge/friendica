<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/templates/events_reminder.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291b92756_08311056',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '981663c6bc303ea066a45f0f114b7cf12d447966' => 
    array (
      0 => '/var/www/friendica/view/templates/events_reminder.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291b92756_08311056 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['count']->value) {?>
<div id="event-notice" class="birthday-notice fakelink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['classtoday']->value, ENT_QUOTES, 'UTF-8');?>
" onclick="openClose('event-wrapper');"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event_reminders']->value, ENT_QUOTES, 'UTF-8');?>
 (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['count']->value, ENT_QUOTES, 'UTF-8');?>
)</div>
<div id="event-wrapper" style="display: none;" ><div id="event-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event_title']->value, ENT_QUOTES, 'UTF-8');?>
</div>
<div id="event-title-end"></div>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['events']->value, 'event');
$_smarty_tpl->tpl_vars['event']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['event']->value) {
$_smarty_tpl->tpl_vars['event']->do_else = false;
?>
<div class="event-list" id="event-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event']->value['id'], ENT_QUOTES, 'UTF-8');?>
"> <a class="ajax-popupbox" href="events/?id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event']->value['title'], ENT_QUOTES, 'UTF-8');?>
</a> - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['event']->value['date'], ENT_QUOTES, 'UTF-8');?>
 </div>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php }?>

<?php }
}
