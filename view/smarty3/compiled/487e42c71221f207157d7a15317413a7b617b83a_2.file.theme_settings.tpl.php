<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/theme_settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b8691dd2_02536712',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '487e42c71221f207157d7a15317413a7b617b83a' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/theme_settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
    'file:field_colorinput.tpl' => 5,
    'file:field/range_percent.tpl' => 1,
    'file:field_fileinput.tpl' => 2,
    'file:field_radio.tpl' => 1,
  ),
),false)) {
function content_6044b1b8691dd2_02536712 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/theme/quattro/jquery.tools.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/js/ajaxupload.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>

<div class="form-group field select">
	<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
	<select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme']->value[0], ENT_QUOTES, 'UTF-8');?>
" class="form-control">
		<option value="---" <?php if ('---' == $_smarty_tpl->tpl_vars['scheme']->value[2]) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['custom']->value, ENT_QUOTES, 'UTF-8');?>
</option>
		<optgroup label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['accented']->value, ENT_QUOTES, 'UTF-8');?>
">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['scheme']->value[3], 'label', false, 'value');
$_smarty_tpl->tpl_vars['label']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['value']->value => $_smarty_tpl->tpl_vars['label']->value) {
$_smarty_tpl->tpl_vars['label']->do_else = false;
?>
			<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['scheme']->value[2]) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['label']->value, ENT_QUOTES, 'UTF-8');?>
</option>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</optgroup>
		<optgroup label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['legacy']->value, ENT_QUOTES, 'UTF-8');?>
">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['scheme']->value[4], 'label', false, 'value');
$_smarty_tpl->tpl_vars['label']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['value']->value => $_smarty_tpl->tpl_vars['label']->value) {
$_smarty_tpl->tpl_vars['label']->do_else = false;
?>
			<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['scheme']->value[2]) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['label']->value, ENT_QUOTES, 'UTF-8');?>
</option>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</optgroup>
	</select>
</div>

<?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value) {?>
<div class="form-group">
	<p><label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[1], ENT_QUOTES, 'UTF-8');?>
</label></p>
	<label class="radio-inline">
		<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_BLUE'), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value[2] == @constant('FRIO_SCHEME_ACCENT_BLUE')) {?> checked<?php }?>>
		<span style="border-radius: 10px; background-color: <?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_BLUE'), ENT_QUOTES, 'UTF-8');?>
; width: 20px; display: inline-block">&nbsp;</span>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[3]['blue'], ENT_QUOTES, 'UTF-8');?>

	</label>
	<label class="radio-inline">
		<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_RED'), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value[2] == @constant('FRIO_SCHEME_ACCENT_RED')) {?> checked<?php }?>>
		<span style="border-radius: 10px; background-color: <?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_RED'), ENT_QUOTES, 'UTF-8');?>
; width: 20px; display: inline-block">&nbsp;</span>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[3]['red'], ENT_QUOTES, 'UTF-8');?>

	</label>
	<label class="radio-inline">
		<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_PURPLE'), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value[2] == @constant('FRIO_SCHEME_ACCENT_PURPLE')) {?> checked<?php }?>>
		<span style="border-radius: 10px; background-color: <?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_PURPLE'), ENT_QUOTES, 'UTF-8');?>
; width: 20px; display: inline-block">&nbsp;</span>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[3]['purple'], ENT_QUOTES, 'UTF-8');?>

	</label>
	<label class="radio-inline">
		<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_GREEN'), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value[2] == @constant('FRIO_SCHEME_ACCENT_GREEN')) {?> checked<?php }?>>
		<span style="border-radius: 10px; background-color: <?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_GREEN'), ENT_QUOTES, 'UTF-8');?>
; width: 20px; display: inline-block">&nbsp;</span>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[3]['green'], ENT_QUOTES, 'UTF-8');?>

	</label>
	<label class="radio-inline">
		<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_PINK'), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['scheme_accent']->value[2] == @constant('FRIO_SCHEME_ACCENT_PINK')) {?> checked<?php }?>>
		<span style="border-radius: 10px; background-color: <?php echo htmlspecialchars(@constant('FRIO_SCHEME_ACCENT_PINK'), ENT_QUOTES, 'UTF-8');?>
; width: 20px; display: inline-block">&nbsp;</span>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['scheme_accent']->value[3]['pink'], ENT_QUOTES, 'UTF-8');?>

	</label>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['share_string']->value) {
$_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['share_string']->value), 0, false);
}
if ($_smarty_tpl->tpl_vars['nav_bg']->value) {
$_smarty_tpl->_subTemplateRender("file:field_colorinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['nav_bg']->value), 0, false);
}
if ($_smarty_tpl->tpl_vars['nav_icon_color']->value) {
$_smarty_tpl->_subTemplateRender("file:field_colorinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['nav_icon_color']->value), 0, true);
}
if ($_smarty_tpl->tpl_vars['link_color']->value) {
$_smarty_tpl->_subTemplateRender("file:field_colorinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['link_color']->value), 0, true);
}?>

<?php if ($_smarty_tpl->tpl_vars['background_color']->value) {
$_smarty_tpl->_subTemplateRender("file:field_colorinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['background_color']->value), 0, true);
}?>

<?php if ($_smarty_tpl->tpl_vars['contentbg_transp']->value) {
$_smarty_tpl->_subTemplateRender("file:field/range_percent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['contentbg_transp']->value), 0, false);
}?>

<?php if ($_smarty_tpl->tpl_vars['background_image']->value) {
$_smarty_tpl->_subTemplateRender("file:field_fileinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['background_image']->value), 0, false);
}?>

<div id="frio_bg_image_options" style="display: none;">
	<label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bg_image_options_title']->value, ENT_QUOTES, 'UTF-8');?>
:</label>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['bg_image_options']->value, 'options');
$_smarty_tpl->tpl_vars['options']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['options']->value) {
$_smarty_tpl->tpl_vars['options']->do_else = false;
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['options']->value), 0, true);
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>

<?php if ($_smarty_tpl->tpl_vars['login_bg_image']->value) {
$_smarty_tpl->_subTemplateRender("file:field_fileinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['login_bg_image']->value), 0, true);
}
if ($_smarty_tpl->tpl_vars['login_bg_color']->value) {
$_smarty_tpl->_subTemplateRender("file:field_colorinput.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['login_bg_color']->value), 0, true);
}?>

<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function() {

		function GenerateShareString() {
			var theme = {};
			// Parse initial share_string
			if ($("#id_frio_nav_bg").length) {
				theme.nav_bg = $("#id_frio_nav_bg").val();
			}

			if ($("#id_frio_nav_icon_color").length) {
				theme.nav_icon_color = $("#id_frio_nav_icon_color").val();
			}

			if ($("#id_frio_link_color").length) {
				theme.link_color = $("#id_frio_link_color").val();
			}

			if ($("#id_frio_background_color").length) {
				theme.background_color = $("#id_frio_background_color").val();
			}

			if ($("#id_frio_background_image").length) {
				theme.background_image = $("#id_frio_background_image").val();

				if (theme.background_image.length > 0) {
					if ($("#id_frio_bg_image_option_stretch").is(":checked") == true) {
						theme.background_image_option = "stretch";
					}
					if ($("#id_frio_bg_image_option_cover").is(":checked") == true) {
						theme.background_image_option = "cover";
					}
					if ($("#id_frio_bg_image_option_contain").is(":checked") == true) {
						theme.background_image_option = "contain";
					}
					if ($("#id_frio_bg_image_option_repeat").is(":checked") == true) {
						theme.background_image_option = "repeat";
					}
				 }
			}

			if ($("#frio_contentbg_transp").length) {
				theme.contentbg_transp = $("#frio_contentbg_transp").val();
			}

			if ($("#id_frio_login_bg_image").length) {
				theme.login_bg_image = $("#id_frio_login_bg_image").val();
			}

			if ($("#id_frio_login_bg_color").length) {
				theme.login_bg_color = $("#id_frio_login_bg_color").val();
			}
			if (!($("#id_frio_share_string").is(":focus"))){
				var share_string = JSON.stringify(theme);
				$("#id_frio_share_string").val(share_string);
			}
		}

		// interval because jquery.val does not trigger events
		window.setInterval(GenerateShareString, 500);
		GenerateShareString();

		// Take advantage of the effects of previous comment
		$(document).on("input", "#id_frio_share_string", function() {
			theme = JSON.parse($("#id_frio_share_string").val());

			if ($("#id_frio_nav_bg").length) {
				$("#id_frio_nav_bg").val(theme.nav_bg);
			}

			if ($("#id_frio_nav_icon_color").length) {
				$("#id_frio_nav_icon_color").val(theme.nav_icon_color);
			}

			if ($("#id_frio_link_color").length) {
				 $("#id_frio_link_color").val(theme.link_color);
			}

			if ($("#id_frio_background_color").length) {
				$("#id_frio_background_color").val(theme.background_color);
			}

			if ($("#id_frio_background_image").length) {
				$("#id_frio_background_image").val(theme.background_image);
				var elText = theme.background_image;
				if(elText.length !== 0) {
					$("#frio_bg_image_options").show();
				} else {
					$("#frio_bg_image_options").hide();
				}

				switch (theme.background_image_option) {
					case 'stretch':
						$("#id_frio_bg_image_option_stretch").prop("checked", true);
						break;
					case 'cover':
						$("#id_frio_bg_image_option_cover").prop("checked", true);
						break;
					case 'contain':
						$("#id_frio_bg_image_option_contain").prop("checked", true);
						break;
					case 'repeat':
						$("#id_frio_bg_image_option_repeat").prop("checked", true);
						break;
				}
			}

			if ($("#frio_contentbg_transp").length) {
				$("#frio_contentbg_transp").val(theme.contentbg_transp);
			}

			if ($("#id_frio_login_bg_image").length) {
				$("#id_frio_login_bg_image").val(theme.login_bg_image);
			}

			if ($("#id_frio_login_bg_color").length) {
				$("#id_frio_login_bg_color").val(theme.login_bg_color);
			}
		});
		// Create colorpickers
		$("#frio_nav_bg, #frio_nav_icon_color, #frio_background_color, #frio_link_color, #frio_login_bg_color").colorpicker({format: 'hex', align: 'left'});

		// show image options when user user starts to type the address of the image
		$("#id_frio_background_image").keyup(function(){
			var elText = $(this).val();
			if(elText.length !== 0) {
				$("#frio_bg_image_options").show();
			} else {
				$("#frio_bg_image_options").hide();
			}
		});

		// show the image options is there is allready an image
		if($("#id_frio_background_image").val().length != 0) {
				$("#frio_bg_image_options").show();
		}
	});
<?php echo '</script'; ?>
>

<div class="settings-submit-wrapper pull-right">
	<button type="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" class="settings-submit btn btn-primary" name="frio-settings-submit"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
</div>
<div class="clearfix"></div>
<?php }
}
