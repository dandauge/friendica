<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:13:06
  from '/var/www/friendica/view/theme/smoothly/templates/bottom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b542c5b1d7_42108798',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3699eb289f5198ecc316b19098adc8097267fd6b' => 
    array (
      0 => '/var/www/friendica/view/theme/smoothly/templates/bottom.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b542c5b1d7_42108798 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/theme/smoothly/js/jquery.autogrow.textarea.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
function tautogrow(id) {
	$("textarea#comment-edit-text-" + id).autogrow();
}
<?php echo '</script'; ?>
>
<?php }
}
