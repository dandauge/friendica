<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:56:49
  from '/var/www/friendica/view/templates/field_textarea.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1714c7948_39884859',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8401394c29381ae670a0476357d3b0a400ff1b69' => 
    array (
      0 => '/var/www/friendica/view/templates/field_textarea.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1714c7948_39884859 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<div class="field textarea">
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> <span class="required" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');?>
">*</span><?php }?></label>
		<textarea name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> required<?php }?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
</textarea>
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	</div>
<?php }
}
