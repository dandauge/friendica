<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:56:52
  from '/var/www/friendica/view/templates/admin/blocklist/server.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b174402964_81034649',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '594daca0949871e814ab2c13a7662609fc4d0795' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/blocklist/server.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 4,
    'file:field_checkbox.tpl' => 1,
  ),
),false)) {
function content_6044b174402964_81034649 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	function confirm_delete(uname){
		return confirm("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['confirm_delete']->value, ENT_QUOTES, 'UTF-8');?>
".format(uname));
	}
<?php echo '</script'; ?>
>
<div id="adminpage">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['intro']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<p><?php echo $_smarty_tpl->tpl_vars['public']->value;?>
</p>
	<?php echo $_smarty_tpl->tpl_vars['syntax']->value;?>


	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addtitle']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/blocklist/server" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['newdomain']->value), 0, false);
?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['newreason']->value), 0, true);
?>
		<div class="submit">
			<button type="submit" class="btn btn-primary" name="page_blocklist_save" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
		</div>
	</form>

	<?php if ($_smarty_tpl->tpl_vars['entries']->value) {?>
	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currenttitle']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currentintro']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/blocklist/server" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['entries']->value, 'e');
$_smarty_tpl->tpl_vars['e']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['e']->value) {
$_smarty_tpl->tpl_vars['e']->do_else = false;
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['e']->value['domain']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['e']->value['reason']), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['e']->value['delete']), 0, true);
?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<div class="submit">
			<button type="submit" class="btn btn-primary" name="page_blocklist_edit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['savechanges']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['savechanges']->value, ENT_QUOTES, 'UTF-8');?>
</button>
		</div>
		<?php }?>
	</form>
</div>
<?php }
}
