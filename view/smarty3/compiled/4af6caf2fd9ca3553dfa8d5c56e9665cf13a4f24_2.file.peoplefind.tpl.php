<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:02
  from '/var/www/friendica/view/theme/frio/templates/widget/peoplefind.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2aaaffe27_02644113',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4af6caf2fd9ca3553dfa8d5c56e9665cf13a4f24' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/widget/peoplefind.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b2aaaffe27_02644113 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="peoplefind-sidebar" class="widget">
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['findpeople'], ENT_QUOTES, 'UTF-8');?>
</h3>

	<form action="dirfind" method="get">
				<label for="side-peoplefind-url" id="peoplefind-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['desc'], ENT_QUOTES, 'UTF-8');?>
</label>
		<div class="form-group form-group-search">
			<input id="side-peoplefind-url" class="search-input form-control form-search" type="text" name="search" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['hint'], ENT_QUOTES, 'UTF-8');?>
" />
			<button id="side-peoplefind-submit" class="btn btn-default btn-sm form-button-search" type="submit"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['findthem'], ENT_QUOTES, 'UTF-8');?>
</button>
		</div>
	</form>

		<div class="side-link" id="side-directory-link"><a href="directory" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['local_directory'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-directory-link"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['global_dir'], ENT_QUOTES, 'UTF-8');?>
" target="extlink" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['directory'], ENT_QUOTES, 'UTF-8');?>
</a></div>
		<div class="side-link" id="side-match-link"><a href="match" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['similar'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-suggest-link"><a href="suggest" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['suggest'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-random-profile-link" ><a href="randprof" target="extlink" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['random'], ENT_QUOTES, 'UTF-8');?>
</a></div>

	<?php if ($_smarty_tpl->tpl_vars['nv']->value['inv']) {?> 
	<div class="side-link" id="side-invite-link" ><button type="button" class="btn-link" onclick="addToModal('invite'); return false;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['inv'], ENT_QUOTES, 'UTF-8');?>
</button></div>
	<?php }?>
</div>
<?php }
}
