<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:11:36
  from '/var/www/friendica/view/theme/quattro/templates/theme_settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4e8e62095_40297182',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f47fb2b60896553cb9d8f810aaa9eb707cd73b7f' => 
    array (
      0 => '/var/www/friendica/view/theme/quattro/templates/theme_settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_select.tpl' => 2,
  ),
),false)) {
function content_6044b4e8e62095_40297182 (Smarty_Internal_Template $_smarty_tpl) {
?> <?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/theme/quattro/jquery.tools.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
 
<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['color']->value), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['align']->value), 0, true);
?>


<div class="field">
    <label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pfs']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pfs']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
    <input type="range" class="inputRange" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pfs']->value[0], ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pfs']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pfs']->value[2], ENT_QUOTES, 'UTF-8');?>
" min="10" max="22" step="1"  />
    <span class="field_help"></span>
</div>


<div class="field">
    <label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tfs']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tfs']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
    <input type="range" class="inputRange" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tfs']->value[0], ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tfs']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tfs']->value[2], ENT_QUOTES, 'UTF-8');?>
" min="10" max="22" step="1"  />
    <span class="field_help"></span>
</div>





<div class="settings-submit-wrapper">
	<input type="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" class="settings-submit" name="quattro-settings-submit" />
</div>

<?php echo '<script'; ?>
>
    
    $(".inputRange").rangeinput();
<?php echo '</script'; ?>
>
<?php }
}
