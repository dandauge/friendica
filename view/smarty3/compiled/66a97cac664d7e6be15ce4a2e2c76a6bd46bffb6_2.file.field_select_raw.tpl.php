<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:34:46
  from '/var/www/friendica/view/templates/field_select_raw.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e36ce6a00_72541893',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '66a97cac664d7e6be15ce4a2e2c76a6bd46bffb6' => 
    array (
      0 => '/var/www/friendica/view/templates/field_select_raw.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449e36ce6a00_72541893 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<div class="field select">
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
			<?php echo $_smarty_tpl->tpl_vars['field']->value[4];?>

		</select>
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	</div>
<?php }
}
