<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:12:28
  from '/var/www/friendica/view/theme/duepuntozero/templates/theme_settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b51caaede0_38480635',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6cec5541429e4b14eaa2f5cc30733731bd4a6a0' => 
    array (
      0 => '/var/www/friendica/view/theme/duepuntozero/templates/theme_settings.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_select.tpl' => 1,
  ),
),false)) {
function content_6044b51caaede0_38480635 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/view/theme/quattro/jquery.tools.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['colorset']->value), 0, false);
?> 

<div class="settings-submit-wrapper">
	<input type="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" class="settings-submit" name="duepuntozero-settings-submit" />
</div>

<?php }
}
