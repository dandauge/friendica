<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:35
  from '/var/www/friendica/view/templates/settings/profile/photo/crop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0eb4107a4_30974277',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '60ec912bc22ee77c583a5980b828a3fb3256634b' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/profile/photo/crop.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0eb4107a4_30974277 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<p id="cropimage-desc">
		<?php echo $_smarty_tpl->tpl_vars['desc']->value;?>

	</p>

	<div id="cropimage-wrapper">
		<p><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_url']->value, ENT_QUOTES, 'UTF-8');?>
" id="croppa" class="imgCrop" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
"></p>
	</div>

	<div id="cropimage-preview-wrapper" >
		<div id="previewWrap" class="crop-preview"></div>
	</div>

	<form action="settings/profile/photo/crop/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['resource']->value, ENT_QUOTES, 'UTF-8');?>
" id="crop-image-form" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<input type="hidden" name="xstart" id="x1" />
		<input type="hidden" name="ystart" id="y1" />
		<input type="hidden" name="height" id="height" />
		<input type="hidden" name="width"  id="width" />

		<div id="settings-profile-photo-crop-submit-wrapper" class="pull-right settings-submit-wrapper">
		<?php if ($_smarty_tpl->tpl_vars['skip']->value) {?>
			<button type="submit" name="action" id="settings-profile-photo-crop-skip" class="btn" value="skip"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['skip']->value, ENT_QUOTES, 'UTF-8');?>
</button>
        <?php }?>
			<button type="submit" name="action" id="settings-profile-photo-crop-submit" class="btn btn-primary" value="crop"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['crop']->value, ENT_QUOTES, 'UTF-8');?>
</button>
		</div>

		<div class="clear"></div>
	</form>

	<?php echo '<script'; ?>
 type="text/javascript" language="javascript">

		var image = document.getElementById('croppa');
		var cropper = new Cropper(image, {
			aspectRatio: 1,
			viewMode: 1,
			preview: '#profile-photo-wrapper, .crop-preview',
			crop: function(e) {
				$('#x1').val(e.detail.x);
				$('#y1').val(e.detail.y);
				$('#width').val(e.detail.width);
				$('#height').val(e.detail.height);
			},
		});

		var skip_button = document.getElementById('settings-profile-photo-crop-skip');

		skip_button.addEventListener('click', function() {
			let image_data = cropper.getImageData();
			cropper.setData({x: 0, y: 0, width: image_data.width, height: image_data.height});
		})
	<?php echo '</script'; ?>
>
</div>
<?php }
}
