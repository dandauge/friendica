<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:12:28
  from '/var/www/friendica/view/templates/field_input.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b51caddf07_43978116',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '37b5fa5b1894f165e7ef36ee82bfd29c669ffd86' => 
    array (
      0 => '/var/www/friendica/view/templates/field_input.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b51caddf07_43978116 (Smarty_Internal_Template $_smarty_tpl) {
?>	
	<div class="field input" id="wrapper_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
">
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> <span class="required" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');?>
">*</span><?php }?></label>
		<input type="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['field']->value[6])===null||$tmp==='' ? 'text' : $tmp), ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
"<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> required<?php }
if ($_smarty_tpl->tpl_vars['field']->value[5] == "autofocus") {?> autofocus<?php } elseif ($_smarty_tpl->tpl_vars['field']->value[5]) {?> <?php echo $_smarty_tpl->tpl_vars['field']->value[5];
}?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	</div>
<?php }
}
