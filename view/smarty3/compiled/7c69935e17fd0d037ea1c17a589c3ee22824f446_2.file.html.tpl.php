<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:37:05
  from '/var/www/friendica/view/templates/email/html.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449ec1744644_73153775',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7c69935e17fd0d037ea1c17a589c3ee22824f446' => 
    array (
      0 => '/var/www/friendica/view/templates/email/html.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449ec1744644_73153775 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional //EN">
<html>
<head>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
</head>
<body>
	<table style="border:1px solid #ccc">
	<tbody>
		<tr>
			<td style="background:#084769; color:#FFFFFF; font-weight:bold; font-family:'lucida grande', tahoma, verdana,arial, sans-serif; padding: 4px 8px; vertical-align: middle; font-size:16px; letter-spacing: -0.03em; text-align: left;">
				<img style="width:32px;height:32px; float:left;" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value, ENT_QUOTES, 'UTF-8');?>
" alt="Friendica Banner">
				<div style="padding:7px; margin-left: 5px; float:left; font-size:18px;letter-spacing:1px;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value, ENT_QUOTES, 'UTF-8');?>
</div>
				<div style="clear: both;"></div>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $_smarty_tpl->tpl_vars['htmlversion']->value;?>

			</td>
		</tr>
	</tbody>
	</table>
</body>
</html>
<?php }
}
