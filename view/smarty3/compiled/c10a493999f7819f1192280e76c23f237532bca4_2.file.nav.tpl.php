<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:13:06
  from '/var/www/friendica/view/theme/smoothly/templates/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b542cc1d66_21049004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c10a493999f7819f1192280e76c23f237532bca4' => 
    array (
      0 => '/var/www/friendica/view/theme/smoothly/templates/nav.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b542cc1d66_21049004 (Smarty_Internal_Template $_smarty_tpl) {
?>
<nav>
	<span id="banner"><?php echo $_smarty_tpl->tpl_vars['banner']->value;?>
</span>

	<div id="notifications">	
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?><a id="net-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?><a id="home-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?>
<!--		<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?><a id="intro-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?> -->
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['introductions']) {?><a id="intro-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['introductions'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?><a id="mail-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?><a rel="#nav-notifications-menu" id="notification-update" class="nav-ajax-update" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
"  title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
"></a><?php }?>

		<ul id="nav-notifications-menu" class="menu-popup">
			<li id="nav-notifications-mark-all"><a href="#" onclick="notificationMarkAll(); return false;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['mark'][3], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<li id="nav-notifications-see-all"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications']['all'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<li class="empty"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emptynotifications']->value, ENT_QUOTES, 'UTF-8');?>
</li>
		</ul>
	</div>
	
	<div id="user-menu" >
		<a id="user-menu-label" onclick="openClose('user-menu-popup'); return false" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitelocation']->value, ENT_QUOTES, 'UTF-8');?>
</a>
		
		<ul id="user-menu-popup" 
			 onmouseover="if (typeof tmenu != 'undefined') clearTimeout(tmenu); openMenu('user-menu-popup')" 
			 onmouseout="tmenu=setTimeout('closeMenu(\'user-menu-popup\');',200)">

			<?php if ($_smarty_tpl->tpl_vars['nav']->value['register']) {?><li><a id="nav-register-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['register'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['home']) {?><li><a id="nav-home-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['home'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
		
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['network']) {?><li><a id="nav-network-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['network'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
		
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['community']) {?>
			<li><a id="nav-community-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['community'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php }?>

			<li><a id="nav-search-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['search'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<li><a id="nav-directory-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['directory'][1], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['apps']) {?><li><a id="nav-apps-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['apps'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['notifications']) {?><li><a id="nav-notification-link" class="nav-commlink nav-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['notifications'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['messages']) {?><li><a id="nav-messages-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['messages'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['contacts']) {?><li><a id="nav-contacts-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['contacts'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
		
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['profiles']) {?><li><a id="nav-profiles-link" class="nav-commlink nav-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['profiles'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['settings']) {?><li><a id="nav-settings-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['settings'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['delegation']) {?><li><a id="nav-delegation-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['delegation'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
		
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['admin']) {?><li><a id="nav-admin-link" class="nav-commlink <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['admin'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['help']) {?><li><a id="nav-help-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['help'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['tos']) {?><li><a id="nav-tos-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['tos'][1], ENT_QUOTES, 'UTF-8');?>
</a></li><?php }?>

			<?php if ($_smarty_tpl->tpl_vars['nav']->value['login']) {?><li><a id="nav-login-link" class="nav-link <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['login'][1], ENT_QUOTES, 'UTF-8');?>
</a></li> <?php }?>
			<?php if ($_smarty_tpl->tpl_vars['nav']->value['logout']) {?><li><a id="nav-logout-link" class="nav-commlink nav-sep <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][2], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nav']->value['logout'][1], ENT_QUOTES, 'UTF-8');?>
</a></li> <?php }?>
		</ul>
	</div>
</nav>

<ul id="nav-notifications-template" style="display:none;" rel="template">
	<li class="{4}"><a href="{0}"><img data-src="{1}" height="24" width="24" alt="" />{2} <span class="notif-when">{3}</span></a></li>
</ul>

<div style="position: fixed; top: 3px; left: 5px; z-index:9999"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['langselector']->value, ENT_QUOTES, 'UTF-8');?>
</div>

<?php echo '<script'; ?>
>
var pagetitle = null;
$("nav").bind('nav-update', function(e,data){
if (pagetitle==null) pagetitle = document.title;
var count = $(data).find('notif').attr('count');
if (count>0) {
document.title = "("+count+") "+pagetitle;
} else {
document.title = pagetitle;
}
});
<?php echo '</script'; ?>
>
<?php }
}
