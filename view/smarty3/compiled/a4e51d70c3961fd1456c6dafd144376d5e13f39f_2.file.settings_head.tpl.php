<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:42
  from '/var/www/friendica/view/templates/admin/settings_head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1e2adbb55_87643926',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4e51d70c3961fd1456c6dafd144376d5e13f39f' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/settings_head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1e2adbb55_87643926 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	$(document).ready(function() {
		$('.settings-content-block').hide();
		$('.settings-heading').click(function(){
			$('.settings-content-block').hide();
			$(this).next('.settings-content-block').toggle();
		});
	});
<?php echo '</script'; ?>
>
<?php }
}
