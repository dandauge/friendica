<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/field_fileinput.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b86b5550_55447668',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6408245655a6062fd078e401fca41b6fe2330222' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_fileinput.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b86b5550_55447668 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group field input file">
	<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="label_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> <span class="required" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');?>
">*</span><?php }?></label>
	<div class="input-group" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
">
		<input class="form-control file" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" type="text" value="<?php echo $_smarty_tpl->tpl_vars['field']->value[2];?>
"<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> required<?php }?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
		<span class="input-group-addon image-select"><i class="fa fa-picture-o"></i></span>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
	<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	<div id="end_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" class="field_end"></div>
</div>
<?php }
}
