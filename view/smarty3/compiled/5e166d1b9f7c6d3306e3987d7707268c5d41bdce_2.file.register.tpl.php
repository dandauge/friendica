<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:36:08
  from '/var/www/friendica/view/templates/register.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e887052e6_91147641',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e166d1b9f7c6d3306e3987d7707268c5d41bdce' => 
    array (
      0 => '/var/www/friendica/view/templates/register.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_password.tpl' => 3,
    'file:field_textarea.tpl' => 1,
  ),
),false)) {
function content_60449e887052e6_91147641 (Smarty_Internal_Template $_smarty_tpl) {
?><h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['regtitle']->value, ENT_QUOTES, 'UTF-8');?>
</h3>

<form action="register" method="post" id="register-form">

	<input type="hidden" name="photo" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['photo']->value, ENT_QUOTES, 'UTF-8');?>
" />
	<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

	<?php if ($_smarty_tpl->tpl_vars['registertext']->value != '') {?><div class="error-message"><?php echo $_smarty_tpl->tpl_vars['registertext']->value;?>
 </div><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['explicit_content']->value) {?> <p id="register-explicid-content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['explicit_content_note']->value, ENT_QUOTES, 'UTF-8');?>
</p> <?php }?>

	<p id="register-fill-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fillwith']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<p id="register-fill-ext"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fillext']->value, ENT_QUOTES, 'UTF-8');?>
</p>

<?php if ($_smarty_tpl->tpl_vars['oidlabel']->value) {?>
	<div id="register-openid-wrapper" >
	<label for="register-openid" id="label-register-openid" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oidlabel']->value, ENT_QUOTES, 'UTF-8');?>
</label><input type="text" maxlength="60" size="32" name="openid_url" class="openid" id="register-openid" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['openid']->value, ENT_QUOTES, 'UTF-8');?>
" >
	</div>
	<div id="register-openid-end" ></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['invitations']->value) {?>
	<p id="register-invite-desc"><?php echo $_smarty_tpl->tpl_vars['invite_desc']->value;?>
</p>
	<div id="register-invite-wrapper" >
		<label for="register-invite" id="label-register-invite" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['invite_label']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="text" maxlength="60" size="32" name="invite_id" id="register-invite" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['invite_id']->value, ENT_QUOTES, 'UTF-8');?>
" >
	</div>
	<div id="register-name-end" ></div>
<?php }?>

	<div id="register-name-wrapper" >
		<label for="register-name" id="label-register-name" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namelabel']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="text" maxlength="60" size="32" name="username" id="register-name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['username']->value, ENT_QUOTES, 'UTF-8');?>
" required>
	</div>
	<div id="register-name-end" ></div>


	<?php if (!$_smarty_tpl->tpl_vars['additional']->value) {?>
		<div id="register-email-wrapper" >
			<label for="register-email" id="label-register-email" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addrlabel']->value, ENT_QUOTES, 'UTF-8');?>
</label>
			<input type="text" maxlength="60" size="32" name="field1" id="register-email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8');?>
" required>
		</div>
		<div id="register-email-end" ></div>

		<div id="register-repeat-wrapper" >
			<label for="register-repeat" id="label-register-repeat" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addrlabel2']->value, ENT_QUOTES, 'UTF-8');?>
</label>
			<input type="text" maxlength="60" size="32" name="repeat" id="register-repeat" value="" required>
		</div>
		<div id="register-repeat-end" ></div>
	<?php }?>

<?php if ($_smarty_tpl->tpl_vars['ask_password']->value) {?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password1']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['password2']->value), 0, true);
}?>

	<p id="register-nickname-desc" ><?php echo $_smarty_tpl->tpl_vars['nickdesc']->value;?>
</p>

	<div id="register-nickname-wrapper" >
		<label for="register-nickname" id="label-register-nickname" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nicklabel']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="text" maxlength="60" size="32" name="nickname" id="register-nickname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nickname']->value, ENT_QUOTES, 'UTF-8');?>
" required><div id="register-sitename">@<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sitename']->value, ENT_QUOTES, 'UTF-8');?>
</div>
	</div>
	<div id="register-nickname-end" ></div>

	<input type="input" id=tarpit" name="email" style="display: none;" placeholder="Don't enter anything here"/>

	<?php if ($_smarty_tpl->tpl_vars['additional']->value) {?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['parent_password']->value), 0, true);
?>
	<?php }?>

<?php if ($_smarty_tpl->tpl_vars['permonly']->value) {?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_textarea.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['permonlybox']->value), 0, false);
}?>

	<?php echo $_smarty_tpl->tpl_vars['publish']->value;?>


<?php if ($_smarty_tpl->tpl_vars['showtoslink']->value) {?>
	<p><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/tos"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tostext']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>
<?php }
if ($_smarty_tpl->tpl_vars['showprivstatement']->value) {?>
	<h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['privstatement']->value[0], ENT_QUOTES, 'UTF-8');?>
</h4>
	<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 3+1 - (1) : 1-(3)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration === 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration === $_smarty_tpl->tpl_vars['i']->total;?>
	<p><?php echo $_smarty_tpl->tpl_vars['privstatement']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</p>
	<?php }
}
}?>

	<div id="register-submit-wrapper">
		<input type="submit" name="submit" id="register-submit-button" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['regbutt']->value, ENT_QUOTES, 'UTF-8');?>
" />
	</div>
	<div id="register-submit-end" ></div>

	<?php if (!$_smarty_tpl->tpl_vars['additional']->value) {?>
		<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importh']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
		<div id ="import-profile">
			<a href="uimport"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['importt']->value, ENT_QUOTES, 'UTF-8');?>
</a>
		</div>
	<?php }?>
</form>
<?php }
}
