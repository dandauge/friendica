<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:28
  from '/var/www/friendica/view/templates/field_intcheckbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1983b6168_27112670',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dfd6b9a3acdd2c843d3e825bf6fff289e5400ba5' => 
    array (
      0 => '/var/www/friendica/view/templates/field_intcheckbox.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1983b6168_27112670 (Smarty_Internal_Template $_smarty_tpl) {
?>
	
	<div class='field checkbox'>
		<label for='id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="checkbox" name='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
' id='id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
' value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[3], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[2]) {?>checked="true"<?php }?> aria-describedby='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip'>
		<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?>
		<span class='field_help' role='tooltip' id='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip'><?php echo $_smarty_tpl->tpl_vars['field']->value[4];?>
</span>
		<?php }?>
	</div>
<?php }
}
