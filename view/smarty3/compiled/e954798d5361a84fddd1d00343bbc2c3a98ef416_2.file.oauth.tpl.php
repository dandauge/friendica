<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:03:18
  from '/var/www/friendica/view/theme/frio/templates/settings/oauth.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2f6cc4f57_41277395',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e954798d5361a84fddd1d00343bbc2c3a98ef416' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/oauth.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:section_title.tpl' => 1,
  ),
),false)) {
function content_6044b2f6cc4f57_41277395 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
		<?php $_smarty_tpl->_subTemplateRender("file:section_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['title']->value), 0, false);
?>


	<form action="settings/oauth" method="post" autocomplete="off">
		<input type='hidden' name='form_security_token' value='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
'>

		<div id="profile-edit-links">
			<ul>
				
				<li role="menuitem">
					<a id="profile-edit-view-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/settings/oauth/add"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['add']->value, ENT_QUOTES, 'UTF-8');?>
</a>
				</li>
			</ul>
		</div>

		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['apps']->value, 'app');
$_smarty_tpl->tpl_vars['app']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['app']->value) {
$_smarty_tpl->tpl_vars['app']->do_else = false;
?>
		<div class='oauthapp'>
			<img src='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value['icon'], ENT_QUOTES, 'UTF-8');?>
' class="<?php if ($_smarty_tpl->tpl_vars['app']->value['icon']) {?> <?php } else { ?>noicon<?php }?>">
			<?php if ($_smarty_tpl->tpl_vars['app']->value['name']) {?><h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h4><?php } else { ?><h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['noname']->value, ENT_QUOTES, 'UTF-8');?>
</h4><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['app']->value['my']) {?>
				<?php if ($_smarty_tpl->tpl_vars['app']->value['oauth_token']) {?>
				<div class="settings-submit-wrapper" ><button class="settings-submit"  type="submit" name="remove" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value['oauth_token'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['remove']->value, ENT_QUOTES, 'UTF-8');?>
</button></div>
				<?php }?>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['app']->value['my']) {?>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/settings/oauth/edit/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value['client_id'], ENT_QUOTES, 'UTF-8');?>
" class="btn" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['edit']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;</a>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/settings/oauth/delete/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value['client_id'], ENT_QUOTES, 'UTF-8');?>
?t=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
" class="btn" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-trash" aria-hidden="true"></i></a>
			<?php }?>
		</div>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	</form>
</div>
<?php }
}
