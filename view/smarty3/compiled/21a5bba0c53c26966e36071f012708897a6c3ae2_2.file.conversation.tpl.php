<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/conversation.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f37d97e5_22233400',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21a5bba0c53c26966e36071f012708897a6c3ae2' => 
    array (
      0 => '/var/www/friendica/view/templates/conversation.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f37d97e5_22233400 (Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['live_update']->value;?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['threads']->value, 'thread');
$_smarty_tpl->tpl_vars['thread']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['thread']->value) {
$_smarty_tpl->tpl_vars['thread']->do_else = false;
?>
<div id="tread-wrapper-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="tread-wrapper">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['thread']->value['items'], 'item');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['item']->value['comment_firstcollapsed']) {?>
			<div class="hide-comments-outer">
			<span id="hide-comments-total-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="hide-comments-total"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['num_comments'], ENT_QUOTES, 'UTF-8');?>
</span> <span id="hide-comments-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="hide-comments fakelink" onclick="showHideComments(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['id'], ENT_QUOTES, 'UTF-8');?>
);"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['hide_text'], ENT_QUOTES, 'UTF-8');?>
</span>
			</div>
			<div id="collapsed-comments-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thread']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="collapsed-comments" style="display: none;">
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['item']->value['comment_lastcollapsed']) {?></div><?php }?>
		
		<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['item']->value['template']), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
		
		
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
if (!$_smarty_tpl->tpl_vars['update']->value) {?>
<div id="conversation-end"></div>

<?php if ($_smarty_tpl->tpl_vars['dropping']->value) {?>
<div id="item-delete-selected" class="fakelink" onclick="deleteCheckedItems();">
  <div id="item-delete-selected-icon" class="icon drophide" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropping']->value, ENT_QUOTES, 'UTF-8');?>
" onmouseover="imgbright(this);" onmouseout="imgdull(this);" ></div>
  <div id="item-delete-selected-desc" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropping']->value, ENT_QUOTES, 'UTF-8');?>
</div>
</div>
<div id="item-delete-selected-end"></div>
<?php }
}
}
}
