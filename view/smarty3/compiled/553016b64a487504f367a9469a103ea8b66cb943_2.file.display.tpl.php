<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/settings/display.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b86cf713_28107420',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '553016b64a487504f367a9469a103ea8b66cb943' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/settings/display.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_themeselect.tpl' => 2,
    'file:field_input.tpl' => 3,
    'file:field_checkbox.tpl' => 7,
    'file:field_select.tpl' => 1,
  ),
),false)) {
function content_6044b1b86cf713_28107420 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="generic-page-wrapper">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ptitle']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<form action="settings/display" id="settings-form" method="post" autocomplete="off" >
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">

		<div class="panel-group panel-group-settings" id="settings" role="tablist" aria-multiselectable="true">
			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="theme-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#theme-settings-content" aria-expanded="true" aria-controls="theme-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['d_tset']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>

				<div id="theme-settings-content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="theme-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_themeselect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['theme']->value), 0, false);
?>

												<?php if (count($_smarty_tpl->tpl_vars['mobile_theme']->value[4]) > 1) {?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_themeselect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mobile_theme']->value), 0, true);
?>
						<?php }?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="custom-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#custom-settings-content" aria-expanded="false" aria-controls="custom-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['d_ctset']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="custom-settings-content" class="panel-collapse collapse<?php if (!$_smarty_tpl->tpl_vars['theme']->value && !$_smarty_tpl->tpl_vars['mobile_theme']->value) {?> in<?php }?>" role="tabpanel" aria-labelledby="custom-settings">
					<div class="panel-body">

					<?php if ($_smarty_tpl->tpl_vars['theme_config']->value) {?>
						<?php echo $_smarty_tpl->tpl_vars['theme_config']->value;?>

					<?php }?>

					</div>
				</div>
			</div>

			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="content-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#content-settings-content" aria-expanded="false" aria-controls="content-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['d_cset']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="content-settings-content" class="panel-collapse collapse<?php if (!$_smarty_tpl->tpl_vars['theme']->value && !$_smarty_tpl->tpl_vars['mobile_theme']->value && !$_smarty_tpl->tpl_vars['theme_config']->value) {?> in<?php }?>" role="tabpanel" aria-labelledby="content-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['itemspage_network']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['itemspage_mobile_network']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['ajaxint']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['no_auto_update']->value), 0, false);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['nosmile']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['infinite_scroll']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['no_smart_threading']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_dislike']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['display_resharer']->value), 0, true);
?>
						<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['stay_local']->value), 0, true);
?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="section-subtitle-wrapper panel-heading" role="tab" id="calendar-settings-title">
					<h2>
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#settings" href="#calendar-settings-content" aria-expanded="false" aria-controls="calendar-settings-content">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['calendar_title']->value, ENT_QUOTES, 'UTF-8');?>

						</a>
					</h2>
				</div>
				<div id="calendar-settings-content" class="panel-collapse collapse<?php if (!$_smarty_tpl->tpl_vars['theme']->value && !$_smarty_tpl->tpl_vars['mobile_theme']->value && !$_smarty_tpl->tpl_vars['theme_config']->value) {?> in<?php }?>" role="tabpanel" aria-labelledby="calendar-settings">
					<div class="panel-body">
						<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['first_day_of_week']->value), 0, false);
?>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<?php }
}
