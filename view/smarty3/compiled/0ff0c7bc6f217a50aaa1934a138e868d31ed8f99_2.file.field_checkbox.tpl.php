<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:13:06
  from '/var/www/friendica/view/templates/field_checkbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b542c4fec5_55270207',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0ff0c7bc6f217a50aaa1934a138e868d31ed8f99' => 
    array (
      0 => '/var/www/friendica/view/templates/field_checkbox.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b542c4fec5_55270207 (Smarty_Internal_Template $_smarty_tpl) {
?>	<div class="field checkbox" id="div_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
">
		<label id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_label" for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" value="0">
		<input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" value="1" <?php if ($_smarty_tpl->tpl_vars['field']->value[2]) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');
}?>>
		<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
		<?php }?>
	</div>
<?php }
}
