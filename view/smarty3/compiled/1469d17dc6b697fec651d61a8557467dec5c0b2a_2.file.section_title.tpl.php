<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:02
  from '/var/www/friendica/view/templates/section_title.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2aab1b9e6_48353581',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1469d17dc6b697fec651d61a8557467dec5c0b2a' => 
    array (
      0 => '/var/www/friendica/view/templates/section_title.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b2aab1b9e6_48353581 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['title']->value) {?>
<div class="section-title-wrapper<?php if ((isset($_smarty_tpl->tpl_vars['pullright']->value))) {?> pull-left<?php }?>">
	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<?php if (!(isset($_smarty_tpl->tpl_vars['pullright']->value))) {?>
	<div class="clear"></div>
	<?php }?>
</div>
<?php }
}
}
