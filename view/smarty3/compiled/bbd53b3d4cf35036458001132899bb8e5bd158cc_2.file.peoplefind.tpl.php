<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:45
  from '/var/www/friendica/view/templates/widget/peoplefind.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f5c4f521_41007783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bbd53b3d4cf35036458001132899bb8e5bd158cc' => 
    array (
      0 => '/var/www/friendica/view/templates/widget/peoplefind.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f5c4f521_41007783 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="peoplefind-sidebar" class="widget">
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['findpeople'], ENT_QUOTES, 'UTF-8');?>
</h3>
	<div id="peoplefind-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['desc'], ENT_QUOTES, 'UTF-8');?>
</div>
	<form action="dirfind" method="get" />
		<input id="side-peoplefind-url" type="text" name="search" size="24" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['hint'], ENT_QUOTES, 'UTF-8');?>
" /><input id="side-peoplefind-submit" type="submit" name="submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['findthem'], ENT_QUOTES, 'UTF-8');?>
" />
	</form>
	<div class="side-link" id="side-match-link"><a href="match" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['similar'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-suggest-link"><a href="suggest" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['suggest'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-directory-link"><a href="directory" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['local_directory'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-directory-link"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['global_dir'], ENT_QUOTES, 'UTF-8');?>
" target="extlink" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['directory'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<div class="side-link" id="side-random-profile-link" ><a href="randprof" target="extlink" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['random'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<?php if ($_smarty_tpl->tpl_vars['nv']->value['inv']) {?> 
	<div class="side-link" id="side-invite-link" ><a href="invite" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nv']->value['inv'], ENT_QUOTES, 'UTF-8');?>
</a></div>
	<?php }?>
</div>

<?php }
}
