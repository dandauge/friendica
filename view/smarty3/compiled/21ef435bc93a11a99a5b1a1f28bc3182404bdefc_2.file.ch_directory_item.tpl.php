<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:45
  from '/var/www/friendica/view/theme/vier/templates/ch_directory_item.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f5c07650_42770052',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21ef435bc93a11a99a5b1a1f28bc3182404bdefc' => 
    array (
      0 => '/var/www/friendica/view/theme/vier/templates/ch_directory_item.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f5c07650_42770052 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="directory-item" id="directory-item-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" >
	<div class="directory-photo-wrapper" id="directory-photo-wrapper-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" > 
		<div class="directory-photo" id="directory-photo-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" >
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_link']->value, ENT_QUOTES, 'UTF-8');?>
" class="directory-profile-link" id="directory-profile-link-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" >
				<img class="directory-photo-img" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['photo']->value, ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['alt_text']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['alt_text']->value, ENT_QUOTES, 'UTF-8');?>
" />
			</a>
		</div>
	</div>
</div>
<?php }
}
