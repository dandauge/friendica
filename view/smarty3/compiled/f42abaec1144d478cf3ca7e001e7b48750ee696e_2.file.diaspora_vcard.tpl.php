<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:43
  from '/var/www/friendica/view/templates/diaspora_vcard.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0f370dda9_96939071',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f42abaec1144d478cf3ca7e001e7b48750ee696e' => 
    array (
      0 => '/var/www/friendica/view/templates/diaspora_vcard.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0f370dda9_96939071 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['diaspora']->value) {?>
<div style="display:none;">
	<dl class="entity_uid">
		<dt>Uid</dt>
		<dd>
			<span class="uid p-uid"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['guid'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class='entity_nickname'>
		<dt>Nickname</dt>
		<dd>		
			<span class="nickname p-nickname"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['nickname'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class='entity_full_name'>
		<dt>Full_name</dt>
		<dd>
			<span class='fn p-name'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['fullname'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class="entity_searchable">
		<dt>Searchable</dt>
		<dd>
			<span class="searchable"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['searchable'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class='entity_first_name'>
		<dt>First_name</dt>
		<dd>
		<span class='given_name p-given-name'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class='entity_family_name'>
		<dt>Family_name</dt>
		<dd>
		<span class='family_name p-family-name'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</span>
		</dd>
	</dl>
	<dl class="entity_url">
		<dt>Url</dt>
		<dd>
			<a id="pod_location" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['podloc'], ENT_QUOTES, 'UTF-8');?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['podloc'], ENT_QUOTES, 'UTF-8');?>
/</a>
		</dd>
	</dl>
	<dl class="entity_photo">
		<dt>Photo</dt>
		<dd>
			<img class="photo u-photo avatar" height="300" width="300" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['photo300'], ENT_QUOTES, 'UTF-8');?>
">
		</dd>
	</dl>
	<dl class="entity_photo_medium">
		<dt>Photo_medium</dt>
		<dd> 
			<img class="photo u-photo avatar" height="100" width="100" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['photo100'], ENT_QUOTES, 'UTF-8');?>
">
		</dd>
	</dl>
	<dl class="entity_photo_small">
		<dt>Photo_small</dt>
		<dd>
			<img class="photo u-photo avatar" height="50" width="50" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['diaspora']->value['photo50'], ENT_QUOTES, 'UTF-8');?>
">
		</dd>
	</dl>
</div>
<?php }
}
}
