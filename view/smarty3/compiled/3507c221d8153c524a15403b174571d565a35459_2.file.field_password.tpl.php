<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:32
  from '/var/www/friendica/view/theme/frio/templates/field_password.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1d875e876_04215526',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3507c221d8153c524a15403b174571d565a35459' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_password.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1d875e876_04215526 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_wrapper" class="form-group field input password">
	<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="label_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> <span class="required" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[4], ENT_QUOTES, 'UTF-8');?>
">*</span><?php }?></label>
	<input class="form-control" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" type="password" value="<?php echo $_smarty_tpl->tpl_vars['field']->value[2];?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?> required<?php }
if ($_smarty_tpl->tpl_vars['field']->value[5] == "autofocus") {?> autofocus<?php } elseif ($_smarty_tpl->tpl_vars['field']->value[5]) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[5], ENT_QUOTES, 'UTF-8');
}?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
	<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	<div class="clear"></div>
</div>
<?php }
}
