<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:11:36
  from '/var/www/friendica/view/templates/field_themeselect.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b4e8e86000_39988908',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8929eedff165240f858e148725c45709c8d478cc' => 
    array (
      0 => '/var/www/friendica/view/templates/field_themeselect.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b4e8e86000_39988908 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['field']->value[5]) {?>
	<?php echo '<script'; ?>
>$(function(){ previewTheme($("#id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
")[0]); });<?php echo '</script'; ?>
>
<?php }?>
	<div class="field select">
		<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>
</label>
		<select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[5]) {?>onchange="previewTheme(this);"<?php }?> aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['field']->value[4], 'val', false, 'opt');
$_smarty_tpl->tpl_vars['val']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['opt']->value => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->do_else = false;
?>
			<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opt']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['opt']->value == $_smarty_tpl->tpl_vars['field']->value[2]) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['val']->value, ENT_QUOTES, 'UTF-8');?>
</option>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</select>
	<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
		<span class="field_help" role="tooltip" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_tip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['field']->value[5]) {?>
		<div id="theme-preview"></div>
	<?php }?>
	</div>
<?php }
}
