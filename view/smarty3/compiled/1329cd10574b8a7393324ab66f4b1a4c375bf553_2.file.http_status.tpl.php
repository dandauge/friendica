<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:37:33
  from '/var/www/friendica/view/templates/http_status.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449edd9b07a5_13831708',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1329cd10574b8a7393324ab66f4b1a4c375bf553' => 
    array (
      0 => '/var/www/friendica/view/templates/http_status.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449edd9b07a5_13831708 (Smarty_Internal_Template $_smarty_tpl) {
?><html>
	<head>
		<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</title>
	</head>
	<body>
		<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
		<p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
	<?php if ($_smarty_tpl->tpl_vars['trace']->value) {?>
		<pre><?php echo $_smarty_tpl->tpl_vars['trace']->value;?>
</pre>
	<?php }?>
	</body>
</html>
<?php }
}
