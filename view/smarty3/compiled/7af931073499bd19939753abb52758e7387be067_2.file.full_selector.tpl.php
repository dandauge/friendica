<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/templates/acl/full_selector.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291b4d532_24009580',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7af931073499bd19939753abb52758e7387be067' => 
    array (
      0 => '/var/www/friendica/view/templates/acl/full_selector.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_checkbox.tpl' => 1,
    'file:field_select.tpl' => 1,
  ),
),false)) {
function content_6044b291b4d532_24009580 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="acl-wrapper">
	<div class="panel-group" id="visibility-accordion-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" role="tablist" aria-multiselectable="true">
		<div class="panel panel-success">
			<label class="panel-heading<?php if ($_smarty_tpl->tpl_vars['visibility']->value != 'public') {?> collapsed<?php }?>" id="visibility-public-heading-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'public') {?>true<?php } else { ?>false<?php }?>">
				<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['visibility'], ENT_QUOTES, 'UTF-8');?>
" id="visibility-public-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" value="public" tabindex="14" <?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'public') {?>checked<?php }?>>
				<i class="fa fa-globe"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['public_title']->value, ENT_QUOTES, 'UTF-8');?>

			</label>
			<fieldset id="visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="panel-collapse collapse<?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'public') {?> in<?php }?>" role="tabpanel" aria-labelledby="visibility-public-heading-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['visibility']->value != 'public') {?>disabled<?php }?>>
				<div class="panel-body">
					<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['public_desc']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	                <?php if ($_smarty_tpl->tpl_vars['for_federation']->value) {?>
		                <?php if ($_smarty_tpl->tpl_vars['jotnets_fields']->value) {?>
		                    <?php if (count($_smarty_tpl->tpl_vars['jotnets_fields']->value) < 3) {?>
								<div class="profile-jot-net">
		                    <?php } else { ?>
								<details class="profile-jot-net">
								<summary><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['jotnets_summary']->value, ENT_QUOTES, 'UTF-8');?>
</summary>
		                    <?php }?>

		                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jotnets_fields']->value, 'jotnets_field');
$_smarty_tpl->tpl_vars['jotnets_field']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['jotnets_field']->value) {
$_smarty_tpl->tpl_vars['jotnets_field']->do_else = false;
?>
		                        <?php if ($_smarty_tpl->tpl_vars['jotnets_field']->value['type'] == 'checkbox') {?>
		                            <?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['jotnets_field']->value['field']), 0, true);
?>
		                        <?php } elseif ($_smarty_tpl->tpl_vars['jotnets_field']->value['type'] == 'select') {?>
		                            <?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['jotnets_field']->value['field']), 0, true);
?>
		                        <?php }?>
		                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

		                    <?php if (count($_smarty_tpl->tpl_vars['jotnets_fields']->value) >= 3) {?>
								</details>
		                    <?php } else { ?>
								</div>
		                    <?php }?>
			            <?php }?>
	                <?php }?>
				</div>
			</fieldset>
		</div>
		<div class="panel panel-info">
			<label class="panel-heading<?php if ($_smarty_tpl->tpl_vars['visibility']->value != 'custom') {?> collapsed<?php }?>" id="visibility-custom-heading-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'custom') {?>true<?php } else { ?>false<?php }?>">
				<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['visibility'], ENT_QUOTES, 'UTF-8');?>
" id="visibility-custom-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" value="custom" tabindex="15" <?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'custom') {?>checked<?php }?>>
				<i class="fa fa-lock"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['custom_title']->value, ENT_QUOTES, 'UTF-8');?>

			</label>
			<fieldset id="visibility-custom-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="panel-collapse collapse<?php if ($_smarty_tpl->tpl_vars['visibility']->value == 'custom') {?> in<?php }?>" role="tabpanel" aria-labelledby="visibility-custom-heading-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['visibility']->value != 'custom') {?>disabled<?php }?>>
				<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['group_allow'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group_allow']->value, ENT_QUOTES, 'UTF-8');?>
"/>
				<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['contact_allow'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_allow']->value, ENT_QUOTES, 'UTF-8');?>
"/>
				<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['group_deny'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group_deny']->value, ENT_QUOTES, 'UTF-8');?>
"/>
				<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['contact_deny'], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_deny']->value, ENT_QUOTES, 'UTF-8');?>
"/>
				<div class="panel-body">
					<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['custom_desc']->value, ENT_QUOTES, 'UTF-8');?>
</p>

					<div class="form-group">
						<label for="acl_allow-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['allow_label']->value, ENT_QUOTES, 'UTF-8');?>
</label>
						<input type="text" class="form-control input-lg" id="acl_allow-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
">
					</div>

					<div class="form-group">
						<label for="acl_deny-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['deny_label']->value, ENT_QUOTES, 'UTF-8');?>
</label>
						<input type="text" class="form-control input-lg" id="acl_deny-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
">
					</div>
				</div>
			</fieldset>
		</div>
	</div>


<?php if ($_smarty_tpl->tpl_vars['for_federation']->value) {?>
	<div class="form-group">
		<label for="profile-jot-email" id="profile-jot-email-label-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emailcc']->value, ENT_QUOTES, 'UTF-8');?>
</label>
		<input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['emailcc'], ENT_QUOTES, 'UTF-8');?>
" id="profile-jot-email-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="form-control" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emtitle']->value, ENT_QUOTES, 'UTF-8');?>
" />
	</div>
	<div id="profile-jot-email-end-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
"></div>
<?php }?>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$(function() {
		let $acl_allow_input = $('#acl_allow-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');
		let $contact_allow_input = $('[name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['contact_allow'], ENT_QUOTES, 'UTF-8');?>
"]');
		let $group_allow_input = $('[name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['group_allow'], ENT_QUOTES, 'UTF-8');?>
"]');
		let $acl_deny_input = $('#acl_deny-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');
		let $contact_deny_input = $('[name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['contact_deny'], ENT_QUOTES, 'UTF-8');?>
"]');
		let $group_deny_input = $('[name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_names']->value['group_deny'], ENT_QUOTES, 'UTF-8');?>
"]');
		let $visibility_public_panel = $('#visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');
		let $visibility_custom_panel = $('#visibility-custom-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');
		let $visibility_public_radio = $('#visibility-public-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');
		let $visibility_custom_radio = $('#visibility-custom-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
');

		// Frio specific
		if ($.fn.collapse) {
			$visibility_public_panel.collapse({parent: '#visibility-accordion-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
', toggle: false});
			$visibility_custom_panel.collapse({parent: '#visibility-accordion-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
', toggle: false});
		}

		$visibility_public_radio.on('change', function (e) {
			if ($.fn.collapse) {
				$visibility_public_panel.collapse('show');
			}

			$visibility_public_panel.prop('disabled', false);
			$visibility_custom_panel.prop('disabled', true);

			$('#visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
 .profile-jot-net input[type=checkbox]').each(function() {
				// Restores checkbox state if it had been saved
				if ($(this).attr('data-checked') !== undefined) {
					$(this).prop('checked', $(this).attr('data-checked') === 'true');
				}
			});
			$('#visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
 .profile-jot-net input').attr('disabled', false);
		});

		$visibility_custom_radio.on('change', function(e) {
			if ($.fn.collapse) {
				$visibility_custom_panel.collapse('show');
			}

			$visibility_public_panel.prop('disabled', true);
			$visibility_custom_panel.prop('disabled', false);

			$('#visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
 .profile-jot-net input[type=checkbox]').each(function() {
				// Saves current checkbox state
				$(this)
					.attr('data-checked', $(this).prop('checked'))
					.prop('checked', false);
			});
			$('#visibility-public-panel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_group_id']->value, ENT_QUOTES, 'UTF-8');?>
 .profile-jot-net input').attr('disabled', 'disabled');
		});

		// Custom visibility tags inputs
		let acl_groups = new Bloodhound({
			local: <?php echo json_encode($_smarty_tpl->tpl_vars['acl_groups']->value);?>
,
			identify: function(obj) { return obj.type + '-' + obj.id.toString(); },
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace(['name']),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
		});
		let acl_contacts = new Bloodhound({
			local: <?php echo json_encode($_smarty_tpl->tpl_vars['acl_contacts']->value);?>
,
			identify: function(obj) { return obj.type + '-' + obj.id.toString(); },
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace(['name', 'addr']),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
		});
		let acl = new Bloodhound({
			local: <?php echo json_encode($_smarty_tpl->tpl_vars['acl_list']->value);?>
,
			identify: function(obj) { return obj.type + '-' + obj.id.toString(); },
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace(['name', 'addr']),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			sorter: function (itemA, itemB) {
				if (itemA.name === itemB.name) {
					return 0;
				} else if (itemA.name > itemB.name) {
					return 1;
				} else {
					return -1;
				}
			},
		});
		acl.initialize();

		let suggestionTemplate = function (item) {
			return '<div><img src="' + item.micro + '" alt="" style="float: left; width: auto; height: 2.8em; margin-right: 0.5em;"><p style="margin-left: 3.3em;"><strong>' + item.name + '</strong><br /><em>' + item.addr + '</em></p></div>';
		};

		$acl_allow_input.tagsinput({
			confirmKeys: [13, 44],
			freeInput: false,
			tagClass: function(item) {
				switch (item.type) {
					case 'group'   : return 'label label-primary';
					case 'contact'  :
					default:
						return 'label label-info';
				}
			},
			itemValue: function (item) { return item.type + '-' + item.id.toString(); },
			itemText: 'name',
			itemThumb: 'micro',
			itemTitle: function(item) {
				return item.addr;
			},
			typeaheadjs: {
				name: 'contacts',
				displayKey: 'name',
				templates: {
					suggestion: suggestionTemplate
				},
				source: acl.ttAdapter()
			}
		});

		$acl_deny_input
			.tagsinput({
				confirmKeys: [13, 44],
				freeInput: false,
				tagClass: function(item) {
					switch (item.type) {
						case 'group'   : return 'label label-primary';
						case 'contact'  :
						default:
							return 'label label-info';
					}
				},
				itemValue: function (item) { return item.type + '-' + item.id.toString(); },
				itemText: 'name',
				itemThumb: 'micro',
				itemTitle: function(item) {
					return item.addr;
				},
				typeaheadjs: {
					name: 'contacts',
					displayKey: 'name',
					templates: {
						suggestion: suggestionTemplate
					},
					source: acl.ttAdapter()
				}
			});

		// Import existing ACL into the tags input fields.

		$group_allow_input.val().split(',').forEach(function (group_id) {
			$acl_allow_input.tagsinput('add', acl_groups.get('group-' + group_id)[0]);
		});
		$contact_allow_input.val().split(',').forEach(function (contact_id) {
			$acl_allow_input.tagsinput('add', acl_contacts.get('contact-' + contact_id)[0]);
		});
		$group_deny_input.val().split(',').forEach(function (group_id) {
			$acl_deny_input.tagsinput('add', acl_groups.get('group-' + group_id)[0]);
		});
		$contact_deny_input.val().split(',').forEach(function (contact_id) {
			$acl_deny_input.tagsinput('add', acl_contacts.get('contact-' + contact_id)[0]);
		});

		// Anti-duplicate callback + acl fields value generation

		$acl_allow_input.on('itemAdded itemRemoved', function (event) {
			if (event.type === 'itemAdded') {
				// Removes duplicate in the opposite acl box
				$acl_deny_input.tagsinput('remove', event.item);
			}

			// Update the real acl field
			$group_allow_input.val('');
			$contact_allow_input.val('');
			[].forEach.call($acl_allow_input.tagsinput('items'), function (item) {
				if (item.type === 'group') {
					$group_allow_input.val($group_allow_input.val() + ',' + item.id);
				} else {
					$contact_allow_input.val($contact_allow_input.val() + ',' + item.id);
				}
			});
		});

		$acl_deny_input.on('itemAdded itemRemoved', function (event) {
			if (event.type === 'itemAdded') {
				// Removes duplicate in the opposite acl box
				$acl_allow_input.tagsinput('remove', event.item);
			}

			// Update the real acl field
			$group_deny_input.val('');
			$contact_deny_input.val('');
			[].forEach.call($acl_deny_input.tagsinput('items'), function (item) {
				if (item.type === 'group') {
					$group_deny_input.val($group_deny_input.val() + ',' + item.id);
				} else {
					$contact_deny_input.val($contact_deny_input.val() + ',' + item.id);
				}
			});
		});
	});
<?php echo '</script'; ?>
>
<?php }
}
