<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:55:14
  from '/var/www/friendica/view/templates/admin/summary.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b11264e058_68128875',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6a368520edf407c9d02e9b3deb9f818a2ae42acb' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/summary.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b11264e058_68128875 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id='adminpage'>
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<?php if (count($_smarty_tpl->tpl_vars['warningtext']->value)) {?>
	<div id="admin-warning-message-wrapper">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['warningtext']->value, 'wt');
$_smarty_tpl->tpl_vars['wt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wt']->value) {
$_smarty_tpl->tpl_vars['wt']->do_else = false;
?>
		<p class="warning-message"><?php echo $_smarty_tpl->tpl_vars['wt']->value;?>
</p>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
<?php }?>

	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['label'], ENT_QUOTES, 'UTF-8');?>
</dt>
		<dd><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/queue/deferred"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['deferred'], ENT_QUOTES, 'UTF-8');?>
</a> - <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/queue"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['queues']->value['workerq'], ENT_QUOTES, 'UTF-8');?>
</a></dd>
	</dl>
	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pending']->value[0], ENT_QUOTES, 'UTF-8');?>
</dt>
		<dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pending']->value[1], ENT_QUOTES, 'UTF-8');?>
</dt>
	</dl>

	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['users']->value[0], ENT_QUOTES, 'UTF-8');?>
</dt>
		<dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['users']->value[1], ENT_QUOTES, 'UTF-8');?>
</dd>
	</dl>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accounts']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
		<dl>
			<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
</dt>
			<dd><?php if ($_smarty_tpl->tpl_vars['p']->value[1]) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[1], ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?></dd>
		</dl>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value[0], ENT_QUOTES, 'UTF-8');?>
</dt>
		
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addons']->value[1], 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
			<dd><a href="/admin/addons/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a></dd>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
	</dl>

	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['version']->value[0], ENT_QUOTES, 'UTF-8');?>
</dt>
		<dd> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['platform']->value, ENT_QUOTES, 'UTF-8');?>
 '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['codename']->value, ENT_QUOTES, 'UTF-8');?>
' <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['version']->value[1], ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['build']->value, ENT_QUOTES, 'UTF-8');?>
</dt>
	</dl>

	<dl>
		<dt><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['serversettings']->value['label'], ENT_QUOTES, 'UTF-8');?>
</dt>
		<dd>
			<table>
				<tbody>
					<tr><td colspan="2"><b>PHP</b></td></tr>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['serversettings']->value['php'], 'p', false, 'k');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
						<tr><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
</td><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</td></tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<tr><td colspan="2"><b>MySQL / MariaDB</b></td></tr>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['serversettings']->value['mysql'], 'p', false, 'k');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
						<tr><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
</td><td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</td></tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</tbody>
			</table>
		</dd>
	</dl>

</div>
<?php }
}
