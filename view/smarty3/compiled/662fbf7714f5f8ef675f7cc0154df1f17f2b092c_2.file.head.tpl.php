<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:12:28
  from '/var/www/friendica/view/templates/head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b51cb02a65_79401175',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '662fbf7714f5f8ef675f7cc0154df1f17f2b092c' => 
    array (
      0 => '/var/www/friendica/view/templates/head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b51cb02a65_79401175 (Smarty_Internal_Template $_smarty_tpl) {
?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<base href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/" />
<meta name="generator" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['generator']->value, ENT_QUOTES, 'UTF-8');?>
" />
<link rel="stylesheet" href="view/global.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="all" />
<link rel="stylesheet" href="view/asset/jquery-colorbox/example5/colorbox.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/jgrowl/jquery.jgrowl.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/jquery-datetimepicker/build/jquery.datetimepicker.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />
<link rel="stylesheet" href="view/asset/perfect-scrollbar/dist/css/perfect-scrollbar.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="screen" />

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value, 'media', false, 'stylesheetUrl');
$_smarty_tpl->tpl_vars['media']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['stylesheetUrl']->value => $_smarty_tpl->tpl_vars['media']->value) {
$_smarty_tpl->tpl_vars['media']->do_else = false;
?>
	<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheetUrl']->value, ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['media']->value, ENT_QUOTES, 'UTF-8');?>
" />
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<link rel="shortcut icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shortcut_icon']->value, ENT_QUOTES, 'UTF-8');?>
" />
<link rel="apple-touch-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['touch_icon']->value, ENT_QUOTES, 'UTF-8');?>
"/>

<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="manifest" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/manifest" />
<?php echo '<script'; ?>
>
// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
// Prevents links to switch to Safari in a home screen app - see https://gist.github.com/irae/1042167
(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(chref=d.href).replace("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/", "").replace(e.href,"").indexOf("#")&&(!/^[a-z\+\.\-]+:/i.test(chref)||chref.indexOf(e.protocol+"//"+e.host)===0)&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone");
// |license-end
<?php echo '</script'; ?>
>

<link rel="search"
         href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/opensearch"
         type="application/opensearchdescription+xml"
         title="Search in Friendica" />

<!--[if IE]>
<?php echo '<script'; ?>
 type="text/javascript" src="https://html5shiv.googlecode.com/svn/trunk/html5.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<![endif]-->
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/modernizr.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery/dist/jquery.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/jquery.textinputs.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/textcomplete/dist/textcomplete.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/autocomplete.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery-colorbox/jquery.colorbox-min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jgrowl/jquery.jgrowl.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/imagesloaded/imagesloaded.pkgd.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/base64/base64.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/asset/dompurify/dist/purify.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="view/js/main.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>

	// Lifted from https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
    jQuery.fn.putCursorAtEnd = function() {
        return this.each(function() {
            // Cache references
            var $el = $(this),
                el = this;

            // Only focus if input isn't already
            if (!$el.is(":focus")) {
                $el.focus();
            }

            // If this function exists... (IE 9+)
            if (el.setSelectionRange) {
                // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                var len = $el.val().length * 2;

                // Timeout seems to be required for Blink
                setTimeout(function() {
                    el.setSelectionRange(len, len);
                }, 1);
            } else {
                // As a fallback, replace the contents with itself
                // Doesn't work in Chrome, but Chrome supports setSelectionRange
                $el.val($el.val());
            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Chrome)
            this.scrollTop = 999999;
        });
    };

    var updateInterval = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['update_interval']->value, ENT_QUOTES, 'UTF-8');?>
;
	var localUser = <?php if ($_smarty_tpl->tpl_vars['local_user']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['local_user']->value, ENT_QUOTES, 'UTF-8');
} else { ?>false<?php }?>;

	function confirmDelete() { return confirm("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delitem']->value, ENT_QUOTES, 'UTF-8');?>
"); }
	function commentExpand(id) {
		$("#comment-edit-text-" + id).putCursorAtEnd();
		$("#comment-edit-text-" + id).addClass("comment-edit-text-full");
		$("#comment-edit-text-" + id).removeClass("comment-edit-text-empty");
		$("#mod-cmnt-wrap-" + id).show();
		openMenu("comment-edit-submit-wrapper-" + id);
		return true;
	}
	function commentOpen(obj,id) {
		if (obj.value == "") {
			$("#comment-edit-text-" + id).addClass("comment-edit-text-full");
			$("#comment-edit-text-" + id).removeClass("comment-edit-text-empty");
			$("#mod-cmnt-wrap-" + id).show();
			openMenu("comment-edit-submit-wrapper-" + id);
			return true;
		}
		return false;
	}
	function commentClose(obj,id) {
		if (obj.value == "") {
			$("#comment-edit-text-" + id).removeClass("comment-edit-text-full");
			$("#comment-edit-text-" + id).addClass("comment-edit-text-empty");
			$("#mod-cmnt-wrap-" + id).hide();
			closeMenu("comment-edit-submit-wrapper-" + id);
			return true;
		}
		return false;
	}


	function commentInsert(obj,id) {
		var tmpStr = $("#comment-edit-text-" + id).val();
		if (tmpStr == "") {
			$("#comment-edit-text-" + id).addClass("comment-edit-text-full");
			$("#comment-edit-text-" + id).removeClass("comment-edit-text-empty");
			openMenu("comment-edit-submit-wrapper-" + id);
		}
		var ins = $(obj).html();
		ins = ins.replace("&lt;","<");
		ins = ins.replace("&gt;",">");
		ins = ins.replace("&amp;","&");
		ins = ins.replace("&quot;","\"");
		$("#comment-edit-text-" + id).val(tmpStr + ins);
	}

	function showHideCommentBox(id) {
		if ($("#comment-edit-form-" + id).is(":visible")) {
			$("#comment-edit-form-" + id).hide();
		} else {
			$("#comment-edit-form-" + id).show();
		}
	}


<?php echo '</script'; ?>
>


<?php }
}
