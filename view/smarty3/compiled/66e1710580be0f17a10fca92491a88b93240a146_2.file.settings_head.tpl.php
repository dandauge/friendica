<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:55:14
  from '/var/www/friendica/view/templates/admin/settings_head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1120be422_38796380',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '66e1710580be0f17a10fca92491a88b93240a146' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/settings_head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1120be422_38796380 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	$(document).ready(function() {
		$('.settings-content-block').hide();
		$('.settings-heading').click(function(){
			$('.settings-content-block').hide();
			$(this).next('.settings-content-block').toggle();
		});
	});
<?php echo '</script'; ?>
>
<?php }
}
