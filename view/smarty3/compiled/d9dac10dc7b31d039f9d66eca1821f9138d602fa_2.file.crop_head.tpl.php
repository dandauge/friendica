<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:54:35
  from '/var/www/friendica/view/templates/settings/profile/photo/crop_head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b0eb40a514_74336195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd9dac10dc7b31d039f9d66eca1821f9138d602fa' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/profile/photo/crop_head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b0eb40a514_74336195 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="view/asset/cropperjs/dist/cropper.min.js?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="view/asset/cropperjs/dist/cropper.min.css?v=<?php echo htmlspecialchars(@constant('FRIENDICA_VERSION'), ENT_QUOTES, 'UTF-8');?>
" type="text/css" />
<?php }
}
