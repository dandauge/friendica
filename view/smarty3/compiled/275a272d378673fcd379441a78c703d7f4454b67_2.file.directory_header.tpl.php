<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:02:02
  from '/var/www/friendica/view/theme/frio/templates/directory_header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b2aab13ac0_53876591',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '275a272d378673fcd379441a78c703d7f4454b67' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/directory_header.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:section_title.tpl' => 1,
    'file:contact_template.tpl' => 1,
  ),
),false)) {
function content_6044b2aab13ac0_53876591 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="generic-page-wrapper">
	<?php if ($_smarty_tpl->tpl_vars['gdirpath']->value) {?>
	<ul class="list-unstyled pull-right">
		<li><div id="global-directory-link"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gdirpath']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['globaldir']->value, ENT_QUOTES, 'UTF-8');?>
</a></div></li>
	</ul>
	<?php }?>

	<?php $_smarty_tpl->_subTemplateRender("file:section_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div id="directory-search-wrapper">
		<form id="directory-search-form" class="navbar-form" role="search" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_mod']->value, ENT_QUOTES, 'UTF-8');?>
" method="get" >
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 ">
					<div class="form-group form-group-search">
						<input type="text" name="search" id="directory-search" class="search-input form-control form-search" onfocus="this.select();" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo $_smarty_tpl->tpl_vars['desc']->value;?>
"/>
						<button class="btn btn-default btn-sm form-button-search" type="submit" id="directory-search-submit"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
</button>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
	</div>

	<hr>

	<div id="directory-search-end" class="clear"></div>

		<ul id="viewcontact_wrapper" class="viewcontact_wrapper media-list">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contacts']->value, 'contact');
$_smarty_tpl->tpl_vars['contact']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->do_else = false;
?>
		<li><?php $_smarty_tpl->_subTemplateRender("file:contact_template.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?></li>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>

	<div class="directory-end" ></div>

	<?php echo $_smarty_tpl->tpl_vars['paginate']->value;?>

</div>
<?php }
}
