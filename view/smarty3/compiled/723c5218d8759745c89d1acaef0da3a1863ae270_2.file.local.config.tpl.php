<?php
/* Smarty version 3.1.36, created on 2021-03-07 09:35:11
  from '/var/www/friendica/view/templates/local.config.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449e4fe3aa15_28607251',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '723c5218d8759745c89d1acaef0da3a1863ae270' => 
    array (
      0 => '/var/www/friendica/view/templates/local.config.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449e4fe3aa15_28607251 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?php

';?>
// Local configuration

// If you're unsure about what any of the config keys below do, please check the static/defaults.config.php for detailed
// documentation of their data type and behavior.

return [
	'database' => [
		'hostname' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['dbhost']->value);?>
',
		'username' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['dbuser']->value);?>
',
		'password' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['dbpass']->value);?>
',
		'database' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['dbdata']->value);?>
',
		'charset' => 'utf8mb4',
	],

	// ****************************************************************
	// The configuration below will be overruled by the admin panel.
	// Changes made below will only have an effect if the database does
	// not contain any configuration for the friendica system.
	// ****************************************************************

	'config' => [
		'php_path' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['phpath']->value);?>
',
		'admin_email' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adminmail']->value);?>
',
		'sitename' => 'Friendica Social Network',
		'hostname' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['hostname']->value);?>
',
		'register_policy' => \Friendica\Module\Register::OPEN,
		'max_import_size' => 200000,
	],
	'system' => [
		'urlpath' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['urlpath']->value);?>
',
		'url' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['baseurl']->value);?>
',
		'ssl_policy' => <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['sslpolicy']->value);?>
,
		'basepath' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['basepath']->value);?>
',
		'default_timezone' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['timezone']->value);?>
',
		'language' => '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['language']->value);?>
',
	],
];
<?php }
}
