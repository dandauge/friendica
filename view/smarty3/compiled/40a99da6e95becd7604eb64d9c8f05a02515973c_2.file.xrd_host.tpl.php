<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:55:14
  from '/var/www/friendica/view/templates/xrd_host.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b112622d48_57309528',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '40a99da6e95becd7604eb64d9c8f05a02515973c' => 
    array (
      0 => '/var/www/friendica/view/templates/xrd_host.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b112622d48_57309528 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>
version='1.0' encoding='UTF-8'<?php echo '?>';?>

<XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'
     xmlns:hm='http://host-meta.net/xrd/1.0'>
 
    <hm:Host><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['zhost']->value, ENT_QUOTES, 'UTF-8');?>
</hm:Host>
 
    <Link rel='lrdd' type='application/xrd+xml' template='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
/xrd?uri={uri}' />
    <Link rel='lrdd' type='application/json' template='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
/.well-known/webfinger?resource={uri}' />
    <Link rel='acct-mgmt' href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
/amcd' />
    <Link rel='http://services.mozilla.com/amcd/0.1' href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
/amcd' />
	<Link rel="http://oexchange.org/spec/0.8/rel/resident-target" type="application/xrd+xml" 
        href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
/oexchange/xrd" />

    <Property xmlns:mk="http://salmon-protocol.org/ns/magic-key"
        type="http://salmon-protocol.org/ns/magic-key"
        mk:key_id="1"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bigkey']->value, ENT_QUOTES, 'UTF-8');?>
</Property>
</XRD>
<?php }
}
