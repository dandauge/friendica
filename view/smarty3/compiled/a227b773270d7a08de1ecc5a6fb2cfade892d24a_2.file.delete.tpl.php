<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:56:55
  from '/var/www/friendica/view/templates/admin/item/delete.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1776c5fa9_90963044',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a227b773270d7a08de1ecc5a6fb2cfade892d24a' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/item/delete.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_input.tpl' => 1,
  ),
),false)) {
function content_6044b1776c5fa9_90963044 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="adminpage">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
	<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['intro1']->value, ENT_QUOTES, 'UTF-8');?>
</p>
	<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['intro2']->value, ENT_QUOTES, 'UTF-8');?>
</p>

	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/item/delete" method="post">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['deleteitemguid']->value), 0, false);
?>
		<div class="submit"><input type="submit" name="page_deleteitem_submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
" /></div>
	</form>

</div>
<?php }
}
