<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:37:05
  from '/var/www/friendica/view/templates/email/system/html.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_60449ec173eb08_93614286',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '66567ecbf376469734607fd9983e4ac5a3d9a857' => 
    array (
      0 => '/var/www/friendica/view/templates/email/system/html.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60449ec173eb08_93614286 (Smarty_Internal_Template $_smarty_tpl) {
?><table>
	<thead>
	<tr>
		<td>
			<?php echo $_smarty_tpl->tpl_vars['preamble']->value;?>

		</td>
	</tr>
	</thead>
	<tbody>
		<tr>
			<td style="padding-right:22px;">
				<?php echo $_smarty_tpl->tpl_vars['htmlversion']->value;?>

			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['thanks']->value, ENT_QUOTES, 'UTF-8');?>

			</td>
		</tr>
	<tr>
		<td>
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['site_admin']->value, ENT_QUOTES, 'UTF-8');?>

		</td>
	</tr>
	</tfoot>
</table>
<?php }
}
