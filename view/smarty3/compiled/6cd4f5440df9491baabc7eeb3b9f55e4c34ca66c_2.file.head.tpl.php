<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:13:06
  from '/var/www/friendica/view/templates/settings/head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b542bfbd17_12735267',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6cd4f5440df9491baabc7eeb3b9f55e4c34ca66c' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/head.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b542bfbd17_12735267 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
>
	var ispublic = "<?php echo $_smarty_tpl->tpl_vars['ispublic']->value;?>
";


	$(document).ready(function() {

		$('#contact_allow, #contact_deny, #group_allow, #group_deny').change(function() {
			var selstr;
			$('#contact_allow option:selected, #contact_deny option:selected, #group_allow option:selected, #group_deny option:selected').each( function() {
				selstr = $(this).html();
				$('#jot-perms-icon').removeClass('unlock').addClass('lock');
				$('#jot-public').hide();
			});
			if(selstr == null) { 
				$('#jot-perms-icon').removeClass('lock').addClass('unlock');
				$('#jot-public').show();
			}

		}).trigger('change');
		
		$('.settings-content-block').hide();
		$('.settings-heading').click(function(){
			$('.settings-content-block').hide();
			$(this).next('.settings-content-block').toggle();
		});

	});

<?php echo '</script'; ?>
>

<?php }
}
