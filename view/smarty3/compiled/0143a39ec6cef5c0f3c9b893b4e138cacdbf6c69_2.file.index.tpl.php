<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:53
  from '/var/www/friendica/view/templates/admin/addons/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1ed2d0471_85838867',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0143a39ec6cef5c0f3c9b893b4e138cacdbf6c69' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/addons/index.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1ed2d0471_85838867 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="adminpage">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>
<?php if ($_smarty_tpl->tpl_vars['pcount']->value == 0) {?>
	<div class="error-message">
	<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['noplugshint']->value, ENT_QUOTES, 'UTF-8');?>

	</div>
<?php } else { ?>
	<a class="btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['function']->value, ENT_QUOTES, 'UTF-8');?>
?action=reload&amp;t=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reload']->value, ENT_QUOTES, 'UTF-8');?>
</a>
	<ul id="addonslist">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addons']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
		<li class="addon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[1], ENT_QUOTES, 'UTF-8');?>
">
			<span class="offset-anchor" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
"></span>
			<a class="toggleaddon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['function']->value, ENT_QUOTES, 'UTF-8');?>
?action=toggle&amp;addon=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
&amp;t=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
" title="<?php if ($_smarty_tpl->tpl_vars['p']->value[1] == 'on') {?>Disable<?php } else { ?>Enable<?php }?>">
				<span class="icon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[1], ENT_QUOTES, 'UTF-8');?>
"></span>
			</a>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['function']->value, ENT_QUOTES, 'UTF-8');?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[0], ENT_QUOTES, 'UTF-8');?>
"><span class="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[2]['name'], ENT_QUOTES, 'UTF-8');?>
</span></a> - <span class="version"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value[2]['version'], ENT_QUOTES, 'UTF-8');?>
</span>
			<?php if ($_smarty_tpl->tpl_vars['p']->value[2]['experimental']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['experimental']->value, ENT_QUOTES, 'UTF-8');?>
 <?php }
if ($_smarty_tpl->tpl_vars['p']->value[2]['unsupported']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unsupported']->value, ENT_QUOTES, 'UTF-8');?>
 <?php }?>
			<div class="desc"><?php echo $_smarty_tpl->tpl_vars['p']->value[2]['description'];?>
</div>
		</li>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>
<?php }?>
</div>
<?php }
}
