<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:01:37
  from '/var/www/friendica/view/templates/widget/filter.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b291ad26f3_81855099',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c367a1b1a4517c0de5b9d13648874f1b0ee37dc' => 
    array (
      0 => '/var/www/friendica/view/templates/widget/filter.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b291ad26f3_81855099 (Smarty_Internal_Template $_smarty_tpl) {
?><span id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar-inflated" class="widget fakelink" onclick="openCloseWidget('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar-inflated');">
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
</span>
<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar" class="widget">
	<span class="fakelink" onclick="openCloseWidget('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar-inflated');">
		<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
	</span>
	<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-desc"><?php echo $_smarty_tpl->tpl_vars['desc']->value;?>
</div>
	<ul role="menu" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-ul">
		<li role="menuitem" <?php if (!is_null($_smarty_tpl->tpl_vars['selected']->value) && !$_smarty_tpl->tpl_vars['selected']->value) {?>class="selected"<?php }?>><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base']->value, ENT_QUOTES, 'UTF-8');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-link<?php if (!$_smarty_tpl->tpl_vars['selected']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-selected<?php }?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-all"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_label']->value, ENT_QUOTES, 'UTF-8');?>
</a></li>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['options']->value, 'option');
$_smarty_tpl->tpl_vars['option']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->do_else = false;
?>
			<li role="menuitem" <?php if ($_smarty_tpl->tpl_vars['selected']->value == $_smarty_tpl->tpl_vars['option']->value['ref']) {?>class="selected"<?php }?>><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['ref'], ENT_QUOTES, 'UTF-8');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-link<?php if ($_smarty_tpl->tpl_vars['selected']->value == $_smarty_tpl->tpl_vars['option']->value['ref']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-selected<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a></li>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</ul>
</div>
<?php echo '<script'; ?>
>
initWidget('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
-sidebar-inflated');
<?php echo '</script'; ?>
>
<?php }
}
