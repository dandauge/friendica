<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:57:09
  from '/var/www/friendica/view/templates/babel.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b18568ecd3_84720999',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d1f8af22049d82996267c7d1cf861f54fed0ec2' => 
    array (
      0 => '/var/www/friendica/view/templates/babel.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_textarea.tpl' => 1,
    'file:field_radio.tpl' => 5,
  ),
),false)) {
function content_6044b18568ecd3_84720999 (Smarty_Internal_Template $_smarty_tpl) {
?><h2>Babel Diagnostic</h2>
<form action="babel" method="post" class="panel panel-default">
	<div class="panel-body">
		<div class="form-group">
			<?php $_smarty_tpl->_subTemplateRender("file:field_textarea.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['text']->value), 0, false);
?>
		</div>
		<div class="form-group">
			<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['type_bbcode']->value), 0, false);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['type_diaspora']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['type_markdown']->value), 0, true);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['type_html']->value), 0, true);
?>
		<?php if ($_smarty_tpl->tpl_vars['flag_twitter']->value) {?>
			<?php $_smarty_tpl->_subTemplateRender("file:field_radio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['type_twitter']->value), 0, true);
?>
		<?php }?>
		</div>
		<p><button type="submit" class="btn btn-primary">Submit</button></p>
	</div>
</form>

<?php if ($_smarty_tpl->tpl_vars['results']->value) {?>
<div class="babel-results">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'result');
$_smarty_tpl->tpl_vars['result']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['result']->value) {
$_smarty_tpl->tpl_vars['result']->do_else = false;
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result']->value['title'], ENT_QUOTES, 'UTF-8');?>
</h3>
		</div>
		<div class="panel-body"><?php echo $_smarty_tpl->tpl_vars['result']->value['content'];?>
</div>
	</div>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php }
}
}
