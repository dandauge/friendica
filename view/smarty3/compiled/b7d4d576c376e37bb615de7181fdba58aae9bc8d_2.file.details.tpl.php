<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:00:45
  from '/var/www/friendica/view/templates/admin/addons/details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b25d57a0c5_24644594',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7d4d576c376e37bb615de7181fdba58aae9bc8d' => 
    array (
      0 => '/var/www/friendica/view/templates/admin/addons/details.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b25d57a0c5_24644594 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="adminpage">
	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

	<p><span class="toggleaddon icon <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['status']->value, ENT_QUOTES, 'UTF-8');?>
"></span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info']->value['name'], ENT_QUOTES, 'UTF-8');?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info']->value['version'], ENT_QUOTES, 'UTF-8');?>
 : <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['function']->value, ENT_QUOTES, 'UTF-8');?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addon']->value, ENT_QUOTES, 'UTF-8');?>
/?action=toggle&amp;t=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');?>
</a></p>
	<p><?php echo $_smarty_tpl->tpl_vars['info']->value['description'];?>
</p>

	<p class="author"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['str_author']->value, ENT_QUOTES, 'UTF-8');?>

	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['info']->value['author'], 'a', false, NULL, 'authors', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
$_smarty_tpl->tpl_vars['a']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
$_smarty_tpl->tpl_vars['a']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['total'];
?>
		<?php if ($_smarty_tpl->tpl_vars['a']->value['link']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['name'], ENT_QUOTES, 'UTF-8');
}
if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_authors']->value['last'] : null)) {
} else { ?>, <?php }?>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</p>

	<p class="maintainer"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['str_maintainer']->value, ENT_QUOTES, 'UTF-8');?>

	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['info']->value['maintainer'], 'a', false, NULL, 'maintainers', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
$_smarty_tpl->tpl_vars['a']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
$_smarty_tpl->tpl_vars['a']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['total'];
?>
		<?php if ($_smarty_tpl->tpl_vars['a']->value['link']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['name'], ENT_QUOTES, 'UTF-8');
}
if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_maintainers']->value['last'] : null)) {
} else { ?>, <?php }?>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</p>

	<?php if ($_smarty_tpl->tpl_vars['screenshot']->value) {?>
	<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['screenshot']->value[0], ENT_QUOTES, 'UTF-8');?>
" class="screenshot"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['screenshot']->value[0], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['screenshot']->value[1], ENT_QUOTES, 'UTF-8');?>
" /></a>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['admin_form']->value) {?>
	<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
	<form method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['baseurl']->value, ENT_QUOTES, 'UTF-8');?>
/admin/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['function']->value, ENT_QUOTES, 'UTF-8');?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addon']->value, ENT_QUOTES, 'UTF-8');?>
">
		<input type="hidden" name="form_security_token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
">
		<?php echo $_smarty_tpl->tpl_vars['admin_form']->value;?>

	</form>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['readme']->value) {?>
	<h3>Readme</h3>
	<div id="addon_readme">
		<?php echo $_smarty_tpl->tpl_vars['readme']->value;?>

	</div>
	<?php }?>
</div>
<?php }
}
