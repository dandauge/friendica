<?php
/* Smarty version 3.1.36, created on 2021-03-07 10:58:00
  from '/var/www/friendica/view/theme/frio/templates/field_radio.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b1b86be336_61853049',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e4be5bf0286c2bdadd2a99b30e4877f017b288de' => 
    array (
      0 => '/var/www/friendica/view/theme/frio/templates/field_radio.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6044b1b86be336_61853049 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<div class="field radio">
		<div class="radio">
			<input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
" id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?>checked<?php }?> tabindex="<?php if ($_smarty_tpl->tpl_vars['field']->value[4]) {?>0<?php } else { ?>-1<?php }?>" aria-describedby="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
_tip">
			<label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
">
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[1], ENT_QUOTES, 'UTF-8');?>

				<?php if ($_smarty_tpl->tpl_vars['field']->value[3]) {?>
				<span class="help-block" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[0], ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value[2], ENT_QUOTES, 'UTF-8');?>
_tip" role="tooltip"><?php echo $_smarty_tpl->tpl_vars['field']->value[3];?>
</span>
				<?php }?>
			</label>
		</div>
	</div>
<?php }
}
