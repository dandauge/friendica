<?php
/* Smarty version 3.1.36, created on 2021-03-07 11:13:06
  from '/var/www/friendica/view/templates/settings/display.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_6044b542c2d344_81688740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9512f01e7e9756235d9411804aa9150c391d1875' => 
    array (
      0 => '/var/www/friendica/view/templates/settings/display.tpl',
      1 => 1615103316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:field_themeselect.tpl' => 2,
    'file:field_input.tpl' => 3,
    'file:field_checkbox.tpl' => 7,
    'file:field_select.tpl' => 1,
  ),
),false)) {
function content_6044b542c2d344_81688740 (Smarty_Internal_Template $_smarty_tpl) {
?><h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ptitle']->value, ENT_QUOTES, 'UTF-8');?>
</h1>

<form action="settings/display" id="settings-form" method="post" autocomplete="off">
	<input type='hidden' name='form_security_token' value='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_security_token']->value, ENT_QUOTES, 'UTF-8');?>
'>

	<?php $_smarty_tpl->_subTemplateRender("file:field_themeselect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['theme']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['itemspage_network']->value), 0, false);
?>

		<?php if (count($_smarty_tpl->tpl_vars['mobile_theme']->value[4]) > 1) {?>
		<?php $_smarty_tpl->_subTemplateRender("file:field_themeselect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['mobile_theme']->value), 0, true);
?>
	<?php }?>

	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['itemspage_mobile_network']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['ajaxint']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['no_auto_update']->value), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['nosmile']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['infinite_scroll']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['no_smart_threading']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['hide_dislike']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['display_resharer']->value), 0, true);
?>
	<?php $_smarty_tpl->_subTemplateRender("file:field_checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['stay_local']->value), 0, true);
?>

	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['calendar_title']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
	<?php $_smarty_tpl->_subTemplateRender("file:field_select.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['first_day_of_week']->value), 0, false);
?>

	<div class="settings-submit-wrapper">
		<input type="submit" name="submit" class="settings-submit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['submit']->value, ENT_QUOTES, 'UTF-8');?>
"/>
	</div>

	<?php if ($_smarty_tpl->tpl_vars['theme_config']->value) {?>
		<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stitle']->value, ENT_QUOTES, 'UTF-8');?>
</h2>
		<?php echo $_smarty_tpl->tpl_vars['theme_config']->value;?>

	<?php }?>

</form>
<?php }
}
